import org.junit.Test;

import org.locationtech.jts.geom.*;

public class JTStest {
    @Test
    public void helloWorld(){
        System.out.println("hello world !");
    }
    @Test
    public void Something(){
        System.out.println("This is a test");
    }
    @Test
    public void trySomeJTS() {
        Coordinate[] coordinateArray = new Coordinate[] {
                new Coordinate(0,0), new Coordinate(10, 10), new Coordinate(10,0), new Coordinate(0,0)
        };
        Geometry lr = new GeometryFactory().createLinearRing(coordinateArray);
        Geometry pg = new GeometryFactory().createPolygon((LinearRing) lr);
        System.out.println(lr);
        System.out.println(pg);
    }

    @Test
    public void tryBooleanIntersection() {
        Coordinate[] coordinateArray = new Coordinate[] {
                new Coordinate(0,0), new Coordinate(10, 10), new Coordinate(10,0), new Coordinate(0,0)
        };

        Geometry lr = new GeometryFactory().createLinearRing(coordinateArray);
        Geometry pg = new GeometryFactory().createPolygon((LinearRing) lr);

        Coordinate[] coordinateArray2 = new Coordinate[] {
                new Coordinate(5,5), new Coordinate(15, 15), new Coordinate(15,5), new Coordinate(5,5)
        };

        Geometry lr2 = new GeometryFactory().createLinearRing(coordinateArray2);
        Geometry pg2 = new GeometryFactory().createPolygon((LinearRing) lr2);

        Geometry output = pg2.intersection(pg);
        System.out.println(output);
    }

    @Test
    public void tryingContainment() {
        Coordinate[] mainLrCoor = new Coordinate[] {
                new Coordinate(0,0),
                new Coordinate(10,0),
                new Coordinate(10,10),
                new Coordinate(0,10),
                new Coordinate(0,0)
        };

        LinearRing mainLr = new GeometryFactory().createLinearRing(mainLrCoor);
        Polygon mainPg = new GeometryFactory().createPolygon(mainLr);

        Coordinate[] secLrA = new Coordinate[] {
                new Coordinate(5, 5),
                new Coordinate(15, 5),
                new Coordinate(15, 15),
                new Coordinate(5, 15),
                new Coordinate(5, 5)
        };

        LinearRing lrA = new GeometryFactory().createLinearRing(secLrA);
        Polygon pgA = new GeometryFactory().createPolygon(lrA);

        Coordinate[] secLrB = new Coordinate[] {
                new Coordinate(15, 15),
                new Coordinate(25, 15),
                new Coordinate(25, 25),
                new Coordinate(15, 25),
                new Coordinate(15, 15)
        };

        LinearRing lrB = new GeometryFactory().createLinearRing(secLrB);
        Polygon pgB = new GeometryFactory().createPolygon(lrB);

        Coordinate[] secLrC = new Coordinate[] {
                new Coordinate(5, 5),
                new Coordinate(10, 5),
                new Coordinate(10, 10),
                new Coordinate(5, 10),
                new Coordinate(5, 5)
        };

        LinearRing lrC = new GeometryFactory().createLinearRing(secLrC);
        Polygon pgC = new GeometryFactory().createPolygon(lrC);

        Coordinate[] secLrD = new Coordinate[] {
                new Coordinate(10, 5),
                new Coordinate(15, 5),
                new Coordinate(15, 10),
                new Coordinate(10, 10),
                new Coordinate(10, 5)
        };

        LinearRing lrD = new GeometryFactory().createLinearRing(secLrD);
        Polygon pgD = new GeometryFactory().createPolygon(lrD);

        Coordinate[] secLrE = new Coordinate[] {
                new Coordinate(2.5, 2.5),
                new Coordinate(7.5, 2.5),
                new Coordinate(7.5, 7.5),
                new Coordinate(2.5, 7.5),
                new Coordinate(2.5, 2.5)
        };

        LinearRing lrE = new GeometryFactory().createLinearRing(secLrE);
        Polygon pgE = new GeometryFactory().createPolygon(lrE);

        Point ptInside = new GeometryFactory().createPoint(new Coordinate(5,5));
        Point ptEdge = new GeometryFactory().createPoint(new Coordinate(10,5));
        Point ptOutside = new GeometryFactory().createPoint(new Coordinate(15,5));

        System.out.println(" does main_pg hava a id ? : " + "no :(, only SRID: " + mainPg.getSRID());
        System.out.println(" does inside point hava a id ? : " + ptInside.getSRID());
        System.out.println(" =============================== ");
        System.out.println(" main polygon with inside point:  " + mainPg.contains(ptInside) );
        System.out.println(" main polygon with edge point:    " + mainPg.contains(ptEdge) );
        System.out.println(" main polygon with edge point:    " + mainPg.intersects(ptEdge) );
        System.out.println(" main polygon with outside point: " + mainPg.contains(ptOutside) );
        System.out.println(" =============================== ");
        System.out.println(" main polygon with pgA: " + mainPg.contains(pgA) );
        System.out.println(" main polygon with pgB: " + mainPg.contains(pgB) );
        System.out.println(" main polygon with pgC: " + mainPg.contains(pgC) );
        System.out.println(" main polygon with pgD: " + mainPg.contains(pgD) );
        System.out.println(" main polygon with pgE: " + mainPg.contains(pgE) );
        System.out.println(" =============================== ");
        System.out.println(" main polygon with lrA: " + mainPg.contains(lrA) );
        System.out.println(" main polygon with lrB: " + mainPg.contains(lrB) );
        System.out.println(" main polygon with lrC: " + mainPg.contains(lrC) );
        System.out.println(" main polygon with lrD: " + mainPg.contains(lrD) );
        System.out.println(" main polygon with lrE: " + mainPg.contains(lrE) );
        System.out.println(" =============================== ");
        System.out.println(" main pg as string: " + mainPg.toString());
        System.out.println(" main pg as json: " + ":( - sadly JTS doesn't have a default JSON parse");
        System.out.println(" main pg as coordinates: " );
        for (Coordinate pt : mainPg.getCoordinates()){
            System.out.println("\t" + pt);
        }
        System.out.println(" applying a boolean difference (get an extra hole in the object)");
//        mainPg = (Polygon) mainPg.difference(pgE);
//        mainPg.getInteriorRingN(0);
//        mainPg.getExteriorRing();
//        System.out.println(mainPg.getCoordinates());
//        for (Coordinate pt : mainPg.getCoordinates()){
//            System.out.println(pt.);
//        }
        System.out.println(" main pg as coordinates: " + mainPg.getCentroid());
    }

//    @Test
//    public void tryJTSSkeleton(){
//        Coordinate[] mainLrCoor = new Coordinate[] {
//                new Coordinate(0,0),
//                new Coordinate(10,0),
//                new Coordinate(10,10),
//                new Coordinate(0,10),
//                new Coordinate(0,0)
//        };
//
//        LinearRing mainLr = new GeometryFactory().createLinearRing(mainLrCoor);
//        Polygon mainPg = new GeometryFactory().createPolygon(mainLr);
//
//        mainPg
//
//
//    }
}
