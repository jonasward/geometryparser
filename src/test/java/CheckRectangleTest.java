import org.junit.Test;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;

public class CheckRectangleTest {
    @Test
    public void JTSRectangleTest(){
        GeometryFactory gf = new GeometryFactory();
        Geometry lr = gf.createLinearRing(new Coordinate[]{
            new Coordinate(0.0, 0.0),
            new Coordinate(1.0, 0.0),
            new Coordinate(1.0, 1.0),
            new Coordinate(0.0, 1.0),
            new Coordinate(0.0, 0.0)
        });

        System.out.println(lr.isRectangle());
    }
}
