import org.junit.Test;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Polygon;

public class JTSBooleans {

    static Coordinate[] figureA = new Coordinate[] {
            new Coordinate(0,0),
            new Coordinate(10,0),
            new Coordinate(10,10),
            new Coordinate(0,10),
            new Coordinate(0,0)
    };

    static Coordinate[] figureB = new Coordinate[] {
            new Coordinate(5,5),
            new Coordinate(15,5),
            new Coordinate(15,15),
            new Coordinate(5,15),
            new Coordinate(5,5)
    };

    @Test
    public void lrLrIntersection() {
        GeometryFactory gf = new GeometryFactory();

        Geometry linearRingA = gf.createLinearRing(figureA);
        Geometry linearRingB = gf.createLinearRing(figureB);

        System.out.println(linearRingA.intersection(linearRingB) );
    }

    @Test
    public void lrPgIntersectionAB() {
        GeometryFactory gf = new GeometryFactory();

        Geometry linearRingA = gf.createLinearRing(figureA);
        Geometry polygonA = gf.createPolygon(figureB);

        System.out.println(linearRingA.intersection(polygonA) );
    }

    @Test
    public void lrPgIntersectionBA() {
        GeometryFactory gf = new GeometryFactory();

        Geometry linearRingA = gf.createLinearRing(figureB);
        Geometry polygonA = gf.createPolygon(figureA);

        System.out.println(linearRingA.intersection(polygonA) );
    }

    @Test
    public void lsMpgIntersectionAB() {
        GeometryFactory gf = new GeometryFactory();

        Geometry lineStringA = gf.createLineString(figureA);
        Geometry polygonA = gf.createMultiPolygon(new Polygon[] { gf.createPolygon(figureB) } );

        System.out.println(lineStringA.intersection(polygonA) );
    }

    @Test
    public void lsPgIntersectionBA() {
        GeometryFactory gf = new GeometryFactory();

        Geometry lineStringA = gf.createLineString(figureB);
        Geometry polygonA = gf.createPolygon(figureA);

        System.out.println(lineStringA.intersection(polygonA) );
    }
}
