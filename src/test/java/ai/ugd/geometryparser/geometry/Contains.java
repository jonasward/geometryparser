package ai.ugd.geometryparser.geometry;

import org.junit.Test;
import org.locationtech.jts.geom.Coordinate;

import java.util.UUID;

public class Contains {
    Coordinate[] smallPolygon = new Coordinate[] {
            new Coordinate(0,0),
            new Coordinate(5,0),
            new Coordinate(5,5),
            new Coordinate(0,5),
            new Coordinate(0,0),
    };

    Coordinate[] largePolygon = new Coordinate[] {
            new Coordinate(-5,-5),
            new Coordinate(10,-5),
            new Coordinate(10,10),
            new Coordinate(-5,10),
            new Coordinate(-5,-5),
    };

    @Test
    public void testContainment() {
        GeomBase largeRec = new GeomBase(UUID.randomUUID(), largePolygon, GeomType.POLYGON);
        GeomBase smallRec = new GeomBase(UUID.randomUUID(), smallPolygon, GeomType.POLYGON);

        System.out.println(largeRec.contains(smallRec) );
        System.out.println(smallRec.contains(largeRec) );

        System.out.println(largeRec.getExternalGeo().contains(smallRec.getInternalGeo() ) );
//        Polygon largePg = new GeometryFactory().createPolygon( (LinearRing) largeRec.getExternalGeo());
//        Polygon smallPg = new GeometryFactory().createPolygon( (LinearRing) smallRec.getExternalGeo());

//        System.out.println(largePg.contains(smallPg) );
    }
}
