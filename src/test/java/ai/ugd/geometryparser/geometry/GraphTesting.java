package ai.ugd.geometryparser.geometry;

import ai.ugd.geometryparser.parsing.FrontEndObject;
import org.junit.Test;
import org.locationtech.jts.geom.*;
import org.locationtech.jts.io.WKTReader;
import org.locationtech.jts.operation.linemerge.LineMergeEdge;
import org.locationtech.jts.operation.linemerge.LineMergeGraph;
import org.locationtech.jts.planargraph.Node;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public class GraphTesting {

    String[] lineStrings = new String[]{
            "LINESTRING (9.57484722137 8.30104732513, 21.8891448975 8.45777511597)",
            "LINESTRING (31.4138793945 0.103040076792, 42.3601799011 3.12650346756)",
            "LINESTRING (43.8901176453 9.79120731354, 36.4238357544 16.6090755463)",
            "LINESTRING (36.9813766479 37.7201194763, 33.6946678162 34.2525100708)",
            "LINESTRING (36.9813766479 37.7201194763, 15.3274297714 32.1164283752)",
            "LINESTRING (11.7506141663 21.4384689331, 17.6918601990 30.1124019623)",
            "LINESTRING (35.6276779175 35.4813079834, 36.4238357544 16.6090755463)",
            "LINESTRING (6.8064661026 35.0012168884, 9.57484722137 8.30104732513)",
            "LINESTRING (32.2922630310 25.8213424683, 21.8891448975 8.45777511597)",
            "LINESTRING (11.7506141663 21.4384689331, 9.57484722137 8.30104732513)",
            "LINESTRING (15.3274297714 32.1164283752, 33.6946678162 34.2525100708)",
            "LINESTRING (7.03974676132 39.6902961731, 6.8064661026 35.0012168884)",
            "LINESTRING (19.9018554688 9.43243217468, 21.8891448975 8.45777511597)",
            "LINESTRING (11.7506141663 21.4384689331, 11.3009891510 26.5819206238)",
            "LINESTRING (11.3009891510 26.5819206238, 10.6122369766 35.1878738403)",
            "LINESTRING (7.03974676132 39.6902961731, 36.9813766479 37.7201194763)",
            "LINESTRING (31.4138793945 0.103040076792, 9.57484722137 8.30104732513)",
            "LINESTRING (19.9018554688 9.43243217468, 32.2922630310 25.8213424683)",
            "LINESTRING (17.6918601990 30.1124019623, 33.6946678162 34.2525100708)",
            "LINESTRING (10.6122369766 35.1878738403, 6.8064661026 35.0012168884)",
            "LINESTRING (31.4138793945 0.103040076792, 21.8891448975 8.45777511597)",
            "LINESTRING (10.6122369766 35.1878738403, 15.3274297714 32.1164283752)",
            "LINESTRING (11.7506141663 21.4384689331, 19.9018554688 9.43243217468)",
            "LINESTRING (36.4238357544 16.6090755463, 42.3601799011 3.12650346756)",
            "LINESTRING (11.7506141663 21.4384689331, 6.8064661026 35.0012168884)",

    };

    String[] coordinates = new String[]{
            "POINT (11.7506141663 21.4384689331)",
            "POINT (17.6918601990 30.1124019623)",
            "POINT (7.03974676132 39.6902961731)",
            "POINT (11.3009891510 26.5819206238)",
            "POINT (10.6122369766 35.1878738403)",
            "POINT (6.8064661026 35.0012168884)",
            "POINT (31.4138793945 0.103040076792)",
            "POINT (19.9018554688 9.43243217468)",
            "POINT (9.57484722137 8.30104732513)",
            "POINT (36.9813766479 37.7201194763)",
            "POINT (35.6276779175 35.4813079834)",
            "POINT (43.8901176453 9.79120731354)",
            "POINT (32.2922630310 25.8213424683)",
            "POINT (15.3274297714 32.1164283752)",
            "POINT (36.4238357544 16.6090755463)",
            "POINT (33.6946678162 34.2525100708)",
            "POINT (42.3601799011 3.12650346756)",
            "POINT (21.8891448975 8.45777511597)",

    };

    int[][] connectionGraph = new int[][] {
            new int[]{2,4},
            new int[]{3,5},
            new int[]{10,15},
            new int[]{7,8},
            new int[]{9,10},
            new int[]{12,14},
            new int[]{11,16},
            new int[]{6,14},
            new int[]{1,12},
            new int[]{3,13},
            new int[]{10,12},
            new int[]{12,15},
            new int[]{9,11},
            new int[]{10,11},
            new int[]{1,3},
            new int[]{0,12},
            new int[]{1,13},
            new int[]{2,13},
            new int[]{14,17},
    };

    MultiLineString mls;
//    public GeomPlanarGraph(UUID uuid, Coordinate[] coords, List<List<Integer>> connectionGraph, double buffer, GeomType geomType) throws IllegalArgumentException
//    IGeomBase test = new GeomPlanarGraph(UUID.randomUUID(), this.getCoords(), this.planerGraphInts(), 1.1, GeomType.NETWORK);
    
    public Coordinate[] getCoords() {
        WKTReader reader = new WKTReader();
        List<Coordinate> coords = new ArrayList<>();
        for (String coor : this.coordinates) {
            try {
                Point thisPt = (Point) reader.read(coor);
                coords.add(thisPt.getCoordinate());
                System.out.println(thisPt);
            } catch (Exception e) {
                System.out.println(e);
            }
        }

        Coordinate[] coors = new Coordinate[coords.size()];
        int i = 0;
        for (Coordinate c : coords) {
            coors[i] = c;
            i++;
        }
        return coors;
    }

    public List<List<Integer>> planerGraphInts() {
        List<List<Integer>> ints = new ArrayList<>();
        int i = 0;
        for (int[] e: this.connectionGraph) {
            List<Integer> localInts = new ArrayList<>();
            for (int f : e) {
                localInts.add(f);
            }
            ints.add(localInts);
        }
        return  ints;
    }

    public LineMergeGraph tryingOutPlanarGraph() {
        LineMergeGraph pg = new LineMergeGraph();
        WKTReader reader = new WKTReader();
        for (String edgeString : this.lineStrings)
            try {
                pg.addEdge((LineString) reader.read(edgeString));
            } catch (Exception e) {
                System.out.println(e);
            }

        for (LineMergeEdge e : (Collection<LineMergeEdge>) pg.getEdges()) {
            System.out.println(e.getLine());
        }

        for (Node c : (Collection<Node>) pg.getNodes()) {
            System.out.println(c.getCoordinate());
        }

        System.out.println(pg.getEdges());

        return pg;
    }

    @Test
    public void creatingBuffer() {
        LineMergeGraph pg = this.tryingOutPlanarGraph();

        Collection<LineMergeEdge> edges = pg.getEdges();

        // getting all the edges
        Geometry[] geoms = new Geometry[edges.size()];

        int i = 0;
        for (LineMergeEdge e : edges) {
            geoms[i] = (e.getLine());
            i ++;
        }

        Geometry geoCollection = new GeometryFactory().createGeometryCollection(geoms);
        for (LinearRing lr : GeometryParsing.toLinearRing(geoCollection.buffer(.1).union())){
            System.out.println(lr);
        }
    }

    public MultiLineString constructingMultiLineString() {
        // trying coordinate reading

        WKTReader reader = new WKTReader();
        List<Coordinate> coords = new ArrayList<>();
        for (String coor : this.coordinates)
            try {
                Point thisPt = (Point) reader.read(coor);
                coords.add(thisPt.getCoordinate());
                System.out.println(thisPt);
            } catch (Exception e) {
                System.out.println(e);
            }

        // trying the graph
        GeometryFactory gf = new GeometryFactory();

        LineString[] lmg = new LineString[this.connectionGraph.length];

        int i = 0;
        for (int[] e: this.connectionGraph) {
            Coordinate[] coo = new Coordinate[] { coords.get(e[0]), coords.get(e[1])};
            lmg[i] = gf.createLineString(coo);

            i ++;
        }

        return gf.createMultiLineString(lmg);
    }

    @Test
    public void deconstructingMLSIntoGraphSet() {
        MultiLineString mls = this.constructingMultiLineString();
        List<LineString> lss = GeometryParsing.toLineString(mls);
        List<Coordinate> css = new ArrayList<>();

        // get all the points
        Coordinate[] allCss = mls.getCoordinates();

        // filter the points that are already there
        for (Coordinate c : allCss) {
            boolean isAlready = false;
            for (Coordinate c_i : css) {
                if (c_i.distance(c) < GeomBaseFactories.distanceTolerance) {
                    isAlready = true;
                    break;
                }
            }

            if (!isAlready) {
                css.add(c);
                System.out.println(c);
            }
        }

        // replacing the lineString coordinates by the correct indexes of the points in the css list
        List<List<Integer>> graph = new ArrayList<>();
        for (LineString ls : lss) {
            List<Integer> localIndex = new ArrayList<>();
            Coordinate[] coords = ls.getCoordinates();
            for (Coordinate coor : coords){
                System.out.println(coor);
                int i = 0;
                for (Coordinate c : css) {
                    if (coor.distance(c) < GeomBaseFactories.distanceTolerance) {
                        localIndex.add(i);
                        System.out.println(" - " + i + " - " + c);
                        break;
                    }
                    i ++;
                }
            }

            graph.add(localIndex);
        }

        System.out.println(graph.toString() + " - size: " + graph.size() );
    }

    @Test
    public void fullProcess(){
        // 1. creating a list of list of Integers from the array
        List<List<Integer>> integers = new ArrayList<>();
        for (int[] pair : this.connectionGraph) {
            List<Integer> locList = new ArrayList<>();
            for (Integer v : pair) {
                locList.add(v);
            }
            integers.add(locList);
        }

        //2. creating a list of points from coordinates
        List<Point2D> pts = new ArrayList<>();
        for (Coordinate crd : this.getCoords() ) {
            pts.add(new Point2D.Double(crd.getX(), crd.getY() ) );
        }

        GeomBase network = GeomBaseFactories.initPredefinedNetwork(UUID.randomUUID(), pts, integers, 1.0);
        FrontEndObject feo = new FrontEndObject(network);

        System.out.println(feo.toString() );
    }
}
