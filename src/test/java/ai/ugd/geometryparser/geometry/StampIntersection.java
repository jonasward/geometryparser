package ai.ugd.geometryparser.geometry;

import org.junit.Test;
import org.locationtech.jts.geom.Coordinate;

import java.awt.geom.Point2D;
import java.util.List;

public class StampIntersection {
    Coordinate[] rec = new Coordinate[] {
            new Coordinate(0,0),
            new Coordinate(0, 10),
            new Coordinate(10,10),
            new Coordinate(10, 0),
            new Coordinate(0,0)
    };

    Coordinate[] pg = new Coordinate[] {
            new Coordinate(0,0),
            new Coordinate(5, 10),
            new Coordinate(15,10),
            new Coordinate(10, 0),
            new Coordinate(0,0)
    };

    @Test
    public void rectangleIntersection() {
        GeomBase pg = GeomBaseFactories.initNewPolygon(GeometryParsing.pointsFromCoordinates(this.pg) );
        GeomBase stmp = GeomBaseFactories.initNewStamp(new Point2D.Double(5,0), 1.0);
        GeomBase stmp2 = GeomBaseFactories.initNewStamp(new Point2D.Double(0,0), 1.0);

        List<Coordinate[]> pairs = GeomBooleanOperations.stampIntersectingCoordinates(stmp, pg);
        List<GeomBase> geomBases = GeomBooleanOperations.stampIntersecting(stmp, pg);

        System.out.println(geomBases);

        List<Coordinate[]> pairs2 = GeomBooleanOperations.stampIntersectingCoordinates(stmp2, pg);
        List<GeomBase> geomBases2 = GeomBooleanOperations.stampIntersecting(stmp2, pg);

        System.out.println(geomBases2);
    }

}
