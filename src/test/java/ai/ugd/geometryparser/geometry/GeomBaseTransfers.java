package ai.ugd.geometryparser.geometry;

import ai.ugd.geometryparser.data.DataContainer;
import ai.ugd.geometryparser.parsing.FrontEndObject;
import org.junit.Test;
import org.locationtech.jts.geom.Coordinate;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class GeomBaseTransfers {

    List<GeomType> geoTypes = new ArrayList<GeomType>(){
        {
            add(GeomType.POINT);
            add(GeomType.POLYLINE);
            add(GeomType.POLYGON);
            add(GeomType.RECTANGLE);
            add(GeomType.STAMP);
            add(GeomType.SNAKE);
        }};

    Double radiusBuffer = 1.5;

    Coordinate[] coordsOpen = new Coordinate[]{
            new Coordinate(0.0, 0.0),
            new Coordinate(2.0, 3.0),
            new Coordinate(2.0, 5.0),
            new Coordinate(7.0, 4.0),
            new Coordinate(3.0, 8.0),
            new Coordinate(5.0, 7.0)
    };

    Coordinate[] coordsClose = new Coordinate[]{
            new Coordinate(10.0, 0.0),
            new Coordinate(15.0, 12.0),
            new Coordinate(12.0, 16.0),
            new Coordinate(8.0, 10.0),
            new Coordinate(6.0, 8.0),
            new Coordinate(8.0, 6.0),
            new Coordinate(10.0, 0.0)
    };

    Coordinate[] coordsRect = new Coordinate[]{
            new Coordinate(0.0, 0.0),
            new Coordinate(15.0, 0.0),
            new Coordinate(15.0, 16.0),
            new Coordinate(0.0, 16.0),
            new Coordinate(0.0, 0.0)
    };

    Coordinate coordSingle = new Coordinate(5, 10);

    Coordinate coord00 = new Coordinate(-5.0, -2.0);
    Coordinate coord01 = new Coordinate(-5.0, -4.0);

    Coordinate coord10 = new Coordinate(0.0, 0.0);
    Coordinate coord11 = new Coordinate(5.0, 0.0);
    Coordinate coord12 = new Coordinate(5.0, 6.0);

    DataContainer dC = new DataContainer() {{
        addLayer("hcd_test");
    }};
    
    @Test
    public void coordsOpen() {
        System.out.println("================== coords OPEN POINTLIST ==================");

        for (GeomType geoType : geoTypes) {
            System.out.println(geoType.name() + " with coordsOpen: ");
            try {
                GeomBase geoBase = new GeomBase(UUID.randomUUID(), coordsOpen, radiusBuffer, geoType);
//                System.out.println(new FrontEndObject(geoBase, dC.getLayer("hcd_test") ) );
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    @Test
    public void coordsOpenRadius() {
        System.out.println("================== coords OPEN POINTLIST RADIUS ==================");

        for (GeomType geoType : geoTypes) {
            System.out.println(geoType.name() + " with coordsOpen: ");
            try {
                GeomBase geoBase = new GeomBase(UUID.randomUUID(), coordsOpen, radiusBuffer, geoType);
//                System.out.println(new FrontEndObject(geoBase, dC.getLayer("hcd_test") ) );
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    @Test
    public void coordsClose() {
        System.out.println("================== coords CLOSE POINTLIST ==================");

        for (GeomType geoType : geoTypes) {
            System.out.println(geoType.name() + " with coordsClose: ");
            try {
                GeomBase geoBase = new GeomBase(UUID.randomUUID(), coordsClose, geoType);
//                System.out.println(new FrontEndObject(geoBase, dC.getLayer("hcd_test") ) );
            } catch (Exception e) {
                System.out.println(e);
            }
        }

    }

    @Test
    public void coordsRect() {
        System.out.println("================== coords RECT POINTLIST ==================");

        for (GeomType geoType : geoTypes) {
            System.out.println(geoType.name() + " with coordsRect: ");
            try {
                GeomBase geoBase = new GeomBase(UUID.randomUUID(), coordsRect, geoType);
//                System.out.println(new FrontEndObject(geoBase, dC.getLayer("hcd_test") ) );
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    @Test
    public void coordsSinglePoint() {
        System.out.println("================== coords SINGLE POINT ==================");

        for (GeomType geoType : geoTypes) {
            System.out.println(geoType.name() + " with coordSingle: ");
            try {
                GeomBase geoBase = new GeomBase(UUID.randomUUID(), coordSingle, geoType);
//                System.out.println(new FrontEndObject(geoBase, dC.getLayer("hcd_test") ) );
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    @Test
    public void coordsSinglePointRadius() {
        System.out.println("================== coords SINGLE POINT RADIUS ==================");
        Coordinate coordSingle = new Coordinate(5, 10);

        for (GeomType geoType : geoTypes) {
            System.out.println(geoType.name() + " with coordSingle: ");
            
            try {
                GeomBase geoBase = new GeomBase(UUID.randomUUID(), coordSingle, radiusBuffer, geoType);
//                System.out.println(new FrontEndObject(geoBase, dC.getLayer("hcd_test") ) );
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    @Test
    public void coordsDoublePoint() {
        System.out.println("================== coords DOUBLE POINT ==================");

        for (GeomType geoType : geoTypes) {
            System.out.println(geoType.name() + " with coordSingle: ");
            try {
                GeomBase geoBase = new GeomBase(UUID.randomUUID(), coord00, coord01, geoType);

//                System.out.println(new FrontEndObject(geoBase, dC.getLayer("hcd_test") ) );
                for (GeomType geoTypeBis : geoTypes) {
                    System.out.println("\t" + geoType.name() + " translating to " + geoTypeBis.name());
                    try {
                        GeomBasePrimaryTranslators.translate(geoBase, geoTypeBis);
                    } catch (Exception e) {
                        System.out.println("\t\t" + e);
                    }
                }
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    @Test
    public void coordsTriplePoint() {
        System.out.println("================== coords TRIPLE POINT ==================");

        for (GeomType geoType : geoTypes) {
            System.out.println(geoType.name() + " with coordSingle: ");
            try {
                GeomBase geoBase = new GeomBase(UUID.randomUUID(), coord10, coord11, coord12, geoType);

//                System.out.println(new FrontEndObject(geoBase, dC.getLayer("hcd_test") ) );
                for (GeomType geoTypeBis : geoTypes) {
                    System.out.println("\t" + geoType.name() + " translating to " + geoTypeBis.name());
                    try {
                        GeomBasePrimaryTranslators.translate(geoBase, geoTypeBis);
                    } catch (Exception e) {
                        System.out.println("\t\t" + e);
                    }
                }
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    @Test
    public void checkingExporting() {
        System.out.println("================== outparsing BACK END ==================");

        GeomBase point = new GeomBase(UUID.randomUUID(), coordSingle, GeomType.POINT);
        System.out.println(point);
        System.out.println(new FrontEndObject(point));
        GeomBase polyline = new GeomBase(UUID.randomUUID(), coordsOpen, GeomType.POLYLINE);
        System.out.println(polyline);
        System.out.println(new FrontEndObject(polyline));
        GeomBase polygon = new GeomBase(UUID.randomUUID(), coordsClose, GeomType.POLYGON);
        System.out.println(polygon);
        System.out.println(new FrontEndObject(polygon));
        GeomBase rectangle = new GeomBase(UUID.randomUUID(), coordsRect, GeomType.RECTANGLE);
        System.out.println(rectangle);
        System.out.println(new FrontEndObject(rectangle));
        GeomBase stamp = new GeomBase(UUID.randomUUID(), coordSingle, radiusBuffer, GeomType.STAMP);
        System.out.println(stamp);
        System.out.println(new FrontEndObject(stamp));
        GeomBase block = new GeomBase(UUID.randomUUID(), coordsClose, GeomType.BLOCK);
        System.out.println(block);
        System.out.println(new FrontEndObject(block));
        GeomBase snake = new GeomBase(UUID.randomUUID(), coordsOpen, radiusBuffer, GeomType.SNAKE);
        System.out.println(snake);
        System.out.println(new FrontEndObject(snake));
    }

    @Test
    public void createJSON() {
        System.out.println("================== creating JSON ==================");

        GeomBase point = new GeomBase(UUID.randomUUID(), coordSingle, GeomType.POINT);
        System.out.println(point);
        System.out.println(new FrontEndObject(point).toJSON());
        GeomBase polyline = new GeomBase(UUID.randomUUID(), coordsOpen, GeomType.POLYLINE);
        System.out.println(polyline);
        System.out.println(new FrontEndObject(polyline).toJSON());
        GeomBase polygon = new GeomBase(UUID.randomUUID(), coordsClose, GeomType.POLYGON);
        System.out.println(polygon);
        System.out.println(new FrontEndObject(polygon).toJSON());
        GeomBase rectangle = new GeomBase(UUID.randomUUID(), coordsRect, GeomType.RECTANGLE);
        System.out.println(rectangle);
        System.out.println(new FrontEndObject(rectangle).toJSON());
        GeomBase stamp = new GeomBase(UUID.randomUUID(), coordSingle, radiusBuffer, GeomType.STAMP);
        System.out.println(stamp);
        System.out.println(new FrontEndObject(stamp).toJSON());
        GeomBase block = new GeomBase(UUID.randomUUID(), coordsClose, GeomType.BLOCK);
        System.out.println(block);
        System.out.println(new FrontEndObject(block).toJSON());
        GeomBase snake = new GeomBase(UUID.randomUUID(), coordsOpen, radiusBuffer, GeomType.SNAKE);
        System.out.println(snake);
        System.out.println(new FrontEndObject(snake).toJSON());
    }

    @Test
    public void checkingBooleans() {
        System.out.println("================== outparsing BACK END ==================");
        
        
        
    }
}
