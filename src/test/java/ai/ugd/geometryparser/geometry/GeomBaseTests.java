package ai.ugd.geometryparser.geometry;

import org.junit.Test;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class GeomBaseTests {
    @Test
    public void Initialization() {
        System.out.println("===== [TEST] - GeomBaseTests.Initialization =====");
        Coordinate[] coordinateArray = new Coordinate[]{
            new Coordinate(0, 0),
            new Coordinate(10, 10),
            new Coordinate(10, 0),
            new Coordinate(0, 0)
        };

        try {
            GeomBase geoBase = new GeomBase(UUID.randomUUID(), coordinateArray, GeomType.POLYGON);
            System.out.println(geoBase);
        } catch (Exception e) {
            ;
        }

    }

    @Test
    public void RhinoGeometriesSelfValidity(){
        System.out.println("===== [TEST] - GeomBaseTests.RhinoGeometriesSelfValidity =====");
        List<String> dataList = new ArrayList<>();
        dataList.add("LINEARRING (-17.3884087555 5.15733993566, -14.2568368219 58.9159914623, 40.2847076881 77.9663873916, 69.5127124016 75.6177084414, 104.220967999 52.6528475951, 103.177110688 -21.7219858276, 70.5565697128 -32.4215232674, -0.947656104161 -16.5026992717, -17.3884087555 5.15733993566)");
        dataList.add("LINESTRING (-104.915737312 48.8400890733, -136.350149386 139.401133383, 28.3062948132 176.074614136, 86.6844886657 172.332422223, -21.8390768293 87.7588849749, -80.2172706817 102.727652629, -159.551739250 63.0604183450)");
        dataList.add("LINESTRING (438.992590550 200.845779455, 382.111273463 325.086550987, 470.427002624 396.936635728, 573.711499440 351.281894382, 189.870814628 152.552804173, 492.880154106 131.241009862, 530.302073242 247.248959184, 394.086287586 210.575478430)");
        dataList.add("LINEARRING (293.119228878 -286.758246813, 296.596559393 -145.926360917, 440.905775805 -72.9024200819, 559.135013347 0.121520752961, 639.113615214 -199.824983914, 635.636284698 -352.827526616, 414.825796936 -370.214179195, 300.073889909 -356.304857132, 293.119228878 -286.758246813)");
        dataList.add("LINEARRING (-320.639500522 -297.685402385, -238.922233397 -221.184131034, -59.8397118258 -181.194830100, 82.7308393280 -275.082754031, 101.856157166 -424.607966217, -98.0903475012 -408.959978895, -348.458144649 -429.823961991, -381.492784551 -372.448008477, -320.639500522 -297.685402385)");
        dataList.add("LINEARRING (-320.639500522 -297.685402385, -238.922233397 -221.184131034, -59.8397118258 -181.194830100, 82.7308393280 -275.082754031, 101.856157166 -424.607966217, -98.0903475012 -408.959978895, -98.0903475012 -408.959978895, -98.0903475012 -408.959978895, -348.458144649 -429.823961991, -381.492784551 -372.448008477, -320.639500522 -297.685402385)");

        List<String> failures = new ArrayList<>();
        failures.add("valid");
        failures.add("invalid - self intersection");
        failures.add("invalid - self intersection");
        failures.add("valid");
        failures.add("valid");
        failures.add("invalid - double points");

        List<GeomType> geomTypes = new ArrayList<>();
        geomTypes.add(GeomType.POLYGON);
        geomTypes.add(GeomType.POLYLINE);
        geomTypes.add(GeomType.POLYLINE);
        geomTypes.add(GeomType.POLYGON);
        geomTypes.add(GeomType.POLYGON);
        geomTypes.add(GeomType.POLYGON);

        List<GeomBase> geoBases = new ArrayList<>();
        WKTReader reader = new WKTReader();
        int idx = 0;
        for (String geoData : dataList){
            try {
                Coordinate[] coordArray = reader.read(geoData).getCoordinates();
//                List<Point2D> pts = new ArrayList<>();
//                for (Coordinate coor : coordArray){
//                    pts.add(new Point2D.Double(coor.x, coor.y));
//                }
                try {
                    GeomBase geoBase = new GeomBase(UUID.randomUUID(), coordArray, geomTypes.get(idx));
                    System.out.println(geoBase + " - should be: " + failures.get(idx) );

                    geoBases.add(geoBase);
                } catch ( Exception e ) {
                    ;
                }



            } catch (ParseException e) {
                System.out.println("[INFO] - GeomBaseTests.RhinoGeometries - failed to parse geom: " + geoData);
            }
            idx ++;
        }
    }

    @Test
    public void RhinoGeometriesLRIntersectionValidity(){
        System.out.println("===== [TEST] - GeomBaseTests.RhinoGeometriesLRIntersectionValidity =====");
        List<String> dataList = new ArrayList<>();
        dataList.add("LINEARRING (-320.639500522 -297.685402385, -238.922233397 -221.184131034, -59.8397118258 -181.194830100, 82.7308393280 -275.082754031, 101.856157166 -424.607966217, -98.0903475012 -408.959978895, -348.458144649 -429.823961991, -381.492784551 -372.448008477, -320.639500522 -297.685402385)");
        dataList.add("LINEARRING (-32.8308194851 -535.894810625, -29.3534889691 -395.062924729, 114.955727443 -322.038983894, 233.184964985 -249.015043060, 313.163566852 -448.961547727, 309.686236336 -601.964090428, 88.8757485731 -619.350743008, -25.8761584532 -605.441420944, -32.8308194851 -535.894810625)");

        List<String> failures = new ArrayList<>();
        failures.add("valid");
        failures.add("valid");

        List<GeomType> geomTypes = new ArrayList<>();
        geomTypes.add(GeomType.POLYGON);
        geomTypes.add(GeomType.POLYGON);

        List<GeomBase> geoBases = new ArrayList<>();
        WKTReader reader = new WKTReader();
        int idx = 0;
        for (String geoData : dataList){
            try {
                Coordinate[] coordArray = reader.read(geoData).getCoordinates();
//                List<Point2D> pts = new ArrayList<>();
//                for (Coordinate coor : coordArray){
//                    pts.add(new Point2D.Double(coor.x, coor.y));
//                }
                try {
                    GeomBase geoBase = new GeomBase(UUID.randomUUID(), coordArray, geomTypes.get(idx));
                    System.out.println(geoBase + " - should be: " + failures.get(idx) );

                    geoBases.add(geoBase);

                } catch ( Exception e ) {
                    System.out.println(e);
                }

            } catch (ParseException e) {
                System.out.println("[INFO] - GeomBaseTests.RhinoGeometriesLRIntersectionValidity - failed to parse geom: " + geoData);
            }
            idx ++;
        }

        failures.set(0, "invalid - intersection");
        failures.set(1, "invalid - intersection");

//        geoBases.get(0).checkIfIntersects(geoBases.get(1));
//        geoBases.get(0).checkIfNeighbours(geoBases.get(1));
//        geoBases.get(0).checkIfContained(geoBases.get(1));
//        geoBases.get(0).checkIfEndsCovered(geoBases.get(1));

        idx = 0;
        for (GeomBase geoBase : geoBases){
            System.out.println(geoBase + " - should be: " + failures.get(idx) );
            idx ++;
        }
    }
}
