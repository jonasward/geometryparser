package ai.ugd.geometryparser.data;

import ai.ugd.geometryparser.geometry.GeomBase;
import ai.ugd.geometryparser.geometry.GeomBaseFactories;
import org.junit.Test;

import java.awt.geom.Point2D;
import java.util.Arrays;
import java.util.List;

public class DataCollector {
    DataContainer forTest = new DataContainer();

    private void addLayersAndObjects() {
        this.forTest.addLayer("0");
        this.forTest.addLayer("1");
        this.forTest.addLayer("2");

        this.forTest.getLayer("0").addGeoBase(GeomBaseFactories.initNewPoint(new Point2D.Double(1,0)));
        this.forTest.getLayer("0").addGeoBase(GeomBaseFactories.initNewStamp(new Point2D.Double(1,0), 1.));

        this.forTest.getLayer("1").addGeoBase(GeomBaseFactories.initNewStamp(new Point2D.Double(1,0), 2.));
        this.forTest.getLayer("1").addGeoBase(GeomBaseFactories.initNewStamp(new Point2D.Double(1,0), 3.));

        this.forTest.getLayer("2").addGeoBase(GeomBaseFactories.initNewStamp(new Point2D.Double(1,0), 4.));
        this.forTest.getLayer("2").addGeoBase(GeomBaseFactories.initNewStamp(new Point2D.Double(1,0), 5.));
    }

    @Test
    public void dataCollection() {
        this.addLayersAndObjects();

        LayerDataCollector ldc = new LayerDataCollector(LayerDataCollector.DataCollectorType.OTHERLAYERS,
                Arrays.asList("1", "2") );

        List<GeomBase> others = ldc.getRelatedGeometries(this.forTest.getLayer("0").getGeomBases().get(0) );
        System.out.println(others.toString() );
    }
}
