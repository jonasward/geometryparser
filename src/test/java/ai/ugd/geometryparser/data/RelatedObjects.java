package ai.ugd.geometryparser.data;

import ai.ugd.geometryparser.geometry.GeomBase;
import ai.ugd.geometryparser.geometry.GeomBaseFactories;
import org.junit.Test;

import java.awt.geom.Point2D;
import java.util.List;

public class RelatedObjects {

    @Test
    public void testAddingRelatedObjects() {
        DataContainer dc = new DataContainer();
        dc.addLayer("0");
        Layer layer0 = dc.getLayer("0");
        layer0.addGeoBase(GeomBaseFactories.initNewPoint(new Point2D.Double(0,0) ) );
        layer0.addGeoBase(GeomBaseFactories.initNewPoint(new Point2D.Double(1,1) ) );

        List<GeomBase> objects = layer0.getGeomBases();
        GeomBase object0 = objects.get(0);
        GeomBase object1 = objects.get(1);

        object0.addRelatedObjects(object1);
        object1.addRelatedObjects(object0);
    }
}
