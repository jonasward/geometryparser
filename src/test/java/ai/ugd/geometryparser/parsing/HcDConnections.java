package ai.ugd.geometryparser.parsing;

import ai.ugd.geometryparser.data.DataContainer;
import ai.ugd.geometryparser.data.Layer;
import ai.ugd.geometryparser.data.LayerBooleanOperations;
import org.junit.Test;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;

import java.util.List;

public class HcDConnections {
//    @Test
//    public void booleanOperationsE() {
//        DXFParser parser = new DXFParser("src/test/resource/test_data/boolean_operations/boolean_e.dxf");
//
//        parser.initializeLayers(ParsingConstants.hcdParsing);
//        parser.parseLayers();
//        DataContainer data = parser.getDataContainer();
//        System.out.println(data);
//
//        List<Layer> layers = data.getLayers();
//        List<LinearRing> lrs = LayerBooleanOperations.booleanUnion(layers);
//
//        for (LinearRing lr : lrs) {
//            System.out.println(lr);
//        }
//
//        System.out.println("[INFO] - GeoParse.KabejaTest.booleanOperationsC - LRs: " + lrs.size());
//    }

//    @Test
//    public void booleanOperationsETrimming() {
//        DXFParser parser = new DXFParser("src/test/resource/test_data/boolean_operations/boolean_e.dxf");
//
//        parser.initializeLayers(ParsingConstants.hcdParsing);
//        parser.parseLayers();
//        DataContainer data = parser.getDataContainer();
//        System.out.println(data);
//
//        List<Layer> layers = data.getLayers();
//        List<LinearRing> lrs = LayerBooleanOperations.booleanUnion(layers);
//        List<LineString> lss = LayerBooleanOperations.curveTrimmingImplicit(data.getLayer("hcd_building_outline"), data.getLayer("hcd_connections"));
//
//        for (LinearRing lr : lrs) {
//            System.out.println(lr);
//        }
//
//        for (LineString ls : lss) {
//            System.out.println(ls);
//        }
//
//        System.out.println("[INFO] - GeoParse.KabejaTest.booleanOperationsC - LRs: " + lrs.size());
//    }

    @Test
    public void booleanOperationsE2Trimming() {
        DXFParser parser = new DXFParser("src/test/resource/test_data/boolean_operations/boolean_e_4.dxf", ParsingConstants.hcdDXFImportLayers);
        
        parser.parseLayers();
        DataContainer data = parser.getDataContainer();
        System.out.println(data);

        List<Layer> layers = data.getLayers();
        List<LinearRing> lrs = LayerBooleanOperations.booleanUnion(layers);
        List<LineString> lss = LayerBooleanOperations.curveTrimmingImplicit(data.getLayer("hcd_building_outline"), data.getLayer("hcd_connections"));

        for (LinearRing lr : lrs) {
            System.out.println(lr);
        }

        for (LineString ls : lss) {
            System.out.println(ls);
        }

        System.out.println("[INFO] - GeoParse.KabejaTest.booleanOperationsC - LRs: " + lrs.size());
    }
}
