package ai.ugd.geometryparser.parsing;

import ai.ugd.geometryparser.data.DataContainer;
import org.junit.Test;

import java.awt.geom.Point2D;
import java.util.HashMap;
import java.util.List;

public class Exporting {
    @Test
    public void runKabeja(){
        DXFParser parser = new DXFParser("src/test/resource/test_data/bla_5.dxf", ParsingConstants.hcdDXFImportLayers);
        
        parser.parseLayers();
        DataContainer data = parser.getDataContainer();
        System.out.println(data);
        HashMap<String, List<List<Point2D>>> namedLists = Outparser.toNamedPointListsExternalOfGeos(data);
        System.out.println(namedLists.keySet());
        System.out.println(namedLists.values());
    }
}
