package ai.ugd.geometryparser.parsing;

import ai.ugd.geometryparser.data.DataContainer;
import ai.ugd.geometryparser.data.Layer;
import ai.ugd.geometryparser.geometry.GeomBase;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class FrontEndObjectTesting {
    List<List<Double>> aDoubleList = new ArrayList<>();

    public static List<List<Double>> run() {
        List<List<Double>> outputList = new ArrayList<>(Arrays.asList(
            new ArrayList<>(Arrays.asList(.1, .5)),
            new ArrayList<>(Arrays.asList(1.1, 2.))
        ));

        return outputList;
    }

    public static List<List<Double>> run2() {
        List<List<Double>> outputList = new ArrayList<>(Arrays.asList(
                new ArrayList<>(Arrays.asList(1.1, 1.5)),
                new ArrayList<>(Arrays.asList(2.1, 3.))
        ));

        return outputList;
    }

    @Test
    public void testStamp() {
        FrontEndObject feo = new FrontEndObject();

        feo.radius = 1.0;
        feo.type = "STAMP";
        feo.vertices = FrontEndObjectTesting.run();
        feo.layer = "hcd_something";

        DataContainer dc = new DataContainer();

        feo.writeToDataContainer(dc);

        return;
    }

    @Test
    public void testDistance() {
        FrontEndObject feoA = new FrontEndObject();

        feoA.uuid = UUID.randomUUID();
        feoA.radius = 1.0;
        feoA.type = "STAMP";
        feoA.vertices = FrontEndObjectTesting.run();
        feoA.layer = "hcd_something";

        DataContainer dc = new DataContainer();

        feoA.writeToDataContainer(dc);

        FrontEndObject feoB = new FrontEndObject();

        feoB.uuid = UUID.randomUUID();
        feoB.radius = 1.0;
        feoB.type = "STAMP";
        feoB.vertices = FrontEndObjectTesting.run2();
        feoB.layer = "hcd_something";

        feoB.writeToDataContainer(dc);

        Layer hcd_something = dc.getLayer("hcd_something");
        List<GeomBase> geomBases = hcd_something.getGeomBases();
        double ptDistance = geomBases.get(0).getInternalGeo().distance(geomBases.get(1).getInternalGeo() );
        double boundaryDistance = geomBases.get(0).getExternalGeo().distance(geomBases.get(1).getExternalGeo() );
        double distance = geomBases.get(0).distanceTo(geomBases.get(1) );

        return;
    }
}
