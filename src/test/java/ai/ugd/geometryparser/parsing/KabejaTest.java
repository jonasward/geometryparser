package ai.ugd.geometryparser.parsing;

import ai.ugd.geometryparser.data.DataContainer;
import ai.ugd.geometryparser.data.Layer;
import ai.ugd.geometryparser.data.LayerBooleanOperations;
import org.junit.Test;
import org.locationtech.jts.geom.LinearRing;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class KabejaTest {
    @Test
    public void runKabeja(){
        DXFParser parser = new DXFParser("src/test/resource/test_data/bla_5.dxf", ParsingConstants.hcdDXFImportLayers);

        parser.parseLayers();
        DataContainer data = parser.getDataContainer();
        System.out.println(data);

        List<FrontEndObject> fEOs = Outparser.frontEndObjectsParser(data);
        System.out.println("fEOs size : " + fEOs.size());
        for (FrontEndObject feo : fEOs) {
            System.out.println(feo.layer + " - " + feo.type + " - " + feo.vertices.size() );
        }
    }

    @Test
    public void booleanOperationsA() {
        DXFParser parser = new DXFParser("src/test/resource/test_data/boolean_operations/boolean_a.dxf", ParsingConstants.hcdDXFImportLayers);

        parser.parseLayers();
        DataContainer data = parser.getDataContainer();
        System.out.println(data);

        List<Layer> layers = data.getLayers();
        List<LinearRing> lrs = LayerBooleanOperations.booleanUnion(layers);

        System.out.println("[INFO] - GeoParse.KabejaTest.booleanOperationsA - LRs: " + lrs.size());
    }

    @Test
    public void booleanOperationsB() {
        DXFParser parser = new DXFParser("src/test/resource/test_data/boolean_operations/boolean_b.dxf", ParsingConstants.hcdDXFImportLayers);

        parser.parseLayers();
        DataContainer data = parser.getDataContainer();
        System.out.println(data);

        List<Layer> layers = data.getLayers();
        List<LinearRing> lrs = LayerBooleanOperations.booleanUnion(layers);

        System.out.println("[INFO] - GeoParse.KabejaTest.booleanOperationsB - LRs: " + lrs.size());
    }

    @Test
    public void booleanOperationsC() {
        DXFParser parser = new DXFParser("src/test/resource/test_data/boolean_operations/boolean_c.dxf", ParsingConstants.hcdDXFImportLayers);

        parser.parseLayers();
        DataContainer data = parser.getDataContainer();
        System.out.println(data);

        List<Layer> layers = data.getLayers();
        List<LinearRing> lrs = LayerBooleanOperations.booleanUnion(layers);

        for (LinearRing lr : lrs) {
            System.out.println(lr);
        }

        System.out.println("[INFO] - GeoParse.KabejaTest.booleanOperationsC - LRs: " + lrs.size());
    }

    @Test
    public void booleanOperationsD() {
        DXFParser parser = new DXFParser("src/test/resource/test_data/boolean_operations/boolean_d.dxf", ParsingConstants.hcdDXFImportLayers);
        
        parser.parseLayers();
        DataContainer data = parser.getDataContainer();
        System.out.println(data);

        List<Layer> layers = data.getLayers();
        List<LinearRing> lrs = LayerBooleanOperations.booleanUnion(layers);

        for (LinearRing lr : lrs) {
            System.out.println(lr);
        }

        System.out.println("[INFO] - GeoParse.KabejaTest.booleanOperationsC - LRs: " + lrs.size());
    }

    @Test
    public void dxfToJson() {
        DXFParser parser = new DXFParser("src/test/resource/test_data/Caro_1000mm.dxf");

        parser.parseLayers();
        DataContainer data = parser.getDataContainer();

        org.json.simple.JSONArray jsa = data.toJSON();
        try {
            FileWriter myObj = new FileWriter("src/test/resource/test_data/Caro_1000mm.dxf.json");
            myObj.write(jsa.toJSONString());
            myObj.flush();
            myObj.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}
