package ai.ugd.geometryparser.data;

import ai.ugd.geometryparser.geometry.GeomBase;
import ai.ugd.geometryparser.parsing.FrontEndObject;
import org.json.simple.JSONArray;

import java.util.*;

public class DataContainer {
    private static String globalLayerName = "defaultGlobalLayer";

    private HashMap<String, Layer> layers = new HashMap<>();
    HashMap<UUID, Layer> uuids = new HashMap<>();
    Layer globalLayer;

    private boolean isSingle = false;
    public LayerGeomParsingLogic parsingLogic = LayerGeomParsingLogic.DEFAULT;

    public DataContainer(){
        this.initGlobalLayer();
    }

    public DataContainer(String masterLayerName){
        this.initGlobalLayer(masterLayerName);
    }

    public DataContainer(List<LayerSettingsObject> layerObjects){
        this.addLayers(layerObjects);
        this.initGlobalLayer();
    }

    public DataContainer(String globalLayerName, List<LayerSettingsObject> layerObjects){
        this.addLayers(layerObjects);
        this.initGlobalLayer(globalLayerName);
    }

//    public void mergeLayer(Layer inputLayer){
//        if (this.layers.keySet().contains(inputLayer.name) ){
//            System.out.println("[INFO] - DXFReader.Importer.DataContainer - already a layer with the name: " + inputLayer.name);
//        } else {
//            this.layers.put(inputLayer.name, inputLayer);
//            this.globalLayer.extendLayer(inputLayer);
//        }
//    }
//
//    public void addLayers(List<Layer> inputLayers){
//        for (Layer subLayer : inputLayers){
//            this.addLayer(subLayer);
//        }
//    }

    private void initGlobalLayer() {
        this.initGlobalLayer(DataContainer.globalLayerName);
    }

    private void initGlobalLayer(String name) {
        this.globalLayer = new Layer(name, this);
    }

    public List<Layer> addLayer(String layerName) {
        this.layers.put(layerName, new Layer(layerName, this));
        return this.getLayers();
    }

    public List<Layer> addLayer(String layerName, LayerGeomParsingLogic parseLogic){
        if (this.layers.get(layerName) == null) {
            System.out.println("[INFO] - DataContainer.addLayer - added layer: " + layerName);
            if (parseLogic == null) {
                this.layers.put(layerName, new Layer(layerName, this, this.parsingLogic));
            } else {
                this.layers.put(layerName, new Layer(layerName, this, parseLogic));
            }
        } else { System.out.println("[INFO] - DataContainer.addLayer - layer: " + layerName + " already exists"); }
        return this.getLayers();
    }
    
    public List<Layer> addLayer(LayerSettingsObject lso) {
        this.layers.put(lso.layerName, new Layer(this, lso) );
        return this.getLayers();
    }

    public List<Layer> addLayersFromStrings(List<String> layerNames) {
        for (String layerName : layerNames){
            this.layers.put(layerName, new Layer(layerName, this));
        }
        return this.getLayers();
    }
    
    public List<Layer> addLayers(List<LayerSettingsObject> layerSettingsObjects) {
        for (LayerSettingsObject lso : layerSettingsObjects) {
            this.addLayer(lso);
        }
        return this.getLayers();
    }

    public List<Layer> addLayers(HashMap<String, LayerGeomParsingLogic> layersWithParseLogics){
        for (String layerName : layersWithParseLogics.keySet()){
            this.addLayer(layerName, layersWithParseLogics.get(layerName));
        }
        return this.getLayers();
    }

    public List<Layer> getLayers(){
        return new ArrayList<>(this.layers.values());
    }

    public Layer getLayer(String layerName){
        Layer layerObject = this.layers.get(layerName);
        if (layerObject == null){
            this.addLayer(layerName);
            System.out.println("[INFO] - DataContainer.getLayer - layer: " + layerName + " didn't exist, so I created it !");
            return this.getLayer(layerName);
//            throw new NullPointerException("This layer doesn't exist in this DataContainer");
        } else {
            return layerObject;
        }
    }

    public Set<String> getLayerNames() {
        return this.layers.keySet();
    }

    public Layer getGlobalLayer() { return this.globalLayer; }
    
    public GeomBase getGeomBase(UUID uuid) {
        return this.globalLayer.getGeomBase(uuid);
    }

    public List<GeomBase> getGeomBase(List<UUID> uuids) {
        List<GeomBase> geoms = new ArrayList<>();
        for (UUID uuid : uuids) {
            geoms.add(this.getGeomBase(uuid) );
        }
        return geoms;
    }
    
    public List<GeomBase> getGeomBases() {
        return this.globalLayer.getGeomBases();
    }

    public void deleteGeomBase(UUID uuid) {
        this.globalLayer.deleteLocal(uuid);
        this.layers.get(uuid).deleteLocal(uuid);
    }

    public void deleteGeomBase(GeomBase geomBase) {
        this.deleteGeomBase(geomBase.getUUID() );
    }

    public void checkValidity() {
        for (GeomBase geo : this.globalLayer.getGeomBases() ) {
            geo.checkValidity();
        }
    }
    
    public boolean isSingle(){
        return this.isSingle;
    }

    // static methods for creation of dataContainer based on different parsing objects
    public static DataContainer singleLayer(String layerName, Layer inputLayer){
        DataContainer dataContainer = new DataContainer(layerName);
        dataContainer.layers.put(dataContainer.globalLayer.getName(), dataContainer.globalLayer);

        dataContainer.isSingle = true;

        return dataContainer;
    }
    
    public static DataContainer fromLayerSettings(String globalLayerName, List<LayerSettingsObject> layerSettingsObjects) {
        DataContainer dataContainer = new DataContainer(globalLayerName);
        for (LayerSettingsObject lso : layerSettingsObjects) {
            dataContainer.addLayer(lso);
        }
        return dataContainer;
    }

    public String toString(){
        List<String> outputList = new ArrayList<>();
        outputList.add("DataContainer object with " + this.layers.size() + " layers in it and a total of:");
        String globalLayerString = this.globalLayer.toString();
        System.out.println( this.globalLayer.getCount() );
        String[] globalLayerStrings = globalLayerString.split("\n");
        if (globalLayerStrings.length > 1) {
            outputList.addAll(Arrays.asList(globalLayerStrings).subList(1, globalLayerStrings.length));
        } else {
            outputList.add("\tno objects at all");
        }
        if (layers.size() > 1) {
            for (Layer thisLayer : this.layers.values()) {
                for (String layerDiscriptor : thisLayer.toString().split("\n") ) {
                    outputList.add("  " + layerDiscriptor);
                }
            }

        } else if (layers.size() == 1){
            outputList.add("There is only one layer");
        } else {
            outputList.add("There are no layers at all");
        }

        return String.join("\n", outputList);
    }

    public JSONArray toJSON() {
        JSONArray jsa = new JSONArray();
        for (GeomBase geom : this.globalLayer.getGeomBases() ) {
            jsa.add(new FrontEndObject(geom).toJSON() );
        }
        return jsa;
    }
}
