package ai.ugd.geometryparser.data;

import ai.ugd.geometryparser.geometry.GeomBase;
import ai.ugd.geometryparser.geometry.GeomBooleanOperations;
import ai.ugd.geometryparser.geometry.GeometryParsing;
import ai.ugd.geometryparser.geometry.GeomType;
import org.locationtech.jts.geom.*;

import java.util.ArrayList;
import java.util.List;

public class LayerBooleanOperations {
    public static List<LinearRing> booleanUnion(List<Layer> layers) {
        GeometryFactory gf = new GeometryFactory();
        List<Geometry> geoms = new ArrayList<>();
        int totalCount = 0;
        int totalGoal = 0;
        for (Layer layer : layers) {
            int layerCount = 0;
            for (GeomBase baseGeo : layer.getGeomBases()) {
                if (baseGeo.getGeomType().isClosed()) {
                    Geometry geo = baseGeo.getExternalGeo();
                    if (geo.getGeometryType().equals(Geometry.TYPENAME_LINEARRING) ) {
                        geoms.add(gf.createPolygon((LinearRing) geo));
                        layerCount++;
                    } else if (geo.getGeometryType().equals(Geometry.TYPENAME_POLYGON) ) {
                        geoms.add(geo);
                        layerCount++;
                    } else {
                        System.out.println("[DEBUG] - GeoParse.LayerBooleanOperations.Union - wasn't able to deal with geo of type : " + geo.getGeometryType());
                    }
                }
            }
            totalCount += layerCount;
            totalGoal += layer.getCount();
            System.out.println("[INFO] - GeoParse.LayerBooleanOperations.Union - Layer: " + layer.getName() + " was able to deal with " + layerCount + " / " + layer.getCount());
        }

        System.out.println("[INFO] - GeoParse.LayerBooleanOperations.Union - Total - was able to deal with " + totalCount + " / " + totalGoal);

        return GeometryParsing.toLinearRing(GeomBooleanOperations.booleanUnion(geoms) );
    }

    public static List<LineString> curveTrimmingImplicit(Layer curve, Layer trimmer) {
        
        List<Geometry> curves = new ArrayList<>();
        List<Geometry> trimmers = new ArrayList<>();
        
        for (GeomBase geom : curve.getGeomBases() ) {
            if ( (geom.isClosed() || geom.getGeomType() == GeomType.POLYLINE) && !( geom.getGeomType() == GeomType.STAMP ) ) {
                curves.add( geom.getExternalGeo() );
            }
        }

        for (GeomBase geom : trimmer.getGeomBases() ) {
            if ( geom.isClosed() ) {
                trimmers.add( geom.getExternalGeo() );
            }
        }
        
        return GeomBooleanOperations.curveTrimming(curves, trimmers);
    }
}
