package ai.ugd.geometryparser.data;

import ai.ugd.geometryparser.geometry.GeomBase;
import ai.ugd.geometryparser.geometry.GeomBaseFactories;
import ai.ugd.geometryparser.geometry.GeomBasePrimaryTranslators;
import ai.ugd.geometryparser.geometry.GeomType;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Polygon;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public enum LayerGeomParsingLogic {
    DEFAULT (Arrays.asList(GeomType.POLYGON, GeomType.POLYLINE, GeomType.RECTANGLE, GeomType.SNAKE, GeomType.BLOCK, GeomType.STAMP, GeomType.POINT, GeomType.NETWORK), false, false, false, false),
    ONLYEXPLICIT (Arrays.asList(GeomType.POLYGON, GeomType.POLYLINE, GeomType.RECTANGLE, GeomType.POINT), false, false, false, false),
    ONLYIMPLICIT (Arrays.asList(GeomType.SNAKE, GeomType.STAMP), false, false, false, false),
    FORCEAREA (Arrays.asList(GeomType.POLYGON, GeomType.POLYLINE, GeomType.RECTANGLE, GeomType.SNAKE, GeomType.BLOCK, GeomType.STAMP), true, false, false, false),
    FORCEAREAONIMPLICIT (Arrays.asList(GeomType.POLYGON, GeomType.POLYLINE, GeomType.RECTANGLE, GeomType.SNAKE, GeomType.BLOCK, GeomType.STAMP), true, true, false, false),
    FORCEPOLYGON (Arrays.asList(GeomType.POLYGON, GeomType.POLYLINE, GeomType.RECTANGLE, GeomType.SNAKE, GeomType.BLOCK, GeomType.STAMP, GeomType.POINT), false, false, false, false),
    ASIMPLICIT (Arrays.asList(GeomType.POLYLINE, GeomType.SNAKE, GeomType.RECTANGLE, GeomType.STAMP, GeomType.POINT), false, false, false, true);

    private static double minimumDistance = .001;   // distance at which two points will be considered as being the same

    private List<GeomType> acceptedTypes;
    private boolean forceArea;                      // POINTs and POLYLINEs are forced into STAMPs and SNAKEs
    private boolean forceAreaImplicits;             // BLOCKs, STAMPs and SNAKEs are always transformed into POLYGONS (combined with forceArea are POINTs and POLYLINEs also transformed into Polygons)
    private boolean forcePolygonize;                // Always tries to interpret POLYLINE as a POLYGON, otherwise like default
    private boolean asImplicit;                     // Transforms the objects found into implicit types (SNAKE, STAMP)

    LayerGeomParsingLogic(List<GeomType> acceptedTypes, boolean forceArea, boolean forceAreaImplicits, boolean forcePolygonize, boolean asImplicit) {
        this.acceptedTypes = acceptedTypes;
        this.forceArea = forceArea;
        this.forceAreaImplicits = forceAreaImplicits;
        this.forcePolygonize = forcePolygonize;
        this.asImplicit = asImplicit;
    }

    public boolean getForceArea() {
        return this.forceArea;
    }

    public double getSnakeBuffer() {
        return GeomBaseFactories.snakeBuffer;
    }

    public void setSnakeBuffer(double snakeBuffer) {
        GeomBaseFactories.snakeBuffer = snakeBuffer;
    }

    public double getRadius() {
        return GeomBaseFactories.circleRadius;
    }

    public void setRadius(double radius) {
        GeomBaseFactories.circleRadius = radius;
    }

    public GeomBase fromCoords(Coordinate[] coords) {
        return this.fromCoords(coords, false);
    }

    public GeomBase fromCoords(Coordinate[] coords, boolean isClosed) {
        GeomBase result = this.coordParser(coords, isClosed);
        if (this.contains(result.getGeomType() ) ){
            return result;
        } else { return null; }
    }

    public GeomBase fromPoints(List<Point2D> pts, boolean isClosed) { return this.fromCoords(GeomBaseFactories.makeCoordinates(pts, isClosed)); }

    public GeomBase fromPoints(List<Point2D> pts) {
        return this.fromCoords(GeomBaseFactories.makeCoordinates(pts));
    }

    private GeomBase coordParser(Coordinate[] coords, boolean isClosed) {
        GeomBase geomBase = this.coordParser(UUID.randomUUID(), coords, isClosed);
        if (this.asImplicit) {
            try {
                return GeomBasePrimaryTranslators.translateToImplicit(geomBase);
            } catch (Exception e) {
                System.out.println(e);
                return null;
            }
        } else {
            return geomBase;
        }
    }

    private GeomBase coordParser(UUID uuid, Coordinate[] coords, boolean isClosed) {
        // if end point is equal to last, presumes POLYGON,
        //     except for if ptCount == 3 -> POLYLINE (with only two points)
        //     or ptCount == 2 -> POINT
        // if not closed presume POLYLINE,
        //     except for if ptCount == 1 -> POINT

        System.out.println("[INFO] - LayerGeometryInparser.fromPointBase - parsing " + coords.length + " points.");

        try {
            if (coords.length > 1) {

                if (coords[0].distance(coords[coords.length - 1]) < LayerGeomParsingLogic.minimumDistance) { // checking whether the first and last point are the same
                    isClosed = true;
                } else if ((isClosed) || this.forcePolygonize) {
                    List<Coordinate> coordsList = new ArrayList<>(Arrays.asList(coords));
                    coordsList.add(coordsList.get(0));
                    coords = coordsList.toArray(coords);
                }

                if (isClosed) {

                    if (coords.length > 3) {
                        System.out.println("[INFO] - LayerGeometryInparser.coordParser - create a Polygon with " + coords.length + "vertexes");
                        coords = Arrays.copyOfRange(coords, 0, coords.length);
                        return new GeomBase(uuid, coords, GeomType.POLYGON);
                    } else if (coords.length == 3) {
                        coords = Arrays.copyOfRange(coords, 1, coords.length);
                        System.out.println("[INFO] - LayerGeometryInparser.coordParser - create a Polyline with " + coords.length + "vertexes");
                        return new GeomBase(uuid, coords, GeomType.POLYLINE);
                    } else { // (coords.length == 2)
                        coords = Arrays.copyOfRange(coords, 1, coords.length);
                        System.out.println("[INFO] - LayerGeometryInparser.coordParser - create a Point with " + coords.length + "vertexes");
                        return new GeomBase(uuid, coords[0], GeomType.POINT);
                    }

                } else {
                    System.out.println("[INFO] - LayerGeometryInparser.coordParser - create a Polyline with " + coords.length + "vertexes");
                    return new GeomBase(uuid, coords, GeomType.POLYLINE);
                }
            } else if (coords.length == 1) {
                System.out.println("[INFO] - LayerGeometryInparser.coordParser - create a Point with " + coords.length + "vertexes");
                return new GeomBase(uuid, coords[0], GeomType.POINT);
            } else {
                System.out.println("[DEBUG] - dealing with a polyline with too few vertexes");
                return null;
            }
        } catch (Exception e) {
            System.out.println("Ran into an error initializing a shape");
            return null;
        }
    }

    public GeomBase fromSinglePoint(Point2D pt) throws Exception {
        return this.fromSingleCoordinate(new Coordinate(pt.getX(), pt.getY()), GeomBaseFactories.circleRadius);
    }

    public GeomBase fromSinglePoint(Point2D pt, double radius) throws Exception {
        return this.fromSingleCoordinate(new Coordinate(pt.getX(), pt.getY()), radius);
    }

    public GeomBase fromSingleCoordinate(Coordinate coor) throws Exception {
        return this.fromSingleCoordinate(coor, GeomBaseFactories.circleRadius);
    }

    public GeomBase fromSingleCoordinate(Coordinate coor, double radius) throws Exception {
        if (this.acceptedTypes.contains(GeomType.STAMP) ) {
            return new GeomBase(UUID.randomUUID(), coor, radius, GeomType.STAMP);
        } else { return null; }
    }

    public GeomBase fromPolygon(Polygon pg, String blockName) {
        if (this.acceptedTypes.contains(GeomType.BLOCK) ) {
            return GeomBase.createBlock(pg, blockName);
        } else { return null; }
    }

    public boolean contains(GeomType geoType){
        return this.acceptedTypes.contains(geoType);
    }
}
