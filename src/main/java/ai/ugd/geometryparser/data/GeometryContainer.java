package ai.ugd.geometryparser.data;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class GeometryContainer {
    public HashMap<String, List<List<Point2D>>> objects;

    public GeometryContainer() {}

    public void addGeometry(String layerName, List<Point2D> pts) {
        if (this.objects.keySet().contains(layerName)) {
            this.objects.get(layerName).add(pts);
        } else {
            List<List<Point2D>> ptsList = new ArrayList<>();
            ptsList.add(pts);
            this.objects.put(layerName, ptsList);
        }
    }

    public Set<String> getLayerNames() {
        return this.objects.keySet();
    }
}
