package ai.ugd.geometryparser.data;

import ai.ugd.geometryparser.geometry.GeomInvalidityType;

import java.util.Arrays;
import java.util.List;

public class LayerValidityCheckLogic {
    public enum LayerValidityTypes {
        NONE(null),
        ALL(Arrays.asList(GeomInvalidityType.SELFINTERSECTS, GeomInvalidityType.ENDSTARTOVERLAP, GeomInvalidityType.DOUBLEPOINTS, GeomInvalidityType.INTERSECTS, GeomInvalidityType.NOTCONTAINED, GeomInvalidityType.NOPARENT, GeomInvalidityType.UNCOVEREDENDS)),
        SPACE(Arrays.asList(GeomInvalidityType.SELFINTERSECTS, GeomInvalidityType.DOUBLEPOINTS, GeomInvalidityType.INTERSECTS)),
        WALL(Arrays.asList(GeomInvalidityType.SELFINTERSECTS, GeomInvalidityType.ENDSTARTOVERLAP, GeomInvalidityType.DOUBLEPOINTS, GeomInvalidityType.INTERSECTS, GeomInvalidityType.NOTCONTAINED)),
        STAMP(Arrays.asList(GeomInvalidityType.NOPARENT) ),
        OBJECT(Arrays.asList(GeomInvalidityType.NOTCONTAINED) ),
        CONNECTION(Arrays.asList(GeomInvalidityType.UNCOVEREDENDS) );

        private List<GeomInvalidityType> invalidities;

        LayerValidityTypes(List<GeomInvalidityType> validitiesToCheck) {
            this.invalidities = validitiesToCheck;
        }

        protected boolean isValidType(GeomInvalidityType geomInvalidityType) {
            return this.invalidities.contains(geomInvalidityType);
        }
    }
    
    LayerValidityTypes layerValidityTypes;
    LayerDataCollector layerValidityReference;

    public LayerValidityCheckLogic(LayerValidityTypes layerValidityTypes, LayerDataCollector layerValidityReference) {
        this.layerValidityTypes = layerValidityTypes;
        this.layerValidityReference = layerValidityReference;
    }
    
    public static final LayerValidityCheckLogic empty() {
        return new LayerValidityCheckLogic(LayerValidityCheckLogic.LayerValidityTypes.NONE, LayerDataCollector.defaultDataCollector);
    }

    public boolean validityToCheck(GeomInvalidityType geomInvalidityType) {
        return (this.layerValidityTypes.isValidType(geomInvalidityType) );
    }

    public LayerDataCollector getLayerValidityReference() {
        return this.layerValidityReference;
    }

    public void setLayerValidityReference(LayerDataCollector layerValidityReference) {
        this.layerValidityReference = layerValidityReference;
    }
}
