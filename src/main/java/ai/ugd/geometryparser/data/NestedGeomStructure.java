package ai.ugd.geometryparser.data;

import ai.ugd.geometryparser.geometry.GeomBase;
import org.locationtech.jts.geom.Geometry;

import java.util.List;

public class NestedGeomStructure extends DataContainer {
    public static String defaultName = "defaultNPL";
    public static String defaultInfiniteName = "defaultInfiniteNGS";
    public static Double defaultBoundaryContainmentBuffer = null;

    private List<NestedGeomStructure> npls;
    private String name = NestedGeomStructure.defaultName;
    private Geometry boundary;

    private boolean isInfinite = false;

    private Geometry boundaryForContainment;
    private Double boundaryContainmentBuffer = NestedGeomStructure.defaultBoundaryContainmentBuffer;

    public NestedGeomStructure(Geometry boundary, String name) {
        this.name = name;
        this.setBoundary(boundary);
    }

    public NestedGeomStructure(Geometry boundary) {
        this.setBoundary(boundary);
    }

    public boolean isInfinite(){
        return this.isInfinite;
    }

    public boolean contains(GeomBase geom) {
        return this.contains(geom.getInternalGeo() );
    }

    public boolean intersects(GeomBase geom) {
        return this.intersects(geom.getInternalGeo() );
    }

    private boolean contains(Geometry otherGeometry) {
        if (this.isInfinite) {
            return true;
        } else {
            return this.boundaryForContainment.contains(otherGeometry);
        }
    }

    private boolean intersects(Geometry otherGeometry) {
        if (this.isInfinite) {
            return false;
        } else {
            return this.boundary.intersects(otherGeometry);
        }
    }

    public void setBoundary(Geometry boundary) {
        if (boundary == null) {
            this.setBoundaryInfinite();
        } else {
            this.boundary = boundary;
            this.isInfinite = false;

            // in case the a boundary buffer has been set, create an extra boundary for containment
            if (!(this.boundaryContainmentBuffer == null) ) {
                this.boundaryForContainment = this.boundary.buffer(this.boundaryContainmentBuffer);
            } else {
                this.boundaryForContainment = this.boundary;
            }
        }
    }

    public void setBoundaryInfinite() {
        this.boundary = null;
        this.isInfinite = true;
    }

    public void setBoundaryContainmentBuffer(Double buffer) {
        this.boundaryContainmentBuffer = buffer;
    }

    public static NestedGeomStructure getInfiniteNGS(String name) {
        return new NestedGeomStructure(null, name);
    }

    public static NestedGeomStructure getInfiniteNGS() {
        return new NestedGeomStructure(null, NestedGeomStructure.defaultInfiniteName);
    }
}
