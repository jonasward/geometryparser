package ai.ugd.geometryparser.data;

public class LayerSettingsObject {
    public String layerName;
    public LayerGeomParsingLogic layerGeomParsingLogic;
    public LayerValidityCheckLogic layerValidityCheckLogic;
    
    public LayerSettingsObject(String layerName, LayerGeomParsingLogic layerGeomParsingLogic, LayerValidityCheckLogic layerValidityCheckLogic) {
        this.layerName = layerName;
        this.layerGeomParsingLogic = layerGeomParsingLogic;
        this.layerValidityCheckLogic = layerValidityCheckLogic;
    }
}
