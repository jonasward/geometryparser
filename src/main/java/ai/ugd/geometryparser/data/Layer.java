package ai.ugd.geometryparser.data;

import ai.ugd.geometryparser.geometry.GeomBase;
import ai.ugd.geometryparser.geometry.GeomType;
import org.locationtech.jts.geom.Coordinate;

import java.awt.geom.Point2D;
import java.util.*;

public class Layer {
    private HashMap<UUID, GeomBase> geometries = new HashMap<>();
    private String layerName;
    private DataContainer dataContainer;
    private LayerGeomParsingLogic parsingLogic = LayerGeomParsingLogic.DEFAULT;
    private LayerValidityCheckLogic validityLogic = LayerValidityCheckLogic.empty();

    public Layer(LayerSettingsObject layerSettings, DataContainer dataContainer) {
        this.layerName = layerSettings.layerName;
        this.parsingLogic = layerSettings.layerGeomParsingLogic;
        this.validityLogic = layerSettings.layerValidityCheckLogic;
        this.dataContainer = dataContainer;
    }

    public Layer(String name, DataContainer dataContainer, LayerGeomParsingLogic parsingLogic){
        this.layerName = name;
        this.dataContainer = dataContainer;
        this.parsingLogic = parsingLogic;
    }

    public Layer(DataContainer dataContainer, LayerSettingsObject lso) {
        this.layerName = lso.layerName;
        this.dataContainer = dataContainer;
        this.parsingLogic = lso.layerGeomParsingLogic;
        this.validityLogic = lso.layerValidityCheckLogic;
    }

    public Layer(String name, DataContainer dataContainer){
        this.layerName = name;
        this.dataContainer = dataContainer;
        this.parsingLogic = dataContainer.parsingLogic;
    }

    public String getName() {
        return this.layerName;
    }

    public int getCount() {
        if (this.geometries == null) {
            return 0;
        } else {
            return this.geometries.size();
        }
    }

    public DataContainer getDataContainer() {
        return this.dataContainer;
    }

    public LayerGeomParsingLogic getParsingLogic() {
        return this.parsingLogic;
    }

    public LayerValidityCheckLogic getValidityLogic() {
        return this.validityLogic;
    }

    public void setValidityLogic(LayerValidityCheckLogic newLayerValidityCheckLogic) {
        this.validityLogic = newLayerValidityCheckLogic;
    }

    public void setName(String layerName){
        this.layerName = layerName;
    }

    public void addGeoBase(GeomBase geomBase){
        if (!(geomBase == null)) {
            geomBase.setLayer(this);
            this.geometries.put(geomBase.getUUID(), geomBase);
            this.dataContainer.globalLayer.geometries.put(geomBase.getUUID(), geomBase);
            this.dataContainer.uuids.put(geomBase.getUUID(), this);
        } else { System.out.println("[INFO] - Layer.addGeoBase - has been handed a null value, ignored"); }
    }

    public void addGeoBases(List<GeomBase> geomBases){
        for (GeomBase geoB: geomBases){
            this.addGeoBase(geoB);
        }
    }

    public GeomBase getGeomBase(UUID uuid) {
        return this.geometries.get(uuid);
    }

    public List<GeomBase> getGeomBase(List<UUID> uuids) {
        List<GeomBase> geoms = new ArrayList<>();
        for (UUID uuid : uuids) {
            geoms.add(this.getGeomBase(uuid) );
        }
        return geoms;
    }

    public List<GeomBase> getGeomBases() {
        return new ArrayList<>(this.geometries.values() );
    }

    public void updateGeomBase(UUID uuid, GeomBase geom) {
        this.getGeomBase(uuid).updateWith(geom);
    }

    public void updateGeomBase(UUID uuid, List<GeomBase> geoms) {
        this.updateGeomBase(uuid, geoms.remove(0) );
        this.addGeoBases(geoms);
    }

    public void deleteGeomBase(UUID uuid) {
        this.dataContainer.deleteGeomBase(uuid);
    }

    void deleteLocal(UUID uuid) {
        this.geometries.remove(uuid);
    }

    public void deleteGeomBase(GeomBase geomBase) {
        this.dataContainer.deleteGeomBase(geomBase.getUUID() );
    }
    
    public void checkValidity() {
        for (GeomBase geo : this.geometries.values() ) {
            geo.checkValidity();
        }
    }

    public void parseIn(Coordinate[] coords) {
        this.addGeoBase( this.getParsingLogic().fromCoords(coords) );
    }

    public void parseIn(Coordinate[] coords, boolean isClosed) {
        this.addGeoBase( this.getParsingLogic().fromCoords(coords, isClosed));
    }

    public void parseIn(List<Point2D> pts) {
        this.addGeoBase(this.getParsingLogic().fromPoints(pts));
    }

    public void parseIn(List<Point2D> pts, boolean isClosed) {
        this.addGeoBase(this.getParsingLogic().fromPoints(pts, isClosed));
    }

    public List<GeomBase> getLinearRings() {
        List<GeomBase> lrs = new ArrayList<>();
        for (GeomBase geo : this.geometries.values()) {
            if (geo.getGeomType() == GeomType.POLYGON){
                lrs.add(geo);
            }
        }
        return lrs;
    }

    public HashMap<GeomType, Integer> geomCounter(){
        List<GeomType> enumList = Arrays.asList(GeomType.class.getEnumConstants());
        HashMap<GeomType, Integer> counter = new HashMap<>();
        for (GeomBase geo: this.geometries.values()){
            GeomType geoType = geo.getGeomType();
            int count = counter.getOrDefault(geoType, 0);
            counter.put(geoType, count + 1);
        }
        return counter;
    }

    public String toString() {
        StringBuilder outputString = new StringBuilder("Layer: " + this.layerName + " has " + this.geometries.size() + " objects in it.");
        HashMap<GeomType, Integer> counter = this.geomCounter();
        for (GeomType gt : counter.keySet()){
            outputString.append("\t\n- ").append(counter.get(gt)).append(" objects of type ").append(gt.name());
        }

        return outputString.toString();
    }
}
