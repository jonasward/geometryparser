package ai.ugd.geometryparser.data;

import ai.ugd.geometryparser.geometry.GeomBase;

import java.security.InvalidParameterException;
import java.util.*;

public class LayerDataCollector {

    public enum DataCollectorType {
        NONE(false),
        OBJECTDEFINED(false),
        OBJECTSONLAYER(false),
        OTHERLAYERS(true),
        OTHERLAYERSANDSELF(true),
        GLOBALLAYER(false);

        private boolean acceptsLayers;

        DataCollectorType(boolean acceptsLayers) {
            this.acceptsLayers = acceptsLayers;
        }

        public boolean acceptsLayers() {
            return this.acceptsLayers;
        }
    }

    List<String> layerNames;
    boolean allowUUIDOverride = false;
    DataCollectorType dataCollectorType;

    public LayerDataCollector(DataCollectorType dataCollectorType) throws InvalidParameterException {
        this.init(dataCollectorType, null, null);
    }

    public LayerDataCollector(DataCollectorType dataCollectorType, String otherLayer) throws InvalidParameterException {
        this.init(dataCollectorType, Arrays.asList(otherLayer), null);
    }

    public LayerDataCollector(DataCollectorType dataCollectorType, String otherLayer, boolean acceptUUIDOverrides) throws InvalidParameterException {
        this.init(dataCollectorType, Arrays.asList(otherLayer), acceptUUIDOverrides);
    }

    public LayerDataCollector(DataCollectorType dataCollectorType, List<String> otherLayers) throws InvalidParameterException {
        this.init(dataCollectorType, otherLayers, null);
    }

    public LayerDataCollector(DataCollectorType dataCollectorType, List<String> otherLayers, boolean acceptUUIDOverrides) throws InvalidParameterException {
        this.init(dataCollectorType, otherLayers, acceptUUIDOverrides);
    }
    
    public static final LayerDataCollector defaultDataCollector = new LayerDataCollector(DataCollectorType.NONE);

    private void init(DataCollectorType dataCollectorType, List<String> otherLayers, Boolean allowUUIDOverride) throws InvalidParameterException {
        this.dataCollectorType = dataCollectorType;

        if ( !(this.dataCollectorType.acceptsLayers() ) && !(otherLayers == null) ) {
            throw new InvalidParameterException("The LayerValidityReference " + this.dataCollectorType.name() + " doesn't accept layer names");
        } else if (this.dataCollectorType.acceptsLayers() && (otherLayers == null) ) {
            throw new InvalidParameterException("The LayerValidityReference " + this.dataCollectorType.name() + " requires you to give layer names");
        }

        this.allowUUIDOverride = allowUUIDOverride != null && allowUUIDOverride;

        this.layerNames = otherLayers;
    }

    public DataCollectorType getDataCollectorType() {
        return this.dataCollectorType;
    }

    public void setDataCollectorType(DataCollectorType layerValidityReference) {
        this.dataCollectorType = layerValidityReference;
    }

    public boolean getAllowUUIDOverride() {
        return this.allowUUIDOverride;
    }

    public void setAllowUUIDOverride(boolean bool) {
        this.allowUUIDOverride = bool;
    }

    public List<GeomBase> getRelatedGeometries(GeomBase geomBase) {
        if (this.allowUUIDOverride && geomBase.getRelatedObjects().size() > 0 ) {
            return this.getRelatedGeometriesOverrideProof(geomBase, DataCollectorType.OBJECTSONLAYER );
        } else {
            return this.getRelatedGeometriesOverrideProof(geomBase, this.dataCollectorType );
        }
    }

    private List<GeomBase> getRelatedGeometriesOverrideProof(GeomBase geomBase, DataCollectorType layerValidityReference) {
        List<GeomBase> relatedGeoms = new ArrayList<>();
        DataContainer dc = geomBase.getDataContainer();

        switch (layerValidityReference) {
            case OBJECTDEFINED:
                relatedGeoms = geomBase.getRelatedObjects();
                break;
            case OBJECTSONLAYER:
                relatedGeoms = geomBase.getParentLayer().getGeomBases();
                break;
            case OTHERLAYERS:
                for (String layerName : this.layerNames) {
                    relatedGeoms.addAll( dc.getLayer(layerName).getGeomBases() );
                }
                break;
            case OTHERLAYERSANDSELF:
                for (String layerName : this.layerNames) {
                    relatedGeoms.addAll( dc.getLayer(layerName).getGeomBases() );
                }
                relatedGeoms.addAll( geomBase.getParentLayer().getGeomBases() );
                break;
            case GLOBALLAYER:
                relatedGeoms.addAll( geomBase.getDataContainer().getGeomBases() );
                break;
            case NONE:
                break;
        }

        // filter self
        HashMap<UUID, GeomBase> filtering = new HashMap<>();
        for (GeomBase geom: relatedGeoms) {
            filtering.put(geom.getUUID(), geom);
        }

        filtering.remove(geomBase.getUUID() );

        return new ArrayList<>(filtering.values() );
    }
}
