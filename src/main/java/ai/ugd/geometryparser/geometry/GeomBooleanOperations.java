package ai.ugd.geometryparser.geometry;

import org.locationtech.jts.geom.*;
import org.locationtech.jts.operation.distance.DistanceOp;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class GeomBooleanOperations {
    public static Geometry booleanUnion(List<Geometry> geoms) {
        // create a boolean union of closed JTS geometries ( (Multi)-Polygons )
        GeometryFactory gf = new GeometryFactory();

        int totalCount = geoms.size();
        Geometry outputGeo;
        if (totalCount > 1) {
            Geometry[] geos = new Geometry[totalCount];
            for (int i = 0; i < totalCount; i ++){
                geos[i] =  geoms.get(i);
            }
            Geometry geoCollection = gf.createGeometryCollection( geos );
            outputGeo = geoCollection.union();
        } else if (totalCount == 1){
            outputGeo = geoms.get(0);
        } else {
            return null;
        }
        
        return outputGeo;
    }

    public static Geometry booleanUnion(List<Geometry> geoms, float bufferValue) {
        // create a boolean union of closed JTS geometries ( (Multi)-Polygons ), with an extra added (negative) buffer
        GeometryFactory gf = new GeometryFactory();

        int totalCount = geoms.size();
        Geometry outputGeo;
        if (totalCount > 1) {
            Geometry[] geos = new Geometry[totalCount];
            for (int i = 0; i < totalCount; i ++){
                geos[i] =  geoms.get(i).buffer(bufferValue);
            }
            Geometry geoCollection = gf.createGeometryCollection( geos );
            outputGeo = geoCollection.union();
        } else if (totalCount == 1){
            outputGeo = geoms.get(0);
        } else {
            return null;
        }

        return outputGeo;
    }

    public static List<LineString> curveTrimming(List<Geometry> toTrim, List<Geometry> trims) {
        // the trims-curves should always be polygons, therefore they should be transferred into

        if (trims.isEmpty() || toTrim.isEmpty()) {
            return new ArrayList<>();
        }

        // step one: union the "trim" geometries
        List<Polygon> trimPolygons = GeometryParsing.toPolygon( GeomBooleanOperations.booleanUnion(trims) );

        // step two: deconstruct "toTrims" geometries into linear elements (lineString / linearRings)
        List<LineString> toTrimLineStrings = new ArrayList<>();
        for (Geometry geo : toTrim) {
            toTrimLineStrings.addAll(GeometryParsing.toLineString(geo));
        }
        
        List<LineString> outputLineStrings = new ArrayList<>();
        for (LineString lr : toTrimLineStrings) {
            for (Polygon pg : trimPolygons) {
                if (pg.intersects(lr) ) {
                    outputLineStrings.addAll(GeometryParsing.toLineString(lr.intersection(pg)));
                }
            }
        }

        return outputLineStrings;
    }

    private static LineString stampIntersecting(LineString ls, Geometry wall) {
        // method for moving a line-string onto a wall geometry

        // if the external geometry of the stamp is a lineString, every of its coordinates will be projected
        // onto the wallObject nearest to it
        
        GeometryFactory gf = new GeometryFactory();
        Coordinate[] coords = ls.getCoordinates();

        for (Coordinate coor : coords ) {
            System.out.println(coor);
            DistanceOp distOp = new DistanceOp(gf.createPoint(coor), wall);
            coor = distOp.nearestLocations()[0].getCoordinate();
            System.out.println(coor);
        }
        
        return gf.createLineString(coords);
    }

    public static List<Coordinate[]> stampIntersectingCoordinates(GeomBase stamp, GeomBase wallObject ) {
        List<LineString> resultLss = new ArrayList<>();
        List<Coordinate[]> outputCoords = new ArrayList<>();

        if (stamp.getGeomType() == GeomType.STAMP) {
            // 1. making sure the geoStamp is a polygon or can be turned into one
            Geometry stampGeo = stamp.getExternalGeo();
            if (!(stampGeo.getGeometryType() == Geometry.TYPENAME_POLYGON)  ) {
                stampGeo = GeometryParsing.toPolygon(stampGeo).get(0);
            }

            // 2. making sure the wallObject is a linestring or can be turned into one
            List<LineString> wallGeo = GeometryParsing.toLineString(wallObject.getExternalGeo() );

            // 3. making the intersections
            for (LineString ls : wallGeo) {
                Geometry intersectionResult = stampGeo.intersection(ls);
                resultLss.addAll( GeometryParsing.toLineString(intersectionResult) );
            }

        } else {
            System.out.println("[INFO] - ai.ugd.geometryParser.GeomBooleanOperations.stampIntersecting - this is not a stamp but a " + stamp.getGeomType().name() );
            return outputCoords;
        }
        
        // deconstructing linestring with more than 2 coordinates
        for (LineString ls : resultLss) {
            outputCoords.addAll(GeometryHelpers.splitLineStringsIntoCoordPairs(ls, GeomBaseFactories.stampLineStringShortening));
        }

        return outputCoords;
    }

    public static List<GeomBase> stampIntersecting(GeomBase stamp, GeomBase wallObject) {
        List<Coordinate[]> coordLists = GeomBooleanOperations.stampIntersectingCoordinates(stamp, wallObject);

        // step 3: (re) initializing the geometry
        List<GeomBase> stamps = new ArrayList<>();

        Coordinate[] cs0 = coordLists.get(0);
        stamps.add(stamp);

        // step 4: updating internal geometry of the original stamp in case multiple new outputs (only in the case multiple geometries were created, otherwise the geometry would shrink indefinetly)
        if (coordLists.size() > 1) {
            stamp.setRadius(cs0[0].distance(cs0[1]) * .5);
            stamp.updateStampPosition(new Coordinate((cs0[0].x + cs0[1].x) * .5, (cs0[0].y + cs0[1].y) * .5));
            
            for (int i = 1; i < coordLists.size(); i++) {
                stamps.add(new GeomBase(UUID.randomUUID(), coordLists.get(i)[0], coordLists.get(i)[1], GeomType.STAMP));
            }
        }

        return stamps;
    }

    public static List<GeomBase> stampIntersecting(GeomBase stamp, List<GeomBase> wallObjects) {
        // step 1: finding the closest wallObject:

        double min_distance = 10000.;
        GeomBase closest = wallObjects.get(0);
        for ( GeomBase geom : wallObjects ) {
            double local_distance = stamp.getInternalGeo().distance(geom.getExternalGeo() ) ;
            if ( local_distance < min_distance ) {
                min_distance = local_distance;
                closest = geom;
            }
        }

        return GeomBooleanOperations.stampIntersecting(stamp, closest);
    }
}
