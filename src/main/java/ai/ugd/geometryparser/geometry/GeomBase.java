package ai.ugd.geometryparser.geometry;

import ai.ugd.geometryparser.data.DataContainer;
import ai.ugd.geometryparser.data.Layer;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Polygon;

import java.util.*;

public class GeomBase implements IGeomBase {

    // A geometry base exist out of two shapes: a representation shape (the externalGeo) and a the geometric one (internalGeo)
    // for the containment and validity checks the internalGeo will be used, though final output and optional boolean operations
    // will be done using the external geometry. There are two types of validity constrains: individual, whether the geometry
    // of the object itself can be considered valid (double points, endpoints meet, self-intersection), and the external validity
    // which says whether the relation between this object and other objects in the layer structure makes sense or not.
    // some of these objects are only relevant for the front end or back end
    
    public enum GeomScope {
        NORMAL(true, true),
        ONLYBACKEND(false, true),
        ONLYFRONTEND(true, false),
        NEITHER(false, false);
        
        private boolean isFrontEnd;
        private boolean isBackEnd;

        GeomScope(boolean isFrontEnd, boolean isBackEnd) {
            this.isFrontEnd = isFrontEnd;
            this.isBackEnd = isBackEnd;
        }

        public boolean isFrontEnd() {return this.isFrontEnd; }
        public boolean isBackEnd() {return this.isBackEnd; }
    }
    
    private UUID uuid;
    private String name;

    private GeomType geomType;
    private Geometry externalGeo;
    private Geometry internalGeo;
    private Double radius = null;

    private List<UUID> relatedObjects = new ArrayList<>();              // in case relatedObjects is set, will be used for relational calculations
    private Layer parentLayer;                      // allows to search for itself
    
    // only for NGS type
    private DataContainer childrenDataContainer;

    private GeomScope scope = GeomScope.NORMAL;
    private Set<GeomInvalidityType> invalidityTypes = new HashSet<>();

    public GeomBase(UUID uuid, Coordinate coord, GeomType geomType) throws IllegalArgumentException {
        this.initFunctions(uuid, geomType);
        this.initFromCoord(coord);
        this.setName();
        this.postProcessing();
    }

    public GeomBase(UUID uuid, Coordinate coord, Double radius, GeomType geomType) throws IllegalArgumentException {
        this.initFunctions(uuid, geomType);
        this.initFromCoordRadius(coord, radius);
        this.setName();
        this.postProcessing();
    }

    public GeomBase(UUID uuid, Coordinate[] coords, GeomType geomType) throws IllegalArgumentException {
        this.initFunctions(uuid, geomType);
        this.initFromCoords(coords);
        this.setName();
        this.postProcessing();
    }

    public GeomBase(UUID uuid, Coordinate[] coords, Double radius, GeomType geomType) throws IllegalArgumentException {
        this.initFunctions(uuid, geomType);
        this.initFromCoordsBuffer(coords, radius);
        this.setName();
        this.postProcessing();
    }

    public GeomBase(UUID uuid, Coordinate[] coords, String name, GeomType geomType) throws IllegalArgumentException {
        this.initFunctions(uuid, geomType);
        this.initFromCoords(coords);
        this.setName(name);
        this.postProcessing();
    }

    public GeomBase(UUID uuid, Coordinate coorA, Coordinate coorB, GeomType geomType) throws IllegalArgumentException {
        this.initFunctions(uuid, geomType);
        this.initFromTwoCoords(coorA, coorB);
        this.setName();
        this.postProcessing();
    }

    public GeomBase(UUID uuid, Coordinate coorA, Coordinate coorB, Coordinate coorC, GeomType geomType) throws IllegalArgumentException {
        this.initFunctions(uuid, geomType);
        this.initFromThreeCoords(coorA, coorB, coorC);
        this.setName();
        this.postProcessing();
    }

    public GeomBase(UUID uuid, Coordinate[] coords, List<List<Integer>> connectionGraph, Double buffer, GeomType geomType) throws IllegalArgumentException {
        this.initFunctions(uuid, geomType);
        this.initFromCoordsAndGraph(coords, connectionGraph, buffer);
        this.setName();
        this.postProcessing();
    }

    // offspring block generation private method
    private GeomBase(Polygon pg, String name) {
        this.initFunctions(UUID.randomUUID(), GeomType.BLOCK);
        this.externalGeo = pg;
        this.internalGeo = pg.getCentroid();
        this.setName(name);
        this.postProcessing();
    }

    public GeomBase(GeomBase otherGeomBase) {
        System.out.println("[INFO] - GeomBase(GeomBase) - createdDuplicate of GB with uuid: " + otherGeomBase.getUUID().toString());
        this.uuid = UUID.randomUUID();                              // the only thing that is not copied over
        this.name = otherGeomBase.name;

        this.geomType = otherGeomBase.geomType;
        this.internalGeo = otherGeomBase.internalGeo.copy();        // have to be re-instantiated
        this.externalGeo = otherGeomBase.externalGeo.copy();        // have to be re-instantiated
        this.radius = otherGeomBase.radius;

        this.scope = otherGeomBase.scope;
        this.invalidityTypes = otherGeomBase.invalidityTypes;
    }

    public GeomBase duplicate(){
        return new GeomBase(this);
    }

    public GeomBase updateWith(GeomBase otherGeomBase) {
        //this.uuid = this.uuid;                                      // the only thing that remains the same
        this.name = otherGeomBase.name;

        this.geomType = otherGeomBase.geomType;
        this.internalGeo = otherGeomBase.internalGeo;
        this.externalGeo = otherGeomBase.externalGeo;
        this.radius = otherGeomBase.radius;

        this.invalidityTypes = otherGeomBase.invalidityTypes;

        return this;
    }

    // geometric initialization functions
    private void initFromCoord(Coordinate coord) throws IllegalArgumentException {
//        if (this.initFromCoord.contains(this.geoType) ){
        if (this.geomType == GeomType.POINT) {
            this.internalGeo = new GeometryFactory().createPoint(coord);
            this.externalGeo = this.internalGeo;
        } else if (this.geomType == GeomType.STAMP) {
            this.initFromCoordRadius(coord, GeomBaseFactories.circleRadius);
        } else {
            throw new IllegalArgumentException(this.geomType.name() + " can not be initialised using a single Coordinate");
        }
    }

    private void initFromCoordRadius(Coordinate coord, Double radius) throws IllegalArgumentException {
//        if (this.initFromCoordRadius.contains(this.geoType) ) {
        if (this.geomType == GeomType.STAMP) {
            if (radius == null) {
                this.radius = GeomBaseFactories.circleRadius;
            } else {
                this.radius = radius;
            }
            GeometryFactory gf = new GeometryFactory();
            this.externalGeo = gf.createPolygon(gf.createLinearRing(GeomBaseFactories.makeCircleFromCoor(coord, this.radius) ) );
            this.internalGeo = gf.createPoint(coord);
        } else {
            throw new IllegalArgumentException(this.geomType.name() + " can not be initialised using a single Coordinate and a Radius");
        }
    }

    private void initFromCoords(Coordinate[] coords) throws IllegalArgumentException {
//        if (this.initFromCoords.contains(this.geoType) ) {
        GeometryFactory gf = new GeometryFactory();
        if (this.geomType == GeomType.POLYLINE) {
            this.internalGeo = gf.createLineString(coords);
            this.externalGeo = this.internalGeo;
        } else if (this.geomType == GeomType.POLYGON || this.geomType == GeomType.RECTANGLE) {
            try {
                this.internalGeo = gf.createPolygon(gf.createLinearRing(coords) ) ;
                this.externalGeo = this.internalGeo;
            } catch (IllegalArgumentException e) {
                throw new IllegalArgumentException("The given point list is not closed (first point doesn't match last), therefor not able to Generate a POLYGON");
            }
        } else if (this.geomType == GeomType.BLOCK) {
            try {
                this.internalGeo = gf.createPolygon(gf.createLinearRing(coords) );
                this.externalGeo = this.internalGeo;
            } catch (IllegalArgumentException e) {
                throw new IllegalArgumentException("The given point list is not closed (first point doesn't match last), therefor not able to Generate a RECTANGLE");
            }
        } else if (this.geomType == GeomType.SNAKE) {
            this.initFromCoordsBuffer(coords, GeomBaseFactories.snakeBuffer);
        } else {
            throw new IllegalArgumentException(this.geomType.name() + " can not be initialised using a bunch of Coordinates");
        }
    }

    private void initFromCoordsBuffer(Coordinate[] coords, Double buffer) throws IllegalArgumentException {
//        if (this.initFromCoordsBuffer.contains(this.geoType) ) {
        if (this.geomType == GeomType.SNAKE) {
            if (buffer == null) {
                this.radius = GeomBaseFactories.snakeBuffer;
            } else {
                this.radius = buffer;
            }
            this.internalGeo = new GeometryFactory().createLineString(coords);
            this.externalGeo = this.internalGeo.buffer(this.radius, -2, 2);
        } else {
            throw new IllegalArgumentException(this.geomType.name() + " can not be initialised using a bunch of Coordinates and a Buffer");
        }
    }

    private void initFromTwoCoords(Coordinate coordA, Coordinate coordB) throws IllegalArgumentException {
        // temporarily changed so that already 2pt stamp elements while retain their shape, without having to rely on
        // intersection/clipping of geometries (not yet implemented)
        GeometryFactory gf = new GeometryFactory();
        if (this.geomType == GeomType.STAMP) {
            this.initFromCoordRadius(
                    new Coordinate((coordA.x + coordB.x) * .5, (coordA.y + coordB.y) * .5 ),
                    coordA.distance(coordB) * .5);
        } else if (this.geomType == GeomType.RECTANGLE || this.geomType == GeomType.POLYGON) {
            this.geomType = GeomType.RECTANGLE;
            Coordinate[] coords = new Coordinate[5];
            coords[0] = coordA;
            coords[1] = new Coordinate(coordA.getX(), coordB.getY() );
            coords[2] = coordB;
            coords[3] = new Coordinate(coordB.getX(), coordA.getY() );
            coords[4] = coordA;
            this.initFromCoords(coords);
        } else {
            throw new IllegalArgumentException(this.geomType.name() + " can not be initialised using just two Coordinates");
        }
    }

    private void initFromThreeCoords(Coordinate coordA, Coordinate coordB, Coordinate coordC) throws IllegalArgumentException {
        if (this.geomType == GeomType.RECTANGLE || this.geomType == GeomType.POLYGON) {
            this.geomType = GeomType.RECTANGLE;
            Coordinate[] coords = new Coordinate[5];
            coords[0] = coordA;
            coords[1] = coordB;
            coords[2] = coordC;
            coords[3] = new Coordinate(coordB.x - coordA.x + coordC.x, coordB.y - coordA.y + coordC.y);
            coords[4] = coordA;
            this.initFromCoords(coords);
        } else {
            throw new IllegalArgumentException(this.geomType.name() + " can not be initialised using just three Coordinates");
        }
    }

    private void initFromCoordsAndGraph(Coordinate[] coords, List<List<Integer>> connectionGraph, Double buffer) throws IllegalArgumentException {
        if (this.geomType == GeomType.NETWORK) {
            this.internalGeo = GeomBaseFactories.makeNetwork(coords, connectionGraph);
            if (buffer == null) {
                this.radius = GeomBaseFactories.snakeBuffer;
            } else {
                this.radius = buffer;
            }
            this.externalGeo = internalGeo.buffer(this.radius).union();
        } else {
            throw new IllegalArgumentException(this.geomType.name() + " can not be initialised using just coors + graph");
        }
    }

    private void checkIfRectangle() {
        if ( (this.geomType == GeomType.POLYGON || this.geomType == GeomType.RECTANGLE) ) {
            if (this.internalGeo.isRectangle()) {
                if (this.geomType == GeomType.POLYGON) {
                    this.geomType = GeomType.RECTANGLE;
                    System.out.println("[INFO] - this Polygon has been turned into a Rectangle!");
                } else {
                    System.out.println("[INFO] - this Rectangle has been kept as a Rectangle!");
                }
            } else {
                if (this.geomType == GeomType.POLYGON) {
                    this.geomType = GeomType.POLYGON;
                    System.out.println("[INFO] - this Polygon has been kept as a Polygon!");
                } else {
                    System.out.println("[INFO] - this was not a valid Rectangle and has therefore been turned into a Polygon!");
                }
            }
        } else {
            System.out.println("[INFO] - this is not a Polygon!");
        }
    }

    // static initialization of block entries
    public static GeomBase createBlock(Polygon pg, String blockName) {
        return new GeomBase(pg, blockName);
    }

    private void initFunctions(UUID uuid, GeomType geoType) {
        this.geomType = geoType;
        this.uuid = uuid;
    }

    private void postProcessing() {
        this.checkIfRectangle();
//        this.checkReflectiveValidity();
    }

    // geometric functions
    public double distanceTo(GeomBase other) {
        double distance = this.internalGeo.distance(other.internalGeo);
        if (this.geomType.isImplicit() ) distance -= this.radius;
        if (other.geomType.isImplicit()) distance -= other.radius;

        return distance;
    }

    public boolean contains(GeomBase other) {
        return this.externalGeo.contains(other.internalGeo);
    }

    public boolean intersects(GeomBase other) {
        return this.externalGeo.intersects(this.externalGeo);
    }

    // some extra basic function
    public boolean isSimple() {
        return this.geomType.isSimple();
    }

    public boolean isImplicit() {
        return this.geomType.isImplicit();
    }

    public boolean isComplex() {
        return this.geomType.isComplex();
    }

    public boolean isClosed() {
        return this.geomType.isClosed();
    }

    public boolean isFeo() { return this.scope.isFrontEnd; }

    public boolean isBeo() { return this.scope.isBackEnd; }

    public void updateGeomBase(GeomBase geomBase) {
        this.parentLayer.updateGeomBase(this.getUUID(), geomBase);
    }
    
    public void updateGeomBase(List<GeomBase> geomBases) {
        this.parentLayer.updateGeomBase(this.getUUID(), geomBases);
    }
    
    public List<GeomBase> getRelatedObjects() {
        return this.parentLayer.getDataContainer().getGeomBase(this.relatedObjects);
    }

    // validity
    public void checkValidity() {
        GeomValidityCheck.checkValidity(this);
    }
    
    public void addInvalidity(GeomInvalidityType invalidity) {
        this.invalidityTypes.add(invalidity);
    }
    
    public void addInvalidities(List<GeomInvalidityType> invalidities) {
        this.invalidityTypes.addAll(invalidities);
    }
    
    public boolean isValid() {
        if (this.invalidityTypes.size() == 0) {
            return false;
        } else {
            return true;
        }
    }

    private String validityString() {
        if (this.isValid() ) {
            return "valid";
        } else {
            return "invalid";
        }
    }

    public Set<GeomInvalidityType> getInvalidityTypes() {
        if (!isValid() ) {
            return this.invalidityTypes;
        } else {
            return null;
        }
    }

    public String getInvalidityTypesString() {
        if (!isValid() ) {
            List<String> invalidityStrings = new ArrayList<>();
            for (GeomInvalidityType invalidityType : this.invalidityTypes) {
                invalidityStrings.add(invalidityType.name());
            }
            return String.join(", ", invalidityStrings);
        } else {
            return null;
        }
    }

    public void addInvalidityType(GeomInvalidityType newInvalidityType) {
        this.invalidityTypes.add(newInvalidityType);
    }

    public String toString() {
        return "GeomBase of type " + this.geomType.name() + ", has " + this.internalGeo.getNumPoints() + " points and is " + this.validityString();
    }

    public boolean validate(IGeomBase geomBase) {
        return false;
    }

    private void setName() {
        this.setName(this.geomType.defaultName());
    }

    private void setName(String objectName){
        this.name = objectName;
    }

    public String getName(){
        return this.name;
    }

    public UUID getUUID(){
        return this.uuid;
    }

    public GeomType getGeomType(){
        return this.geomType;
    }

    public Geometry getExternalGeo(){
        return this.externalGeo;
    }

    public Geometry getInternalGeo(){
        return this.internalGeo;
    }

    public Double getRadius(){
        return this.radius;
    }

    protected void setInternalGeo(Geometry newInternalGeo) {
        this.internalGeo = newInternalGeo;
    }

    protected void setExternalGeo(Geometry newExternalGeo) {
        this.externalGeo = newExternalGeo;
    }

    protected void setRadius(Double radius) { this.radius = radius; }

    // data management
    public Layer getParentLayer() {
        return this.parentLayer;
    }

    public DataContainer getDataContainer() { return this.parentLayer.getDataContainer(); }

    public void setLayer(Layer layer) {
        this.parentLayer = layer;
    }

    public void setScope (GeomScope newScope) {
        this.scope = newScope;
    }

    public GeomScope getScope () {return this.scope; };

    public void addRelatedObjects(GeomBase geomBase) {
        this.relatedObjects.add(geomBase.getUUID() );
    }

    public void addRelatedObjects(List<GeomBase> geomBases) {
        for (GeomBase geomBase : geomBases) {
            this.addRelatedObjects(geomBase);
        }
    }

    public void updateStampPosition(Coordinate newCoordinate) {
        if (this.geomType == GeomType.STAMP) {
            this.initFromCoordRadius(newCoordinate, this.getRadius());
        }
    }

    public void updateRadius(double newRadius) {
        if (this.geomType.isImplicit() ) {
            this.radius = newRadius;
            this.externalGeo = internalGeo.buffer(newRadius).union();
        }
    }
}
