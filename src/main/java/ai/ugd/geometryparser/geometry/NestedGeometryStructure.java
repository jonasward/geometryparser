package ai.ugd.geometryparser.geometry;
//
//import data.Layer;
//import org.locationtech.jts.geom.LinearRing;
//
//import java.util.List;
//
//// A Nested Geometry Structure is a data structure constructed from a bunch of LinearRings (closed polylines / polygons)
//// The normal type consists of a boundary LinearRing and a List of NGSs which there is also a Layer object containing all
//// the other geometrical objects which are strictly located in this NGS (meaning inside the boundary yet not inside one
//// of the NGS's interiors). A NGS can also not have a boundary. In this case it's the collection of all the LinearRings
//// and objects that couldn't find there way into another LinearRing.
//
//class NestedGeometryStructure {
//    private LinearRing boundary;
//    private Layer objects;
//    private List<NestedGeometryStructure> interiors;
//
//    private boolean isGrouper = false;
//
//    public NestedGeometryStructure(List<LinearRing> inputLRs) {
//
//    }
//
//    public NestedGeometryStructure(LinearRing boundary, Layer objects) {
//
//    }
//
//    private NestedGeometryStructure(LinearRing boundary, Layer otherObjects, List<NestedGeometryStructure> interiors){
//
//    }
//
//    public static NestedGeometryStructure createFromGeometries(List<GeomBase> geos){
//
//    }
//}
