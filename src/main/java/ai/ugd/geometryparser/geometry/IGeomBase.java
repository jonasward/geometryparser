package ai.ugd.geometryparser.geometry;

public interface IGeomBase {
    
    boolean validate(IGeomBase geomBase);
}
