package ai.ugd.geometryparser.geometry;

public enum GeomType {

    NONE(GeomTypeClass.SIMPLEOPEN,"defaultPointCollectionName"),
    POINTCOLLECTION(GeomTypeClass.SIMPLEOPEN, "defaultPointCollection"),    // point collection
    POINT(GeomTypeClass.SIMPLEOPEN, "defaultPoint"),                        // standard point
    POLYLINE(GeomTypeClass.SIMPLEOPEN, "standardPolyline"),                 // open polyline
    POLYGON(GeomTypeClass.SIMPLECLOSED, "standardPolygon"),                 // closed polyline
    RECTANGLE(GeomTypeClass.SIMPLECLOSED, "standardRectangle"),             // same as polygon, yet rectangle
    STAMP(GeomTypeClass.IMPLICIT, "defaultStampName"),                      // centroid based: location with a radius or a polygon
    SNAKE(GeomTypeClass.IMPLICIT, "defaultSnake"),                          // (open) polyline which is buffered
    NETWORK(GeomTypeClass.IMPLICIT, "defaultNetwork"),                      // multilinestring that represents a connected graph, otherwise the same as the snake type
    BLOCK(GeomTypeClass.COMPLEX, "defaultBlockName"),                       // centroid based: bbox rectangle with a name
    NGS(GeomTypeClass.COMPLEX,"defaultNGS");                                // nested geometry structure: data container with polygon, can also contain multiple other NGSs
    
    private GeomTypeClass geoClass;
    
    private String defaultName;

    GeomType(GeomTypeClass geoClass, String defaultName) {
        this.geoClass = geoClass;
        this.defaultName = defaultName;
    }

    public boolean isClosed() {
        return this.geoClass.isClosed();
    }

    public boolean isSimple() {
        return this.geoClass.isSimple();
    }

    public boolean isImplicit() {
        return this.geoClass.isImplicit();
    }

    public boolean isComplex() {
        return this.geoClass.isComplex();
    }
    
    public boolean isValidReflectiveValidityCheckType(GeomInvalidityType geomInvalidityType) {
        return geomInvalidityType.applies(this);
    }
    
    public boolean isValidRelationalValidityCheckType(GeomInvalidityType geomInvalidityType) {
        return geomInvalidityType.relationApplies(this);
    }

    protected String defaultName() {
        return this.defaultName;
    }
}

