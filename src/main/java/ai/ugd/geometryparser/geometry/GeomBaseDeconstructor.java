package ai.ugd.geometryparser.geometry;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Point;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

public class GeomBaseDeconstructor {
    public static List<Point2D> externalToPoints(GeomBase geomBase) {
        List<Point2D> pts = new ArrayList<>();

        Coordinate[] coords = geomBase.getExternalGeo().getCoordinates();
        for (Coordinate coor : coords) {
            pts.add( new Point2D.Double(coor.x, coor.y) );
        }

        return pts;
    }

    public static List<List<Double>> externalToDoubleLists(GeomBase geomBase) {
        List<List<Double>> pts = new ArrayList<>();

        Coordinate[] coords = geomBase.getExternalGeo().getCoordinates();
        for (Coordinate coor : coords) {
            pts.add(new ArrayList<Double>() { {add(coor.x); add( coor.y); } });
        }

        return pts;
    }

    public static double[][] externalToDoubleArrays(GeomBase geomBase) {


        Coordinate[] coords = geomBase.getExternalGeo().getCoordinates();
        double[][] doubles = new double[coords.length][2];
        int idx = 0;
        for (Coordinate coor : coords) {
            doubles[idx] = new double[]{coor.x, coor.y };
            idx ++;
        }

        return doubles;
    }

    public static Point2D internalGeometryToPoint(GeomBase geomBase) {
        Point pt = geomBase.getInternalGeo().getCentroid();
        return new Point2D.Double(pt.getX(), pt.getY());
    }
}
