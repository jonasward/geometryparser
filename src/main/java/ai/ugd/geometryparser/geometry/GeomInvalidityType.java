package ai.ugd.geometryparser.geometry;

import java.util.Arrays;
import java.util.List;

public enum GeomInvalidityType {
    NONE(null, null, null),
    SELFINTERSECTS (false, Arrays.asList(GeomType.POLYGON, GeomType.POLYLINE, GeomType.NGS), null ),
    ENDSTARTOVERLAP (false, Arrays.asList(GeomType.POLYLINE), null ),
    DOUBLEPOINTS (false, Arrays.asList(GeomType.POINTCOLLECTION, GeomType.POLYLINE, GeomType.POLYGON, GeomType.SNAKE, GeomType.NGS), null ),
    INTERSECTS (true, Arrays.asList(GeomType.POLYGON, GeomType.POLYLINE, GeomType.RECTANGLE, GeomType.NGS), Arrays.asList(GeomType.POLYLINE, GeomType.POLYGON, GeomType.RECTANGLE, GeomType.NGS)),
    NOTCONTAINED (true, Arrays.asList(GeomType.values()), Arrays.asList(GeomType.POLYGON, GeomType.RECTANGLE, GeomType.NGS) ),
    NOPARENT(true, Arrays.asList(GeomType.POINT, GeomType.POINTCOLLECTION, GeomType.STAMP, GeomType.BLOCK), Arrays.asList(GeomType.POLYLINE, GeomType.POLYGON, GeomType.RECTANGLE, GeomType.NGS) ),
    UNCOVEREDENDS (true, Arrays.asList(GeomType.POLYLINE, GeomType.SNAKE), Arrays.asList(GeomType.POLYGON, GeomType.RECTANGLE, GeomType.SNAKE, GeomType.NETWORK) ),
    TOOCLOSENEIGHBOUR (true, Arrays.asList(GeomType.STAMP, GeomType.BLOCK), Arrays.asList(GeomType.STAMP, GeomType.BLOCK) );
    
    private Boolean isRelated;
    private List<GeomType> acceptedGeoTypes;            // GeomBase types that are considered relevant to be checked
    private List<GeomType> acceptedRelatedGeoTypes;     // for the externalValidity: types to which these geoms can be related to
    
    public static double notContainedBuffer = .05;
    public static double intersectionShortening = .05;
    public static double distanceTolerance = .1;
    
    GeomInvalidityType(Boolean isRelated, List<GeomType> acceptedGeoTypes, List<GeomType> acceptedReferenceGeoTypes) {
        this.isRelated = isRelated;
        this.acceptedGeoTypes = acceptedGeoTypes;
        this.acceptedRelatedGeoTypes = acceptedReferenceGeoTypes;
    }
    
    public boolean isReflective() {
        if (this.isRelated == null) {
            return false;
        } else {
            return !this.isRelated;
        }
    }

    public boolean isRelated() {
        if (this.isRelated == null) {
            return true;
        } else {
            return this.isRelated;
        }
    }
    
    public boolean applies(GeomBase geomBase) {
        return this.applies( geomBase.getGeomType() );
    }
    
    public boolean applies(GeomType geomType) {
        if (this.acceptedGeoTypes == null) {
            return true;
        } else if(this.acceptedGeoTypes.contains( geomType ) ) {
            return true;
        } else {
            return false;
        }
    }

    public boolean relationApplies(GeomBase geomBase) {
        return this.relationApplies( geomBase.getGeomType() );
    }

    public boolean relationApplies(GeomType geomType) {
        if (this.acceptedRelatedGeoTypes == null) {
            return true;
        } else if(this.acceptedRelatedGeoTypes.contains( geomType ) ) {
            return true;
        } else {
            return false;
        }
    }
}
