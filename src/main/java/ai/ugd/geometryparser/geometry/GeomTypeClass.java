package ai.ugd.geometryparser.geometry;

public enum GeomTypeClass {
    SIMPLEOPEN(false, true, false, false),
    SIMPLECLOSED(true, true, false, false),
    IMPLICIT(true, false, true, true),
    COMPLEX(true, false, false, true);

    private boolean isClosed;
    private boolean isSimple;
    private boolean isImplicit;
    private boolean isComplex;

    GeomTypeClass(boolean isClosed, boolean isSimple, boolean isImplicit, boolean isComplex){
        this.isClosed = isClosed;
        this.isSimple = isSimple;
        this.isImplicit = isImplicit;
        this.isComplex = isComplex;
    }

    public boolean isClosed(){
        return this.isClosed;
    }

    public boolean isSimple(){
        return this.isSimple;
    }

    public boolean isImplicit(){
        return this.isImplicit;
    }

    public boolean isComplex(){
        return this.isComplex;
    }
}
