package ai.ugd.geometryparser.geometry;

import org.locationtech.jts.geom.*;
import org.locationtech.jts.operation.buffer.BufferParameters;

import java.awt.geom.Point2D;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class GeomBaseFactories {
    public static double distanceTolerance = .001;
    public static int circleResolution = 32;
    public static double TAU = Math.PI * 2.0;
    
    public static double circleRadius = .2;                 // default radius that will be used to generate linear rings for the forced stamps
    public static double snakeBuffer = .5;                  // default distance that will be used to generate the forced snakes with
    public static double stampLineStringShortening = .1;

    // constructors
    public static GeomBase initNewPoint(Point2D pt) {
        return new GeomBase(UUID.randomUUID(), new Coordinate(pt.getX(), pt.getY() ), GeomType.POINT);
    }

    public static GeomBase initPredefinedPoint(UUID exiUUID, Point2D pt) {
        return new GeomBase(exiUUID, new Coordinate(pt.getX(), pt.getY() ), GeomType.POINT);
    }

    public static GeomBase initNewStamp(Point2D pt, Double radius) {
        return new GeomBase(UUID.randomUUID(), new Coordinate(pt.getX(), pt.getY() ), radius, GeomType.STAMP);
    }

    public static GeomBase initNewStamp(Point2D ptA, Point2D ptB) {
        Coordinate cA = new Coordinate(ptA.getX(), ptA.getY() );
        Coordinate cB = new Coordinate(ptB.getX(), ptB.getY() );
        return new GeomBase(UUID.randomUUID(), cA, cB, GeomType.STAMP);
    }

    public static GeomBase initPredefinedStamp(UUID exiUUID, Point2D pt, Double radius) {
        return new GeomBase(exiUUID, new Coordinate(pt.getX(), pt.getY() ), radius, GeomType.STAMP);
    }

    public static GeomBase initNewPolygon(List<Point2D> pts) {
        Coordinate [] coords = GeomBaseFactories.makeCoordinates(pts, true);
        return new GeomBase(UUID.randomUUID(), coords, GeomType.POLYGON);
    }

    public static GeomBase initPredefinedPolygon(UUID exiUUID, List<Point2D> pts) {
        Coordinate [] coords = GeomBaseFactories.makeCoordinates(pts, true);
        return new GeomBase(exiUUID, coords, GeomType.POLYGON);
    }

    public static GeomBase initNewPolyline(List<Point2D> pts) {
        Coordinate [] coords = GeomBaseFactories.makeCoordinates(pts, false);
        return new GeomBase(UUID.randomUUID(), coords, GeomType.POLYLINE);
    }

    public static GeomBase initPredefinedPolyline(UUID exiUUID, List<Point2D> pts) {
        Coordinate [] coords = GeomBaseFactories.makeCoordinates(pts, false);
        return new GeomBase(exiUUID, coords, GeomType.POLYLINE);
    }

    public static GeomBase initNewSnake(List<Point2D> pts, Double snakeBuffer) {
        Coordinate [] coords = GeomBaseFactories.makeCoordinates(pts, false);
        return new GeomBase(UUID.randomUUID(), coords, snakeBuffer, GeomType.SNAKE);
    }

    public static GeomBase initPredefinedSnake(UUID exiUUID, List<Point2D> pts, Double snakeBuffer) {
        Coordinate [] coords = GeomBaseFactories.makeCoordinates(pts, false);
        return new GeomBase(exiUUID, coords, GeomType.SNAKE);
    }

    public static GeomBase initPredefinedNetwork(UUID exiUUID, List<Point2D> pts, List<List<Integer>> connectionGraph, Double snakeBuffer){
        Coordinate [] coords = GeomBaseFactories.makeCoordinates(pts);
        return new GeomBase(exiUUID, coords, connectionGraph, snakeBuffer, GeomType.NETWORK);
    }

    // translators for other types
    public static Coordinate makeCoordinate(Point2D pt){
        return new Coordinate(pt.getX(), pt.getY());
    }

    public static Coordinate[] makeCoordinates(List<Point2D> pts) {
        return GeomBaseFactories.makeCoordinates(pts, false);
    }

    public static Coordinate[] makeCoordinates(List<Point2D> pts, boolean isClosed){
        if (isClosed) {
            if (!(pts.get(0).distance(pts.get(pts.size() - 1)) < GeomBaseFactories.distanceTolerance)) {
                pts.add(pts.get(0));
            }
        }

        Coordinate[] coordArray = new Coordinate[pts.size()];
        int idx = 0;
        for (Point2D pt: pts){
            coordArray[idx] = GeomBaseFactories.makeCoordinate(pt);
            idx++;
        }
        return coordArray;
    }

    public static Point2D makePoint2D(List<Double> dbls) throws InvalidParameterException {
        if (dbls == null || dbls.size() != 2 || dbls.get(0) == null || dbls.get(1) == null ) {
            throw new InvalidParameterException("this is an invalid double pair");
        }

        return new Point2D.Double(dbls.get(0), dbls.get(1));
    }

    public static List<Point2D> makePoint2Ds(List<List<Double>> dblls) throws InvalidParameterException {
        if (dblls == null || dblls.isEmpty() ) {
            throw new InvalidParameterException("this is an invalid list of double pairs");
        }

        List<Point2D> outputPts = new ArrayList<>();
        for (List<Double> dbls: dblls) {
            outputPts.add(GeomBaseFactories.makePoint2D(dbls));
        }

        return outputPts;
    }

    public static Geometry makePoint(Point2D pt){
        return new GeometryFactory().createPoint(GeomBaseFactories.makeCoordinate(pt));
    }

    public static Geometry[] makePoints(List<Point2D> pts){
        Geometry[] pointArray = new Geometry[pts.size()];
        int idx = 0;
        for (Point2D pt: pts){
            pointArray[idx] = GeomBaseFactories.makePoint(pt);
            idx++;
        }
        return pointArray;
    }

    public static Geometry MakeRectanglePGFrom2DArray(double[][] coords){
        Coordinate[] lrBase = new Coordinate[]{
            new Coordinate(coords[0][0], coords[1][0]),
            new Coordinate(coords[0][1], coords[1][0]),
            new Coordinate(coords[0][1], coords[1][1]),
            new Coordinate(coords[0][0], coords[1][1]),
            new Coordinate(coords[0][0], coords[1][0])
        };
        
        GeometryFactory gf = new GeometryFactory();
        return gf.createPolygon(gf.createLinearRing(lrBase) );
    }

    public static Geometry makeLinearRing(List<Point2D> pts){
        return new GeometryFactory().createLinearRing(GeomBaseFactories.makeCoordinates(pts));
    }

    public static Geometry makeLineString(List<Point2D> pts){
        return new GeometryFactory().createLineString(GeomBaseFactories.makeCoordinates(pts));
    }

    public static GeometryCollection makePointCollection(List<Point2D> pts){
        return new GeometryFactory().createGeometryCollection(GeomBaseFactories.makePoints(pts));
    }

    public static Coordinate[] makeCircleFromPoint(Point2D pt) {
        return GeomBaseFactories.makeCircleFromPoint(pt, circleRadius);
    }

    public static Coordinate[] makeCircleFromPoint(Point2D pt, double radius) {
        return GeomBaseFactories.makeCircleFromCoor(new Coordinate(pt.getX(), pt.getY()), radius);
    }

    public static Coordinate[] makeCircleFromCoor(Coordinate coor) {
        return GeomBaseFactories.makeCircleFromCoor(coor, circleRadius);
    }

    public static Coordinate[] makeCircleFromCoor(Coordinate coor, double radius) {
        Coordinate[] coords = new Coordinate[GeomBaseFactories.circleResolution + 1];
        double alfaDelta = Math.PI / (GeomBaseFactories.circleResolution * .5);
        for (int i = 0; i < GeomBaseFactories.circleResolution; i ++) {
            coords[i] = new Coordinate(coor.x + Math.cos(alfaDelta * i) * radius, coor.y + Math.sin(alfaDelta * i) * radius);
        }
        coords[GeomBaseFactories.circleResolution] = coords[0];
        return coords;
    }

    public static Geometry makeSnake(Geometry lineString, double bufferThickness){
        return lineString.buffer(bufferThickness, 10, BufferParameters.CAP_SQUARE);
    }

    public static Geometry makeSnake(Geometry lineString){
        return lineString.buffer(GeomBaseFactories.snakeBuffer, 10, BufferParameters.CAP_SQUARE);
    }

    // translating polygons & polylines into stamps
    public static List<GeomBase> toStamps(Geometry geom, Double shortening) throws Exception {
        if ( (geom.getGeometryType() == Geometry.TYPENAME_LINEARRING) || (geom.getGeometryType() == Geometry.TYPENAME_LINESTRING) ) {
            List<GeomBase> stampList = new ArrayList<>();
            if (shortening == null) {
                shortening = GeomBaseFactories.stampLineStringShortening; // if no value given, use the library default one
            }

            Coordinate[] coords = geom.getCoordinates();
            if (coords.length == 2) {
                // initializing stamp using first and second point
                stampList.add(new GeomBase(UUID.randomUUID(), coords[0], coords[1], GeomType.STAMP));
            } else if (coords.length > 2) {
                // deconstructing the LineString/LinearRings into line segments. those segments touching another one
                // are shortened a given "shortening" distance, by shifting those towards the center location

                // first segment - only end point adjustment

            }
            return stampList;
        } else {
            throw new Exception("toStamps is not defined for " + geom.getGeometryType() + " it is for Polylines, Polygons & Rectangles");
        }
    }

    public static Geometry makeNetwork(Coordinate[] coords, List<List<Integer>> connectionGraph) {
        GeometryFactory gf = new GeometryFactory();

        LineString[] lmg = new LineString[connectionGraph.size()];

        int i = 0;
        for (List<Integer> e: connectionGraph) {
            Coordinate[] coo = new Coordinate[] { coords[e.get(0)], coords[e.get(1)]};
            lmg[i] = gf.createLineString(coo);

            i ++;
        }

        return gf.createMultiLineString(lmg);
    }
}
