package ai.ugd.geometryparser.geometry;

public class GeomBaseSecondaryTranslators {

    public static GeomBase toPoint(GeomBase geomBase) throws IllegalArgumentException {
        GeomType outputGeomType = GeomType.POINT;
        if ( geomBase.getGeomType() == outputGeomType) {
            System.out.println("[INFO] - GeomBaseSecondaryTranslators - already is a " + outputGeomType.name());
            return geomBase;

        } else if ( geomBase.getGeomType() == GeomType.STAMP ) {
            return GeomBasePrimaryTranslators.toPoint(geomBase);

        } else if ( geomBase.getGeomType() == GeomType.POLYGON || geomBase.getGeomType() == GeomType.RECTANGLE || geomBase.getGeomType() == GeomType.BLOCK || geomBase.getGeomType() == GeomType.SNAKE ) {
            GeomBase stamp = GeomBasePrimaryTranslators.toStamp(geomBase);
            return GeomBasePrimaryTranslators.toPoint(stamp);

        } else {
            throw new IllegalArgumentException(GeomBasePrimaryTranslators.exceptionString(geomBase.getGeomType(), outputGeomType));
        }
    }

    public static GeomBase toPolyline(GeomBase geomBase) throws IllegalArgumentException {
        GeomType outputGeomType = GeomType.POLYLINE;
        if ( geomBase.getGeomType() == outputGeomType) {
            System.out.println("[INFO] - GeomBaseSecondaryTranslators - already is a " + outputGeomType.name());
            return geomBase;

        } else if ( geomBase.getGeomType() == GeomType.SNAKE ) {
            return GeomBasePrimaryTranslators.toPoint(geomBase);

        } else if ( geomBase.getGeomType() == GeomType.RECTANGLE ) {
            GeomBase snake = GeomBasePrimaryTranslators.toSnake(geomBase);
            return GeomBasePrimaryTranslators.toPoint(snake);

        } else {
            throw new IllegalArgumentException(GeomBasePrimaryTranslators.exceptionString(geomBase.getGeomType(), outputGeomType));
        }
    }

    public static GeomBase toPolygon(GeomBase geomBase) throws IllegalArgumentException {
        GeomType outputGeomType = GeomType.POLYGON;
        if ( geomBase.getGeomType() == outputGeomType) {
            System.out.println("[INFO] - GeomBaseSecondaryTranslators - already is a " + outputGeomType.name());
            return geomBase;

        } else if ( geomBase.getGeomType() == GeomType.POLYLINE || geomBase.getGeomType() == GeomType.SNAKE || geomBase.getGeomType() == GeomType.BLOCK || geomBase.getGeomType() == GeomType.STAMP ) {
            return GeomBasePrimaryTranslators.toPoint(geomBase);

        } else if ( geomBase.getGeomType() == GeomType.POINT ) {
            GeomBase stamp = GeomBasePrimaryTranslators.toStamp(geomBase);
            return GeomBasePrimaryTranslators.toPolygon(stamp);

        } else {
            throw new IllegalArgumentException(GeomBasePrimaryTranslators.exceptionString(geomBase.getGeomType(), outputGeomType));
        }
    }

    public static GeomBase toRectangle(GeomBase geomBase) throws IllegalArgumentException {
        GeomType outputGeomType = GeomType.RECTANGLE;
        if ( geomBase.getGeomType() == outputGeomType) {
            System.out.println("[INFO] - GeomBaseSecondaryTranslators - already is a " + outputGeomType.name());
            return geomBase;

        } else {
            return GeomBaseSecondaryTranslators.toPolygon(geomBase);
        }
    }

    public static GeomBase toStamp(GeomBase geomBase) throws IllegalArgumentException {
        GeomType outputGeomType = GeomType.STAMP;
        if ( geomBase.getGeomType() == outputGeomType) {
            System.out.println("[INFO] - GeomBaseSecondaryTranslators - already is a " + outputGeomType.name());
            return geomBase;

        } else if ( geomBase.getGeomType() == GeomType.POINT || geomBase.getGeomType() == GeomType.SNAKE || geomBase.getGeomType() == GeomType.BLOCK || geomBase.getGeomType() == GeomType.POLYGON || geomBase.getGeomType() == GeomType.RECTANGLE ) {
            return GeomBasePrimaryTranslators.toStamp(geomBase);

        } else if ( geomBase.getGeomType() == GeomType.POLYLINE ) {
            GeomBase polygon = GeomBasePrimaryTranslators.toPolygon(geomBase);
            return GeomBasePrimaryTranslators.toStamp(polygon);

        } else {
            throw new IllegalArgumentException(GeomBasePrimaryTranslators.exceptionString(geomBase.getGeomType(), outputGeomType));
        }
    }

    public static GeomBase toSnake(GeomBase geomBase) throws IllegalArgumentException {
        GeomType outputGeomType = GeomType.SNAKE;
        if ( geomBase.getGeomType() == outputGeomType) {
            System.out.println("[INFO] - GeomBaseSecondaryTranslators - already is a " + outputGeomType.name());
            return geomBase;

        } else if ( geomBase.getGeomType() == GeomType.POLYLINE ) {
            return GeomBasePrimaryTranslators.toSnake(geomBase);

        } else {
            GeomBase geom = GeomBaseSecondaryTranslators.toRectangle(geomBase);
            if ( geom.getGeomType() == GeomType.RECTANGLE ) {
                return GeomBasePrimaryTranslators.toSnake(geomBase);
            } else {
                throw new IllegalArgumentException(GeomBasePrimaryTranslators.exceptionString(geomBase.getGeomType(), outputGeomType));
            }
        }
    }

    public static GeomBase toBlock(GeomBase geomBase) throws IllegalArgumentException {
        return GeomBaseSecondaryTranslators.toBlock(geomBase, GeomType.BLOCK.name() );
    }

    public static GeomBase toBlock(GeomBase geomBase, String name) throws IllegalArgumentException {
        GeomType outputGeomType = GeomType.BLOCK;
        if ( geomBase.getGeomType() == outputGeomType) {
            System.out.println("[INFO] - GeomBaseSecondaryTranslators - already is a " + outputGeomType.name());
            return geomBase;

        } else if ( geomBase.getGeomType() == GeomType.POLYGON || geomBase.getGeomType() == GeomType.RECTANGLE || geomBase.getGeomType() == GeomType.SNAKE ) {
            return GeomBasePrimaryTranslators.toBlock(geomBase, name);

        } else if ( geomBase.getGeomType() == GeomType.POLYLINE || geomBase.getGeomType() == GeomType.STAMP ){
            GeomBase polygon = GeomBasePrimaryTranslators.toPolygon(geomBase);
            return GeomBasePrimaryTranslators.toBlock(polygon, name);

        } else {
            throw new IllegalArgumentException(GeomBasePrimaryTranslators.exceptionString(geomBase.getGeomType(), outputGeomType));
        }
    }

    public static String exceptionString(GeomType geoTypeA, GeomType geoTypeB) {
        return geoTypeA.name() + " has no Primary or Secondary method declared that can translate it to a " + geoTypeB.name();
    }

    public static String exceptionFailureString(GeomType geoTypeA, GeomType geoTypeB) {
        return "something went wrong when trying to translate " + geoTypeA.name() + " to a " + geoTypeB.name();
    }

    public static GeomBase translate(GeomBase inputBase, GeomType outputType) throws Exception {
        if (outputType == GeomType.POINT) {
            return GeomBaseSecondaryTranslators.toPoint(inputBase);
        } else if (outputType == GeomType.POLYLINE) {
            return GeomBaseSecondaryTranslators.toPolyline(inputBase);
        } else if (outputType == GeomType.POLYGON) {
            return GeomBaseSecondaryTranslators.toPolygon(inputBase);
        } else if (outputType == GeomType.RECTANGLE) {
            return GeomBaseSecondaryTranslators.toRectangle(inputBase);
        } else if (outputType == GeomType.SNAKE) {
            return GeomBaseSecondaryTranslators.toSnake(inputBase);
        } else if (outputType == GeomType.BLOCK) {
            return GeomBaseSecondaryTranslators.toBlock(inputBase);
        } else if (outputType == GeomType.STAMP) {
            return GeomBaseSecondaryTranslators.toStamp(inputBase);
        } else {
            throw new Exception(outputType.name() + " has no translation implemented");
        }
    }
}
