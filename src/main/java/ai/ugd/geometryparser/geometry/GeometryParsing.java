package ai.ugd.geometryparser.geometry;

import org.locationtech.jts.geom.*;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

public class GeometryParsing {
    public static List<LinearRing> toLinearRing(Geometry geo) {

        List<LinearRing> outcome = new ArrayList<>();
        if (geo == null) {
            System.out.println("[DEBUG] - GeometryParser.geometry.GeomParsing.toLinearRing - been given a null geometry");
            return null;
        }

        switch (geo.getGeometryType()) {
            case Geometry.TYPENAME_GEOMETRYCOLLECTION:
                GeometryCollection geoCollection = (GeometryCollection) geo;
                for (int i = 0; i < geoCollection.getNumGeometries(); i++) {
                    outcome.addAll(GeometryParsing.toLinearRing(geoCollection.getGeometryN(i)));
                }

                break;
            case Geometry.TYPENAME_MULTIPOLYGON:
                MultiPolygon mPolygon = (MultiPolygon) geo;
                for (int i = 0; i < mPolygon.getNumGeometries(); i++) {
                    outcome.addAll(GeometryParsing.toLinearRing(mPolygon.getGeometryN(i)));
                }

                break;
            case Geometry.TYPENAME_POLYGON:
//                System.out.println(geo);
                Polygon polygon = (Polygon) geo;
                outcome.add(polygon.getExteriorRing());
                for (int i = 0; i < polygon.getNumInteriorRing(); i++) {
//                    System.out.println(i);
                    outcome.add(polygon.getInteriorRingN(i));
                }

                break;

            case Geometry.TYPENAME_LINEARRING:
                outcome.add((LinearRing) geo);

                break;
            default:
                System.out.println("[DEBUG] - GeometryParser.geometry.GeomParsing.toLinearRing - can not deal with geo type " + geo.getGeometryType());

                break;
        }
        System.out.println("[INFO] - GeometryParser.geometry.GeomParsing.toLinearRing - amount of items in outcome : " + outcome.size() );
        return outcome;
    }

    public static List<LineString> toLineString(Geometry geo) {
        
        List<LineString> outcome = new ArrayList<>();

        if (geo == null) {
            System.out.println("[DEBUG] - GeometryParser.geometry.GeomParsing.toLineString - been given a null geometry");
            return null;
        }
        
        switch (geo.getGeometryType() ) {
            case Geometry.TYPENAME_GEOMETRYCOLLECTION:
                GeometryCollection geoCollection = (GeometryCollection) geo;
                for (int i = 0; i < geoCollection.getNumGeometries(); i++) {
                    outcome.addAll(GeometryParsing.toLineString(geoCollection.getGeometryN(i)));
                }

                break;
            case Geometry.TYPENAME_MULTIPOLYGON:
                MultiPolygon mPolygon = (MultiPolygon) geo;
                for (int i = 0; i < mPolygon.getNumGeometries(); i++) {
                    outcome.addAll(GeometryParsing.toLineString(mPolygon.getGeometryN(i)));
                }

                break;
            case Geometry.TYPENAME_POLYGON:
//                System.out.println(geo);
                Polygon polygon = (Polygon) geo;
                outcome.add(polygon.getExteriorRing());
                for (int i = 0; i < polygon.getNumInteriorRing(); i++) {
//                    System.out.println(i);
                    outcome.add(polygon.getInteriorRingN(i));
                }

                break;

            case Geometry.TYPENAME_LINEARRING:
                outcome.add( (LineString) geo);

                break;
            
            
            case Geometry.TYPENAME_MULTILINESTRING:
                MultiLineString mls = (MultiLineString) geo;
                for (int i = 0; i < mls.getNumGeometries(); i++) {
                    outcome.addAll(GeometryParsing.toLineString(mls.getGeometryN(i)));
                }

                break;
            case Geometry.TYPENAME_LINESTRING:
                outcome.add( (LineString) geo);

                break;

            default:
                
                System.out.println("[DEBUG] - GeometryParser.geometry.GeomParsing.toLineString - can not deal with geo type " + geo.getGeometryType());

                break;
        }

        System.out.println("[INFO] - GeometryParser.geometry.GeomParsing.toLinearRing - amount of items in outcome : " + outcome.size() );
        return outcome;
    }
    
    public static List<Polygon> toPolygon(Geometry geo) {

        List<Polygon> outcome = new ArrayList<>();
        if (geo == null) {
            System.out.println("[DEBUG] - GeometryParser.geometry.GeomParsing.toPolygon - been given a null geometry");
            return null;
        }

        switch (geo.getGeometryType()) {
            case Geometry.TYPENAME_GEOMETRYCOLLECTION:
                GeometryCollection geoCollection = (GeometryCollection) geo;
                for (int i = 0; i < geoCollection.getNumGeometries(); i++) {
                    outcome.addAll(GeometryParsing.toPolygon(geoCollection.getGeometryN(i)));
                }

                break;
            case Geometry.TYPENAME_MULTIPOLYGON:
                MultiPolygon mPolygon = (MultiPolygon) geo;
                for (int i = 0; i < mPolygon.getNumGeometries(); i++) {
                    outcome.addAll(GeometryParsing.toPolygon(mPolygon.getGeometryN(i)));
                }

                break;
            case Geometry.TYPENAME_POLYGON:
                outcome.add( (Polygon) geo);

                break;
            case Geometry.TYPENAME_MULTILINESTRING:
                MultiLineString mls = (MultiLineString) geo;
                for (int i = 0; i < mls.getNumGeometries(); i++) {
                    outcome.addAll(GeometryParsing.toPolygon(mls.getGeometryN(i)));
                }

                break;
            case Geometry.TYPENAME_LINESTRING:
                outcome.addAll( GeometryParsing.toPolygon( new GeometryFactory().createLinearRing( geo.getCoordinates() ) ) ) ;

                break;
            case Geometry.TYPENAME_LINEARRING:
                outcome.add(new GeometryFactory().createPolygon((LinearRing) geo) );

                break;
            default:
                System.out.println("[DEBUG] - GeometryParser.geometry.GeomParsing.toPolygon - can not deal with geo type " + geo.getGeometryType());

                break;
        }
        System.out.println("[INFO] - GeometryParser.geometry.GeomParsing.toPolygon - amount of items in outcome : " + outcome.size());
        return outcome;
    }

//    public static Point2D pointFromDoubles(List<Double> dbls) {
//        return new Point2D.Double(dbls.get(0), dbls.get(1) );
//    }
//
//    public static List<Point2D> pointsFromDoubles(List<List<Double>> dblls){
//        List<Point2D> pts = new ArrayList<>();
//        for (List<Double> dbls : dblls) {
//            pts.add(GeometryParsing.pointFromDoubles(dbls) );
//        }
//        return pts;
//    }

    public static List<Point2D> pointsFromCoordinates(Coordinate[] crds) {
        List<Point2D> pts = new ArrayList<>();
        for (Coordinate crd : crds) {
            pts.add(new Point2D.Double(crd.getX(), crd.getY() ) );
        }

        return pts;
    }
}