package ai.ugd.geometryparser.geometry;

import org.locationtech.jts.geom.*;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GeometryHelpers {
    public static LineString shortenLineString(LineString ls, double amount) {

        // checking whether the string is not too short
        if (ls.getLength() < 2. * amount) {
            return null;
        }

        // getting the coordinate sequence
        Coordinate[] coords = ls.getCoordinates();
        List<Coordinate> coordList = Arrays.asList(coords) ;

        // start
        Coordinate lastCoordinate = coordList.remove(0);
        double totalLength = 0.0;
        while (totalLength < amount) {
            Coordinate nextCoordinate = coordList.get(0);
            double segmentLength = lastCoordinate.distance(nextCoordinate);
            totalLength += segmentLength;

            if (totalLength >= amount) {
                double scaleVal = (totalLength - amount) / segmentLength ;
                double newX = nextCoordinate.x  + ( lastCoordinate.x - nextCoordinate.x ) * scaleVal;
                double newY = nextCoordinate.y  + ( lastCoordinate.y - nextCoordinate.y ) * scaleVal;

                coordList.add(0, new Coordinate(newX, newY) ) ;

            } else { lastCoordinate = coordList.remove(0) ; }
        }

        // end
        lastCoordinate = coordList.remove(coordList.size() - 1);
        totalLength = 0.0;
        while (totalLength < amount) {
            Coordinate nextCoordinate = coordList.get(coordList.size() - 1);
            double segmentLength = lastCoordinate.distance(nextCoordinate);
            totalLength += segmentLength;

            if (totalLength >= amount) {
                double scaleVal = (totalLength - amount) / segmentLength ;
                double newX = nextCoordinate.x  + ( lastCoordinate.x - nextCoordinate.x ) * scaleVal;
                double newY = nextCoordinate.y  + ( lastCoordinate.y - nextCoordinate.y ) * scaleVal;

                coordList.add(0, new Coordinate(newX, newY) ) ;

            } else { lastCoordinate = coordList.remove(coordList.size() - 1) ; }
        }

        // outputing the linestring
        return new GeometryFactory().createLineString( (Coordinate[]) coordList.toArray() );
    }

    private static boolean shortenCoordinatePair(Coordinate[] pair, double shortening, int sides) {
        if (! (pair.length == 2) ) {
            throw new InvalidParameterException("you can should give exact two values for the pair");
        }

        Coordinate a = pair[0];
        Coordinate b = pair[1];

        double distance = a.distance(b);
        boolean isLongEnough = (distance > shortening * 2);

        if (isLongEnough) {
            double deltaS = shortening / distance;
            double deltaE = 1.0 - deltaS;
            double deltaX = b.x - a.x;
            double deltaY = b.y - a.y;
            Coordinate startPt = new Coordinate(a.x + deltaS * deltaX, a.y + deltaS * deltaY);
            Coordinate endPt = new Coordinate(a.x + deltaE * deltaX, a.y + deltaE * deltaY);

            switch (sides) {
                case 1: // only end of segment is shortened
                    pair[0] = new Coordinate(a);
                    pair[1] = endPt;
                    break;

                case 2: // only start of segment is shortened
                    pair[0] = startPt;
                    pair[1] = new Coordinate(b);
                    break;

                default: // so also in case sides == 0 - both sides are shortened
                    pair[0] = startPt;
                    pair[1] = endPt;
                    break;
            }

            return true;
        } else return false;
    }

    public static List<Coordinate[]> splitLineStringsIntoCoordPairs(LineString ls, double shortening, boolean endsShortened) {
        // segmenting line strings
        // start and end point will always remain the same
        // if need for subdividing, (means more than two coordinates), all the segments that are linked with one another
        // are shortened on the covered sides by the shortening distance

        List<Coordinate[]> coordList = new ArrayList<>();
        if (ls.getNumPoints() > 2) {
            GeometryFactory gf = new GeometryFactory();
            Coordinate[] coords = ls.getCoordinates();

            if (endsShortened) {
                for (int i = 0; i < coords.length - 1; i++) {
                    Coordinate[] pair = new Coordinate[]{coords[i], coords[i + 1]};
                    if (GeometryHelpers.shortenCoordinatePair(pair, shortening, 0)) {
                        coordList.add(pair);
                    }
                }

            } else {
                Coordinate[] pairStart = new Coordinate[]{coords[0], coords[1]};
                if (GeometryHelpers.shortenCoordinatePair(pairStart, shortening, 1)) {
                    coordList.add(pairStart);
                }
    
                if (ls.getNumPoints() > 3) {
                    for (int i = 1; i < coords.length - 2; i++) {
                        Coordinate[] pair = new Coordinate[]{coords[i], coords[i + 1]};
                        if (GeometryHelpers.shortenCoordinatePair(pair, shortening, 0)) {
                            coordList.add(pair);
                        }
                    }
                }
    
                Coordinate[] pairEnd = new Coordinate[]{coords[coords.length - 2], coords[coords.length - 1]};
                if (GeometryHelpers.shortenCoordinatePair(pairEnd, shortening, 2)) {
                    coordList.add(pairStart);
                }
            }

        } else {
            coordList.add(ls.getCoordinates() );
        }

        return coordList;
    }

    public static List<Coordinate[]> splitLineStringsIntoCoordPairs(LineString ls, double shortening) {
        return GeometryHelpers.splitLineStringsIntoCoordPairs(ls, shortening, true);
    }

    private static List<Coordinate> reduceCoordinatesList(List<Coordinate> coors) {
        Coordinate[] tmpCoorArray = new Coordinate[coors.size()];
        coors.toArray(tmpCoorArray);
        return GeometryHelpers.reduceCoordinates(tmpCoorArray);
    }

    public static List<Coordinate> reduceCoordinates(Coordinate[] coors) {
        List<Coordinate> output = new ArrayList<>();
        for (Coordinate coor : coors) {
            boolean tooClose = false;
            for (Coordinate cr : output) {
                if (cr.distance(coor) < .0001) {
                    tooClose = true;
                    break;
                }
            }

            if (!tooClose) {
                output.add(coor);
            }
        }
        return output;
    }

    public static List<Coordinate> reduceCoordinateLists(List<Coordinate[]> coords) {
        List<Coordinate> output = new ArrayList<>();
        for (Coordinate[] coors : coords) {
            output.addAll(GeometryHelpers.reduceCoordinates(coors) );
        }
        return GeometryHelpers.reduceCoordinatesList(output);
    }

    public static List<LineString> splitlineStringsIntoSegments(LineString ls, double shortening) {
        List<LineString> lls = new ArrayList<>();
        GeometryFactory gf = new GeometryFactory();
        for (Coordinate[] coor : GeometryHelpers.splitLineStringsIntoCoordPairs(ls, shortening) ) {
            lls.add(gf.createLineString(coor) );
        }
        return lls;
    }

    public static Coordinate[] multiLineStringToNetwork(List<LineString> lls, List<List<Integer>> graphList) {
        List<Coordinate> css = new ArrayList<>();
        List<Coordinate> allCss = new ArrayList<>();

        // get all the points
        for (LineString ls : lls) {
            for (Coordinate c : ls.getCoordinates() ) {
                allCss.add(c);
            }
        }

        // filter the points that are already there
        for (Coordinate c : allCss) {
            boolean isAlready = false;
            for (Coordinate c_i : css) {
                if (c_i.distance(c) < GeomBaseFactories.distanceTolerance) {
                    isAlready = true;
                    break;
                }
            }

            if (!isAlready) {
                css.add(c);
//                System.out.println(c);
            }
        }

        // replacing the lineString coordinates by the correct indexes of the points in the css list
        for (LineString ls : lls) {
            List<Integer> localIndex = new ArrayList<>();
            Coordinate[] coords = ls.getCoordinates();
            for (Coordinate coor : coords){
//                System.out.println(coor);
                int i = 0;
                for (Coordinate c : css) {
                    if (coor.distance(c) < GeomBaseFactories.distanceTolerance) {
                        localIndex.add(i);
//                        System.out.println(" - " + i + " - " + c);
                        break;
                    }
                    i ++;
                }
            }

            graphList.add(localIndex);
        }

        // turning the coordinates into an array
        Coordinate[] allCssArray = new Coordinate[allCss.size()];
        for (int i = 0; i < allCss.size(); i ++) {
            allCssArray[i] = allCss.get(i);
        }
        return allCssArray;
    }

    public static Coordinate[] multiLineStringToNetwork(Geometry mls, List<List<Integer>> graphList) {
        return GeometryHelpers.multiLineStringToNetwork(GeometryParsing.toLineString(mls), graphList);
    }
}
