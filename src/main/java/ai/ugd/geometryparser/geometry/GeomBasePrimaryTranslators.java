package ai.ugd.geometryparser.geometry;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.Point;

//              primary translations                            secondary translation                   total
// toPoint -    Stamp                                           Polygon (/Rectangle), Block, Snake      Stamp, Polygon (/Rectangle), Block, Snake, Polyline
// toPolyline - Snake                                           Rectangle                               Snake (any other if they form a Rectangle)
// toPolygon -  Polyline, Snake, Block, Stamp                   Point                                   Polyline, Snake, Block, Stamp, Point
// toRectangle  (special case of Polygon)                       (special case of Polygon)               (special case of Polygon)
// toStamp -    Point, Polygon (/Rectangle), Block, Snake       Polyline                                Point, Polygon (/Rectangle), Block, Snake, Polyline
// toSnake -    Polyline, Rectangle                             (any other if they form a Rectangle)    Polyline, Rectangle (any other if they form a Rectangle)
// toBlock -    Polygon (/Rectangle)                            Polyline, Stamp, Snake                  Polygon (/Rectangle), Snake, Polyline, Stamp

public class GeomBasePrimaryTranslators {
    public static GeomBase toPoint(GeomBase geomBase) throws IllegalArgumentException {
        GeomType outputGeomType = GeomType.POINT;
        if ( geomBase.getGeomType() == outputGeomType) {
            System.out.println("[INFO] - GeomBasePrimaryTranslators - already is a " + outputGeomType.name());
            return geomBase;

        } else if ( geomBase.getGeomType() == GeomType.STAMP ) {
            return new GeomBase(geomBase.getUUID(), geomBase.getInternalGeo().getCoordinate(), outputGeomType);

        } else {
            throw new IllegalArgumentException(GeomBasePrimaryTranslators.exceptionString(geomBase.getGeomType(), outputGeomType));

        }
    }

    public static GeomBase toPolyline(GeomBase geomBase) throws IllegalArgumentException {
        GeomType outputGeomType = GeomType.POLYLINE;
        if ( geomBase.getGeomType() == outputGeomType) {
            System.out.println("[INFO] - GeomBasePrimaryTranslators - already is a " + outputGeomType.name());
            return geomBase;

        } else if ( geomBase.getGeomType() == GeomType.SNAKE ) {
            return new GeomBase(geomBase.getUUID(), geomBase.getInternalGeo().getCoordinates(), outputGeomType);

        } else {
            throw new IllegalArgumentException(GeomBasePrimaryTranslators.exceptionString(geomBase.getGeomType(), outputGeomType));

        }
    }

    public static GeomBase toPolygon(GeomBase geomBase) throws IllegalArgumentException {
        GeomType outputGeomType = GeomType.POLYGON;
        if ( geomBase.getGeomType() == outputGeomType) {
            System.out.println("[INFO] - GeomBasePrimaryTranslators - already is a " + outputGeomType.name());
            return geomBase;

        } else if ( geomBase.getGeomType() == GeomType.POLYLINE ) {
            return new GeomBase(geomBase.getUUID(), geomBase.getInternalGeo().getCoordinates(), outputGeomType);

        } else if ( geomBase.getGeomType() == GeomType.SNAKE || geomBase.getGeomType() == GeomType.BLOCK ) {
            return new GeomBase(geomBase.getUUID(), geomBase.getExternalGeo().getCoordinates(), outputGeomType);

        } else if ( geomBase.getGeomType() == GeomType.STAMP ) {
            Coordinate[] coords = GeomBaseFactories.makeCircleFromCoor(geomBase.getInternalGeo().getCoordinate(), geomBase.getRadius() );
            return new GeomBase(geomBase.getUUID(), coords, outputGeomType);

        } else {
            throw new IllegalArgumentException(GeomBasePrimaryTranslators.exceptionString(geomBase.getGeomType(), outputGeomType));

        }
    }

    public static GeomBase toRectangle(GeomBase geomBase) throws IllegalArgumentException {
        if ( geomBase.getGeomType() == GeomType.RECTANGLE) {
            System.out.println("[INFO] - GeomBasePrimaryTranslators - already is a " + GeomType.RECTANGLE.name());
            return geomBase;
        } else {
            return GeomBasePrimaryTranslators.toPolygon(geomBase);
        }
    }

    public static GeomBase toStamp(GeomBase geomBase) throws IllegalArgumentException {
        return GeomBasePrimaryTranslators.toStamp(geomBase, null);
    }

    public static GeomBase toStamp(GeomBase geomBase, Double radius) throws IllegalArgumentException {
        GeomType outputGeomType = GeomType.STAMP;
        if ( geomBase.getGeomType() == outputGeomType) {
            System.out.println("[INFO] - GeomBasePrimaryTranslators - already is a " + outputGeomType.name());
            return geomBase;

        } else if ( geomBase.getGeomType() == GeomType.POINT ) {
            if (radius == null) {
                return new GeomBase( geomBase.getUUID(), geomBase.getInternalGeo().getCoordinate(), outputGeomType );
            } else {
                return new GeomBase( geomBase.getUUID(), geomBase.getInternalGeo().getCoordinate(), radius, outputGeomType );
            }

        } else if ( geomBase.getGeomType() == GeomType.POLYGON || geomBase.getGeomType() == GeomType.RECTANGLE || geomBase.getGeomType() == GeomType.BLOCK || geomBase.getGeomType() == GeomType.SNAKE ) {
            return GeomBasePrimaryTranslators.toStamp(geomBase, geomBase.getExternalGeo(), radius);

        } else {
            throw new IllegalArgumentException(GeomBasePrimaryTranslators.exceptionString(geomBase.getGeomType(), outputGeomType));
        }
    }

    public static GeomBase toStamp(GeomBase geomBase, Geometry geo, Double radius) throws IllegalArgumentException {
        // from a lr, getting its centroid and setting the stamp radius as the largest distance to the centroid
        Point ptCenter = geo.getCentroid();
        Coordinate centroid = new Coordinate(ptCenter.getX(), ptCenter.getY());
        Coordinate[] coords = geo.getCoordinates();

        if (radius == null) {
            radius = 0.0;
            for (Coordinate coor : coords) {
                double distance = centroid.distance(coor);
                if (radius < distance) {
                    radius = distance;
                }
            }
        }

        return new GeomBase(geomBase.getUUID(), geo.getCoordinate(), radius, GeomType.STAMP);
    }

    public static GeomBase toSnake(GeomBase geomBase) throws IllegalArgumentException {
        return GeomBasePrimaryTranslators.toSnake(geomBase, null);
    }

    public static GeomBase toSnake(GeomBase geomBase, Double buffer) throws IllegalArgumentException {
        GeomType outputGeomType = GeomType.SNAKE;
        if ( geomBase.getGeomType() == outputGeomType) {
            System.out.println("[INFO] - GeomBasePrimaryTranslators - already is a " + outputGeomType.name());
            return geomBase;

        } else if ( geomBase.getGeomType() == GeomType.POLYLINE ) {
            if (buffer == null) {
                return new GeomBase(geomBase.getUUID(), geomBase.getInternalGeo().getCoordinates(), outputGeomType);
            } else {
                return new GeomBase(geomBase.getUUID(), geomBase.getInternalGeo().getCoordinates(), buffer, outputGeomType);
            }
        } else if ( geomBase.getGeomType() == GeomType.RECTANGLE ){
            Point ptCenter = geomBase.getInternalGeo().getCentroid();
            Coordinate[] coords = geomBase.getInternalGeo().getCoordinates();
            double width = coords[0].distance(coords[1]);
            double length = coords[1].distance(coords[2]);

            if (width - length < .01){
                throw new IllegalArgumentException(GeomBasePrimaryTranslators.exceptionFailureString(geomBase.getGeomType(), outputGeomType));
            }

            Coordinate[] outputCoordinates = new Coordinate[2];
            if ( width < length ){
                double multiplier = (.5 - .5 * width / length);
                double xDelta = ( coords[1].x - coords[2].x ) * multiplier;
                double yDelta = ( coords[1].y - coords[2].y ) * multiplier;
                outputCoordinates[0] = new Coordinate(ptCenter.getX() + xDelta, ptCenter.getY() + yDelta);
                outputCoordinates[1] = new Coordinate(ptCenter.getX() - xDelta, ptCenter.getY() - yDelta);
            } else {
                double multiplier = (.5 - .5 * length / width);
                double xDelta = ( coords[0].x - coords[1].x ) * multiplier;
                double yDelta = ( coords[0].y - coords[1].y ) * multiplier;
                outputCoordinates[0] = new Coordinate(ptCenter.getX() + xDelta, ptCenter.getY() + yDelta);
                outputCoordinates[1] = new Coordinate(ptCenter.getX() - xDelta, ptCenter.getY() - yDelta);
            }

            buffer = Math.abs( (width - length) * .5 );

            return new GeomBase(geomBase.getUUID(), outputCoordinates, buffer, outputGeomType);
        } else {
            throw new IllegalArgumentException(GeomBasePrimaryTranslators.exceptionString(geomBase.getGeomType(), outputGeomType));
        }
    }

    public static GeomBase toBlock(GeomBase geomBase) throws IllegalArgumentException {
        return GeomBasePrimaryTranslators.toBlock(geomBase, GeomType.BLOCK.name() );
    }

    public static GeomBase toBlock(GeomBase geomBase, String name) throws IllegalArgumentException {
        GeomType outputGeomType = GeomType.BLOCK;
        if ( geomBase.getGeomType() == outputGeomType) {
            System.out.println("[INFO] - GeomBasePrimaryTranslators - already is a " + outputGeomType.name());
            return geomBase;

        } else if ( geomBase.getGeomType() == GeomType.POLYGON || geomBase.getGeomType() == GeomType.RECTANGLE ) {
            return new GeomBase(geomBase.getUUID(), geomBase.getInternalGeo().getCoordinates(), name, outputGeomType);

        } else if ( geomBase.getGeomType() == GeomType.SNAKE ){
            return new GeomBase(geomBase.getUUID(), geomBase.getExternalGeo().getCoordinates(), name, outputGeomType);

        } else {
            throw new IllegalArgumentException(GeomBasePrimaryTranslators.exceptionString(geomBase.getGeomType(), outputGeomType));
        }
    }

    public static String exceptionString(GeomType geoTypeA, GeomType geoTypeB) {
        return geoTypeA.name() + " has no Primary method declared that can translate it to a " + geoTypeB.name();
    }

    public static String exceptionFailureString(GeomType geoTypeA, GeomType geoTypeB) {
        return "something went wrong when trying to translate " + geoTypeA.name() + " to a " + geoTypeB.name();
    }
    
    public static GeomBase translate(GeomBase inputBase, GeomType outputType) throws Exception {
        if (outputType == GeomType.POINT) {
            return GeomBasePrimaryTranslators.toPoint(inputBase);
        } else if (outputType == GeomType.POLYLINE) {
            return GeomBasePrimaryTranslators.toPolyline(inputBase);
        } else if (outputType == GeomType.POLYGON) {
            return GeomBasePrimaryTranslators.toPolygon(inputBase);
        } else if (outputType == GeomType.RECTANGLE) {
            return GeomBasePrimaryTranslators.toRectangle(inputBase);
        } else if (outputType == GeomType.SNAKE) {
            return GeomBasePrimaryTranslators.toSnake(inputBase);
        } else if (outputType == GeomType.BLOCK) {
            return GeomBasePrimaryTranslators.toBlock(inputBase);
        } else if (outputType == GeomType.STAMP) {
            return GeomBasePrimaryTranslators.toStamp(inputBase);
        } else {
            throw new Exception(outputType.name() + " has no translation implemented");
        }
    }
    
    public static GeomBase translateToImplicit(GeomBase inputBase) throws Exception {
        if (inputBase.isImplicit() ) {
            return inputBase;
        } else if (inputBase.getGeomType() == GeomType.POINT) {
            return GeomBasePrimaryTranslators.toStamp(inputBase);
        } else if (inputBase.getGeomType() == GeomType.POLYLINE || inputBase.getGeomType() == GeomType.RECTANGLE) {
            return GeomBasePrimaryTranslators.toSnake(inputBase);
        } else {
            throw new Exception(inputBase.getGeomType().name() + " has no translation to implicit implemented");
        }
    }
}
