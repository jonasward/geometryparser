package ai.ugd.geometryparser.geometry;

import ai.ugd.geometryparser.data.LayerValidityCheckLogic;
import org.locationtech.jts.geom.*;
import org.locationtech.jts.simplify.DouglasPeuckerSimplifier;

import java.util.ArrayList;
import java.util.List;

// Class that defines the static methods which executes both the reflective as the relational geometry constraints
// defined in GeomInvalidityType. These methods always apply themselves onto a (list of) GeomBases. There is therefore
// no need to re-assign them.
// Class has two parts: reflective methods & relational methods. In both cases, the geometric operations are defined on top
// extra overloads are defined thereafter.

public class GeomValidityCheck {

    // GeomBase based
    public static boolean checkReflexiveValidity(GeomBase geomBase) {
        return (GeomValidityCheck.checkSelfIntersection(geomBase) ||
            GeomValidityCheck.checkDoublePoints(geomBase) ||
            GeomValidityCheck.checkEndPoints(geomBase) );
    }

    private static boolean checkSelfIntersection(GeomBase geom) {
        if (GeomInvalidityType.SELFINTERSECTS.applies(geom) ) {
            Geometry intGeom = geom.getInternalGeo();

            boolean selfIntersects = !intGeom.isSimple();
            if (selfIntersects) geom.addInvalidity(GeomInvalidityType.SELFINTERSECTS);

            return (selfIntersects);
        } else { return false; }
    }

    private static boolean checkDoublePoints(GeomBase geom) {
        if (GeomInvalidityType.DOUBLEPOINTS.applies(geom) ) {
            Geometry intGeom = geom.getInternalGeo();

            boolean hasDoublePoints = (intGeom.getNumPoints() != (DouglasPeuckerSimplifier.simplify(intGeom, .001)).getNumPoints() );
            if (hasDoublePoints) geom.addInvalidity(GeomInvalidityType.DOUBLEPOINTS);

            return hasDoublePoints;
        } else { return false; }
    }

    private static boolean checkEndPoints(GeomBase geom) {
        if (GeomInvalidityType.ENDSTARTOVERLAP.applies(geom) ) {
            Geometry intGeom = geom.getInternalGeo();

            boolean endStartOverlaps = ( ( (LineString) intGeom ).isClosed() );
            if (endStartOverlaps) geom.addInvalidity(GeomInvalidityType.ENDSTARTOVERLAP);

            return endStartOverlaps;
        } else {
            return false;
        }
    }

    // GeomBase with Geometry Based
    public static boolean checkIfContained(GeomBase geom, Geometry other) {
        if (GeomInvalidityType.NOTCONTAINED.applies(geom) ) {
            Geometry geometry = other.buffer(GeomInvalidityType.notContainedBuffer);

            boolean isNotContained = (! geometry.contains(geom.getInternalGeo() ) );
            if (isNotContained) geom.addInvalidity(GeomInvalidityType.NOTCONTAINED);

            return isNotContained;
        } else { return false; }
    }
    
    public static boolean checkIfIntersects(GeomBase geom, Geometry other) {
        if (GeomInvalidityType.INTERSECTS.applies(geom) ) {
            Geometry intGeom = geom.getInternalGeo();
            if (intGeom.getGeometryType().equals( Geometry.TYPENAME_LINESTRING ) ) {
                intGeom = ( GeometryHelpers.shortenLineString( (LineString) intGeom, GeomInvalidityType.intersectionShortening) );

                boolean intersects = intGeom.intersects(other);
                if (intersects) geom.addInvalidity(GeomInvalidityType.INTERSECTS);

                return intersects;
            } else return false;
        } else { return false; }
    }

    public static boolean checkIfNeighbours(GeomBase geom, Geometry other) {
        if (GeomInvalidityType.NOPARENT.applies(geom) ) {
            boolean hasNoNeighbour = !(geom.getInternalGeo().distance(other) < GeomInvalidityType.distanceTolerance);
            if (hasNoNeighbour) geom.addInvalidity(GeomInvalidityType.NOPARENT);

            return hasNoNeighbour;
        } else { return false; }
    }

    public static boolean checkIfEndsCovered(GeomBase geom, Geometry other) {
        if (GeomInvalidityType.UNCOVEREDENDS.applies(geom) ) {
            GeometryFactory gf = new GeometryFactory();
            Coordinate[] coords = geom.getInternalGeo().getCoordinates();
            Geometry pt0 = gf.createPoint(coords[0] );
            Geometry pt1 = gf.createPoint(coords[coords.length - 1] );
            Geometry bufferGeom = other.buffer(GeomInvalidityType.notContainedBuffer);

            boolean nonCoveredEnds = !(bufferGeom.contains(pt0) && bufferGeom.contains(pt1) );
            if (nonCoveredEnds) geom.addInvalidity(GeomInvalidityType.UNCOVEREDENDS);

            return nonCoveredEnds;
        } else {
            return false;
        }
    }

    // GeomBase with other GeomBases
    private static Geometry validGeometries(GeomInvalidityType invalidityType, List<GeomBase> geomBases) {
        List<Geometry> geometryCollection = new ArrayList<>();
        for (GeomBase geom : geomBases) {
            if (invalidityType.relationApplies(geom) ) {
                Geometry externalGeo = geom.getExternalGeo();
                if (!(externalGeo==null) ) {
                    geometryCollection.add(geom.getInternalGeo());
                } else {
                    System.out.println("[INFO] - ai.ugd.geometryparser.geometry.GeomValidityCheck - had to filter out a null geometry");
                }
            } else {
                System.out.println("[INFO] - ai.ugd.geometryparser.geometry.GeomValidityCheck - had to filter out this object : " + geom.getGeomType());
            }
        }

        // how to cast list into array :
        Geometry[] geos = new Geometry[geometryCollection.size()];      // initialize an array of the correct size
        geos = geometryCollection.toArray(geos);                        // convert the list onto the array

        return new GeometryFactory().createGeometryCollection( geos );
    }

    // list of GeomBases with the others
    private static boolean checkIfContained(GeomBase geom, List<GeomBase> others) {
        return GeomValidityCheck.checkIfContained(geom, GeomValidityCheck.validGeometries(GeomInvalidityType.NOTCONTAINED, others));
    }

    private static boolean checkIfIntersects(GeomBase geom, List<GeomBase> others) {
        return GeomValidityCheck.checkIfIntersects(geom, GeomValidityCheck.validGeometries(GeomInvalidityType.INTERSECTS, others));
    }

    private static boolean checkIfNeighbours(GeomBase geom, List<GeomBase> others) {
        return GeomValidityCheck.checkIfNeighbours(geom, GeomValidityCheck.validGeometries(GeomInvalidityType.NOPARENT, others));
    }

    private static boolean checkIfEndsCovered(GeomBase geom, List<GeomBase> others) {
        return GeomValidityCheck.checkIfEndsCovered(geom, GeomValidityCheck.validGeometries(GeomInvalidityType.UNCOVEREDENDS, others));
    }
    
    // Check all for geometry base
    //SELFINTERSECTS, ENDSTARTOVERLAP, DOUBLEPOINTS, INTERSECTS, NOTCONTAINED, NONEIGHBOUR, UNCOVEREDENDS
    
    public static boolean checkValidity(GeomBase geomToBeValidated) {
        LayerValidityCheckLogic validationLogic = geomToBeValidated.getParentLayer().getValidityLogic();
//        List<GeomBase> toValidateWith = validationLogic.getRelatedGeometries(geomToBeValidated, geomToBeValidated.getDataContainer() );
//        return GeomValidityCheck.checkValidity(geomToBeValidated, toValidateWith, validationLogic);
        return false;
    }

    private static boolean checkValidity(GeomBase geomToBeValidated, List<GeomBase> toValidateWith, LayerValidityCheckLogic filter) {
        boolean inValid = true;
        
        if (filter.validityToCheck(GeomInvalidityType.SELFINTERSECTS) ) {
            inValid = (inValid && !GeomValidityCheck.checkSelfIntersection(geomToBeValidated) );
        }
        
        if (filter.validityToCheck(GeomInvalidityType.ENDSTARTOVERLAP) ) {
            inValid = (inValid && !GeomValidityCheck.checkEndPoints(geomToBeValidated) );
        }
        
        if (filter.validityToCheck(GeomInvalidityType.DOUBLEPOINTS) ) {
            inValid = (inValid && !GeomValidityCheck.checkDoublePoints(geomToBeValidated) );
        }
        
        if (filter.validityToCheck(GeomInvalidityType.INTERSECTS) ) {
            inValid = (inValid && !GeomValidityCheck.checkIfIntersects(geomToBeValidated, toValidateWith) );
        }
        
        if (filter.validityToCheck(GeomInvalidityType.NOTCONTAINED) ) {
            inValid = (inValid && !GeomValidityCheck.checkIfIntersects(geomToBeValidated, toValidateWith) );
        }

        if (filter.validityToCheck(GeomInvalidityType.NOPARENT) ) {
            inValid = (inValid && !GeomValidityCheck.checkIfNeighbours(geomToBeValidated, toValidateWith) );
        }

        if (filter.validityToCheck(GeomInvalidityType.UNCOVEREDENDS) ) {
            inValid = (inValid && !GeomValidityCheck.checkIfEndsCovered(geomToBeValidated, toValidateWith) );
        }
        
        return inValid;
    }
}