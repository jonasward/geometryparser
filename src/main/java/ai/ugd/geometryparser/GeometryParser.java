package ai.ugd.geometryparser;

import ai.ugd.geometryparser.parsing.DXFParser;
import ai.ugd.geometryparser.parsing.Outparser;
import ai.ugd.geometryparser.parsing.ParsingConstants;

import java.awt.geom.Point2D;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

public class GeometryParser {
    public static HashMap<String, List<List<Point2D>>> parseHCDDXF(InputStream hcdDXStream){
        DXFParser dxfParser = new DXFParser(hcdDXStream, ParsingConstants.hcdDXFImportLayers);
        dxfParser.parseLayers();
        return Outparser.toNamedPointListsExternalOfGeos(dxfParser.getDataContainer() );
    }
}
