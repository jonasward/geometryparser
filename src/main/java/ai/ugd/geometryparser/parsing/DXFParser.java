package ai.ugd.geometryparser.parsing;

import ai.ugd.geometryparser.data.*;
import ai.ugd.geometryparser.geometry.GeomBaseFactories;
import ai.ugd.geometryparser.geometry.GeomType;
import org.kabeja.dxf.*;
import org.kabeja.dxf.helpers.Point;
import org.kabeja.parser.ParseException;
import org.kabeja.parser.Parser;
import org.kabeja.parser.ParserBuilder;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.geom.util.AffineTransformation;

import java.awt.geom.Point2D;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DXFParser extends ParsingBareBone {

    // basic parser using kabeja
    // to initializations, either filepath or a filestream
    // after parsing, the dxfDocument is extracted from the kabeja parser object
    // then layer by layer, the dxfDocument is deconstructed into layers and objects

    // By Default are these the types that are recognised and parsed:
        // POLYGON = closed Polyline - DXFPolyline of which the last point is the same as the end
        // POLYLINE = list of Point2Ds - Non-Polygon DXFPolyline
        // STAMP = Point2Ds and Circles (with radius)
        // BLOCK = a LinearRing, representing the bounding box of a block entity, also can get a name

    // Different layer import maps (will) be incorporated later on
    // based on the dxf unit settings, the objects getting imported will be scaled

    public static final String defaultGlobalLayerName = "DXFParsing";

    public Parser parserObject = ParserBuilder.createDefaultParser();
    private DXFDocument dxfDocument;

    private double scaleValue = 1.0;                // default coordinate scale value

    // init stream or filepath (with or without a custom LGInparser

    public DXFParser(String filePath) {
        this.initPath(filePath);
    }

    public DXFParser(InputStream dxfStream) {
        this.initStream(dxfStream);
    }

    public DXFParser(String filePath, LayerGeomParsingLogic inparser) {
        this.initPath(filePath);
        this.dataContainer.parsingLogic = inparser;
    }

    public DXFParser(InputStream dxfStream, LayerGeomParsingLogic inparser) {
        this.initStream(dxfStream);
        this.dataContainer.parsingLogic = inparser;
    }
    
    public DXFParser(String filePath, List<LayerSettingsObject> layerSettingsObjects) {
        this.initPath(filePath);
        this.initializeFromLayerSettings(DXFParser.defaultGlobalLayerName, layerSettingsObjects);
    }
    
    public DXFParser(InputStream dxfStream, List<LayerSettingsObject> layerSettingsObjects) {
        this.initStream(dxfStream);
        this.initializeFromLayerSettings(DXFParser.defaultGlobalLayerName, layerSettingsObjects);
    }

    // initialization functions
    private void initPath(String filePath){
        try {
            this.parserObject.parse(filePath);
            this.init();
        } catch (ParseException e) {
            System.out.println("parsing failed");
        }
    }

    private void initStream(InputStream dxfStream) {
        try {
            this.parserObject.parse(dxfStream, org.kabeja.parser.DXFParser.DEFAULT_ENCODING);
            this.init();
        } catch (ParseException e) {
            System.out.println("parsing failed");
        }
    }

    private void init() {
        System.out.println(this.parserObject.getName());
        System.out.println("[INFO] - DXFImport - trying to parse : ");

        this.dxfDocument = this.parserObject.getDocument();
        this.scaleValue = DXFParser.getScaleToMeter(this.dxfDocument);
        System.out.println("[INFO] - DXFImport - parsing success & document set");
//        this.createBlocks();
    }

    public DataContainer getDataContainer(){
        return this.dataContainer;
    }


    // parsing dxf objects to LayerGeometryInparser method objects types

    private void parsePolylines(Layer layerObject, DXFLayer dxfLayer) {
        List<DXFPolyline> plines = dxfLayer.getDXFEntities(DXFConstants.ENTITY_TYPE_POLYLINE);
        try {
            for (DXFPolyline pl : plines) {

                Coordinate[] coords = new Coordinate[pl.getVertexCount()];

                for(int i = 0; i < pl.getVertexCount(); i++){
                    DXFVertex vec = pl.getVertex(i);
                    coords[i] = this.getScaledCoordinate( vec.getX(), vec.getY() );
                }

                layerObject.addGeoBase( layerObject.getParsingLogic().fromCoords(coords, pl.isClosed() ) );
            }
        } catch (NullPointerException e) {
            System.out.println("[INFO] - ImportDXF.parsePolylines in layer '" + layerObject.getName() + "' has no polyline objects");
        }
    }

    private void parseLines(Layer layerObject, DXFLayer dxfLayer) {
        List<DXFLine> lines = dxfLayer.getDXFEntities(DXFConstants.ENTITY_TYPE_LINE);
        try {
            for (DXFLine ln : lines) {

                Point start = ln.getStartPoint();
                Point end = ln.getEndPoint();

                Coordinate[] coords = new Coordinate[] {
                    this.getScaledCoordinate( start.getX(), start.getY() ),
                    this.getScaledCoordinate( end.getX(), end.getY() )
                };

                layerObject.addGeoBase( layerObject.getParsingLogic().fromCoords(coords) );
            }
        } catch (NullPointerException e) {
            System.out.println("[INFO] - ImportDXF.parseLines in layer '" + layerObject.getName() + "' has no line objects");
        }
    }

    private void parseCircles(Layer layerObject, DXFLayer dxfLayer) throws Exception {
        // gets all the circles in a layer, gets their centerpoint and radius

        List<DXFCircle> circles = dxfLayer.getDXFEntities(DXFConstants.ENTITY_TYPE_CIRCLE);
        try {
            for (DXFCircle c : circles) {

                Point cPt = c.getCenterPoint();

                layerObject.addGeoBase( layerObject.getParsingLogic().fromSingleCoordinate(
                        this.getScaledCoordinate( cPt.getX(), cPt.getY() ), c.getRadius() * this.scaleValue ) );
            }
        } catch (NullPointerException e) {
            System.out.println("[INFO] - ImportDXF.parseLines in layer '" + layerObject.getName() + "' has no circle objects");
        }
    }

    private void parseSinglePoints(Layer layerObject, DXFLayer dxfLayer) throws Exception {
        List<Point> pts = dxfLayer.getDXFEntities(DXFConstants.ENTITY_TYPE_POINT);
        try {
            for (Point pt : pts){
                layerObject.addGeoBase( layerObject.getParsingLogic().fromSingleCoordinate(this.getScaledCoordinate(pt.getX(), pt.getY() ) ) );
            }
        } catch (NullPointerException e) {
            System.out.println("[INFO] - ImportDXF.parseSinglePoints in layer '" + layerObject.getName() + "' has no point objects");
        }
    }

    private void parseBlocks(Layer layerObject, DXFLayer dxfLayer) {
        List<DXFInsert> inserts = dxfLayer.getDXFEntities(DXFConstants.ENTITY_TYPE_INSERT);
        try {
            for (DXFInsert insert : inserts) {
                DXFBlock block = this.dxfDocument.getDXFBlock( insert.getBlockID() );
                String blockName = block.getName();
                Bounds bds = block.getBounds();

                double x0 = bds.getMinimumX() * this.scaleValue;
                double x1 = bds.getMaximumX() * this.scaleValue;

                double y0 = bds.getMinimumY() * this.scaleValue;
                double y1 = bds.getMaximumY() * this.scaleValue;

                Geometry pg = GeomBaseFactories.MakeRectanglePGFrom2DArray(new double[][]{new double[]{x0, y0}, new double[]{x1, y1} } );

                Point iPt = insert.getPoint();
                double rotation = insert.getRotate() * Math.PI / 180.0;
                double xScale = insert.getScaleX();
                double yScale = insert.getScaleY();

                AffineTransformation scalingM = AffineTransformation.scaleInstance(xScale, yScale);
                AffineTransformation rotationM = AffineTransformation.rotationInstance(rotation);
                AffineTransformation translationM = AffineTransformation.translationInstance(iPt.getX() * this.scaleValue, iPt.getY() * this.scaleValue);

                pg = translationM.transform( rotationM.transform( scalingM.transform(pg) ) );
                layerObject.addGeoBase( layerObject.getParsingLogic().fromPolygon( (Polygon) pg, blockName) );

//                System.out.println("[INFO] - ImportDXF.parseBlocks - rot: " + rotation + " , xSc: " + xScale + " , ySc: " + yScale);

//                layerObject.addBlock(localBlock.createOffspring(new Point2d(iPt.getX(), iPt.getY()), rotation, xScale, yScale));
            }
        } catch (NullPointerException e) {
            System.out.println("[INFO] - ImportDXF.parseBlocks in layer '" + layerObject.getName() + "' has no block objects");
        }
    }

    private void parseLayer(Layer layerObject) {
        // initializing a layer
        // based on the geometry of the objects, the different geometries will be parsed differently
        // polylines can be transformed into either polygon2d's, polyline2d's, line2d's or point2d's
        // lines into line2d's or point2d's
        // blocks can have either a point representation or a polyline representation (boundingbox)


        DXFLayer dxfLayer = this.dxfDocument.getDXFLayer( layerObject.getName() );
        int failureCount = 0;
        try {
            this.parsePolylines(layerObject, dxfLayer);

            this.parseLines(layerObject, dxfLayer);

            this.parseSinglePoints(layerObject, dxfLayer);

            this.parseCircles(layerObject, dxfLayer);

            if (layerObject.getParsingLogic().contains(GeomType.BLOCK)) {
                this.parseBlocks(layerObject, dxfLayer);
            }

        } catch (Exception e) {
            failureCount++;
        }
            System.out.println("[INFO] - DXFParser.parseLayer - " + layerObject.getName() + " - import failed for " + failureCount + " items.");
        }

    public void parseLayers() {
        if (!this.initialisedLayers) {
            this.noLayerSetParsing();
        }

        for (Layer layer : this.dataContainer.getLayers() ) {
//                try {
            this.parseLayer(layer);
//                } catch (NullPointerException e) {
//                    System.out.println("[INFO] - DxfParser.parseLayers - layer with name: " + layer.getName() + " does not appear in this dxf document");
//                }
        }
    }

    private void noLayerSetParsing() {
        List<Layer> objLayers = new ArrayList<>();

        List<String> layerNames = new ArrayList<>();
        Iterator layerIterator = this.dxfDocument.getDXFLayerIterator();
        while (layerIterator.hasNext()) {
            layerNames.add(((DXFLayer) layerIterator.next()).getName());
        }

        this.initializeFromLayerNames(DXFParser.defaultGlobalLayerName, layerNames);
        this.initialisedLayers = true;
    }

    public Layer parseDocument() {
        this.parseLayers();
        return this.dataContainer.getGlobalLayer();
    }

    public List<String> blockNames() {
        List<String> blockNames = new ArrayList<>();
        Iterator<?> iteratorObject = parserObject.getDocument().getDXFBlockIterator();
        while (iteratorObject.hasNext()) {
            DXFBlock value = (DXFBlock) iteratorObject.next();
            System.out.println(value);
            String blockName = value.getName();
            // filtering of block names starting with an asterix -> seem to be system standard / reserved
            if (!blockName.contains("*"))
                blockNames.add(blockName); // System.out.println("[INFO] - blockname " + blockName + " ignored");
        }
        return blockNames;
    }

    public List<String> getLayerNames() {
        List<String> layerNames = new ArrayList<>();
        Iterator<?> iteratorObject = parserObject.getDocument().getDXFLayerIterator();
        while (iteratorObject.hasNext()) {
            DXFLayer value = (DXFLayer) iteratorObject.next();
            System.out.println(value);
            layerNames.add(value.getName());
        }
        return layerNames;
    }

    public static void writeGeometryContainer(String filePath, GeometryContainer geoContainer) {
        DXFDocument document = new DXFDocument();

        short i = 0;
        for (String layerName : geoContainer.getLayerNames() ) {
            DXFLayer layer = new DXFLayer();
            layer.setName(layerName);
            layer.setColor( i );
            document.addDXFLayer(layer);
            i += 2;
        }

        for (String layerName : geoContainer.getLayerNames() ) {
            DXFLayer layer = document.getDXFLayer(layerName);
            for (List<Point2D> pts : geoContainer.objects.get(layerName) ) {
                DXFPolyline entity = new DXFPolyline();
                for (Point2D pt : pts) {
                    entity.addVertex(new DXFVertex(new Point(pt.getX(), pt.getY(), 0.0)));
                }
                layer.addDXFEntity(entity);
            }
        }
    }

    // values for getting the scale
    private Coordinate getScaledCoordinate(double x, double y) {
        return new Coordinate(x * this.scaleValue, y * this.scaleValue);
    }

    public static int getDXFDrawUnit(DXFDocument dxf) {
        DXFHeader header = dxf.getDXFHeader();
        DXFVariable var = header.getVariable("$INSUNITS");
        if (var == null)
            return 0;
        return var.getIntegerValue("70");

        // !!!
        // http://www.autodesk.com/techpubs/autocad/acad2000/dxf/header_section_group_codes_dxf_02.htm
    }

    public static double getScaleToMeter(DXFDocument dxf) {
        return DXFParser.getScaleToMeter( DXFParser.getDXFDrawUnit(dxf) );
    }

    public static double getScaleToMeter(int originalScale) {
        switch (originalScale) {
            case 1: // Inches
                return 0.0254;
            case 2: // Feet
                return DXFParser.getScaleToMeter(1) * 12.0;
            case 3: // Miles
                return DXFParser.getScaleToMeter(10) * 1760.0;
            case 4: // Millimeters;
                return .001;
            case 5: // Centimeters
                return .01;
            case 6: // Meters
                return 1.0;
            case 7: // Kilometers
                return 1000.0;
            case 8: // Microinches
                return DXFParser.getScaleToMeter(9) * .001;
            case 9: // Mils
                return DXFParser.getScaleToMeter(1) * .001;
            case 10: // Yards
                return DXFParser.getScaleToMeter(2) * 3.0;
            case 11: // Angstroms
                return .0000000001;
            case 12: // Nanometers
                return .000000001;
            case 13: // Microns
                return .000001;
            case 14: // Decimeters
                return .1;
            case 15: // Dekameters
                return 10.;
            case 16: // Hectometers
                return 100.;
            case 17: // Gigameters
                return 1000000000.;
            case 18: // Astronomical Units
                return 149597870700.0;
            case 19: // Light Years
                return 9460730472580800.;
            case 20: // Parsecs
                return 30856775814913673.;
            case 21: // US Survey Feet
                return DXFParser.getScaleToMeter(2);
            default: // assume it to be meters
                return 1.0;
        }
    }
}