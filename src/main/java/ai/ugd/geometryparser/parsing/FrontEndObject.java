package ai.ugd.geometryparser.parsing;

import ai.ugd.geometryparser.data.DataContainer;
import ai.ugd.geometryparser.data.Layer;
import ai.ugd.geometryparser.geometry.*;
import org.json.simple.JSONObject;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class FrontEndObject {
    // identifier
    public UUID uuid;

    // geometry
    public String type;

    public List<List<Double>> vertices = null; // even if point or stamp, this will be used, the size of the list should be just one
    public Double radius = null;
    public List<List<Integer>> connectionGraph = null;
    public List<UUID> referencedObjects;

    // other
    public Boolean isValid;
    public List<String> invalidInformation = new ArrayList<>();
    public String name;

    public String layer;

    public FrontEndObject() {
        // do shit all;
    }

    public FrontEndObject(GeomBase geomBase) {
        this.uuid = geomBase.getUUID();

        // geometry transfer
        this.type = geomBase.getGeomType().name();

        if ( geomBase.isImplicit() ) {
            this.radius = geomBase.getRadius();
        }

        // points
        if ( geomBase.getGeomType() == GeomType.POLYLINE || geomBase.getGeomType() == GeomType.SNAKE || geomBase.getGeomType() == GeomType.POINTCOLLECTION ) {
            this.vertices = Outparser.coordinatesToDoubles( geomBase.getInternalGeo().getCoordinates() );
        } else if ( geomBase.getGeomType() == GeomType.POLYGON || geomBase.getGeomType() == GeomType.RECTANGLE) {
            this.vertices = Outparser.coordinatesToDoubles( geomBase.getInternalGeo().getCoordinates() );
            this.vertices.remove(this.vertices.size() - 1); // removing the last point - all these types are defined by a closed geometry type
        }

        // TODO implement block
        // could also be deconstructed into a multilinestring
        // no clue what to do yet with blocks
        if ( geomBase.getGeomType() == GeomType.BLOCK ) {
            this.vertices = Outparser.coordinatesToDoubles(geomBase.getExternalGeo().getCoordinates());
            this.vertices.remove(this.vertices.size() - 1); // removing the last point - all these types are defined by a closed geometry type
        }

            // single point = stamp
        if ( geomBase.getGeomType() == GeomType.STAMP ) {
            this.vertices = Outparser.coordinatesToDoubles( geomBase.getInternalGeo().getCoordinates() );
        }

        // deconstructing network (bunch of line strings)
        if ( geomBase.getGeomType() == GeomType.NETWORK ) {
            this.connectionGraph = new ArrayList<>();
            this.vertices = Outparser.deconstructingMultilineString(geomBase.getInternalGeo(), this.connectionGraph);
        }

        // validity transfer
        this.isValid = (geomBase.isValid() );
        if (!this.isValid) {
            for (GeomInvalidityType invalidity : geomBase.getInvalidityTypes() ) {
                this.invalidInformation.add(invalidity.name() );
            }
        }

        if (geomBase.getGeomType() == GeomType.BLOCK) {
            this.name = geomBase.getName();
        }

        if (geomBase.getParentLayer() == null) {
            this.layer = null;
        } else {
            this.layer = geomBase.getParentLayer().getName();
        }
    }

    public void writeToDataContainer(DataContainer dC) throws InvalidParameterException {
        Layer layer = dC.getLayer(this.layer);
        if ( this.type.equals( GeomType.POINT.name() ) ) {
            layer.addGeoBase(GeomBaseFactories.initPredefinedPoint(this.uuid, GeomBaseFactories.makePoint2D(this.vertices.get(0) ) ) );
        } else if (this.type.equals(GeomType.POLYLINE.name() ) ) {
            layer.addGeoBase(GeomBaseFactories.initPredefinedPolyline(this.uuid, GeomBaseFactories.makePoint2Ds(this.vertices) ) );
        } else if (this.type.equals(GeomType.POLYGON.name() ) ) {
            layer.addGeoBase(GeomBaseFactories.initPredefinedPolygon(this.uuid, GeomBaseFactories.makePoint2Ds(this.vertices) ) );
        } else if (this.type.equals(GeomType.RECTANGLE.name() ) ) {
            layer.addGeoBase(GeomBaseFactories.initPredefinedPolygon(this.uuid, GeomBaseFactories.makePoint2Ds(this.vertices) ) );
        } else if (this.type.equals(GeomType.STAMP.name() ) ) {
            layer.addGeoBase(GeomBaseFactories.initPredefinedStamp(this.uuid, GeomBaseFactories.makePoint2D(this.vertices.get(0) ), this.radius ) );
        } else if (this.type.equals(GeomType.SNAKE.name() ) ) {
            layer.addGeoBase(GeomBaseFactories.initPredefinedSnake(this.uuid, GeomBaseFactories.makePoint2Ds(this.vertices), this.radius ) );
        } else if (this.type.equals(GeomType.NETWORK.name() ) ) {
            layer.addGeoBase(GeomBaseFactories.initPredefinedNetwork(this.uuid, GeomBaseFactories.makePoint2Ds(this.vertices), this.connectionGraph, this.radius) );
        }
    }

    // json export
    public JSONObject toJSON() {
        JSONObject js = new JSONObject();
        js.put("uuid", this.uuid.toString());
        js.put("type", this.type);
        js.put("layer", this.layer );
//        js.put("scope", this.scope );
        js.put("reference_objects", this.referencedObjects);

        js.put("radius", this.radius);
        js.put("vertices", this.vertices );
        js.put("connections", this.connectionGraph);

        js.put("name", this.name);

        js.put("invalidities", this.invalidInformation);

        return js;
    }
}
