package ai.ugd.geometryparser.parsing;

import ai.ugd.geometryparser.data.DataContainer;
import ai.ugd.geometryparser.data.Layer;
import ai.ugd.geometryparser.data.LayerGeomParsingLogic;
import ai.ugd.geometryparser.data.LayerSettingsObject;

import java.awt.geom.Point2D;
import java.util.HashMap;
import java.util.List;

public class PointParser {
    private HashMap<String, List<List<Point2D> > > pointData;
    private DataContainer dataContainer = new DataContainer("ImportPoints.MasterLayer");

    private boolean initialisedLayers = false;

    public PointParser(HashMap<String, List<List<Point2D> > > pointData) {
        this.pointData = pointData;
    }

    private void parsePoints(Layer layerObject) {
        for (List<Point2D> pts : pointData.get(layerObject.getName( ) ) ) {
            layerObject.getParsingLogic().fromPoints( pts );
        }
    }

    public DataContainer getDataContainer(){
        return this.dataContainer;
    }

    private void parseLayer(Layer layerObject) {
        this.parsePoints(layerObject);
    }

    public List<Layer> initializeLayersFromStrings(List<String> layerNames) {
        this.initialisedLayers = true;
        return this.dataContainer.addLayersFromStrings(layerNames);
    }

    public List<Layer> initializeLayers(List<LayerSettingsObject> leos) {
        this.initialisedLayers = true;
        return this.dataContainer.addLayers(leos);
    }

    public List<Layer> initializeLayers(HashMap<String, LayerGeomParsingLogic> layersWithParseLogic) {
        this.initialisedLayers = true;
        return this.dataContainer.addLayers(layersWithParseLogic);
    }

    public void noLayerSetInitialization(){
        this.initialisedLayers = true;
        for (String layerName : this.pointData.keySet() ) {
            this.dataContainer.addLayer(layerName);
        }
    }

    private void parseDataToLayers() {
        for (Layer layer : this.dataContainer.getLayers() ) {
            try {
                this.parseLayer(layer);
            } catch (NullPointerException e) {
                System.out.println("[INFO] - PointParsing.parseDataToLayers - layer with name: " + layer.getName() + " does not appear in this dxf document");
            }
        }
    }

    public void parseLayers() {
        if (!this.initialisedLayers) {
            this.noLayerSetInitialization();
        }
        this.parseDataToLayers();
    }
}
