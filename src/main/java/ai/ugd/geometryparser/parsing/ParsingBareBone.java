package ai.ugd.geometryparser.parsing;

import ai.ugd.geometryparser.data.DataContainer;
import ai.ugd.geometryparser.data.LayerGeomParsingLogic;
import ai.ugd.geometryparser.data.LayerSettingsObject;

import java.util.HashMap;
import java.util.List;

public class ParsingBareBone {
    protected DataContainer dataContainer;
    protected boolean initialisedLayers = false;
    
    protected void initializeFromLayerNamesAndParsingLogic(String globalLayerName, HashMap<String, LayerGeomParsingLogic> layersWithParseLogic) {
        this.dataContainer = new DataContainer(globalLayerName);
        this.dataContainer.addLayers(layersWithParseLogic);
        this.initialisedLayers = true;
    }
    
    protected void initializeFromLayerNames(String globalLayerName, List<String> layerNames) {
        this.dataContainer = new DataContainer(globalLayerName);
        this.dataContainer.addLayersFromStrings(layerNames);
        this.initialisedLayers = true;
    }
    
    protected void initializeFromLayerSettings(String globalLayerName, List<LayerSettingsObject> layerSettingsObjects) {
        this.dataContainer = new DataContainer(globalLayerName);
        this.dataContainer.addLayers(layerSettingsObjects);
        this.initialisedLayers = true;
    }
}
