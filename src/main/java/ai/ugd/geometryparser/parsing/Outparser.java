package ai.ugd.geometryparser.parsing;

import ai.ugd.geometryparser.data.DataContainer;
import ai.ugd.geometryparser.data.Layer;
import ai.ugd.geometryparser.geometry.GeomBase;
import ai.ugd.geometryparser.geometry.GeomBaseDeconstructor;
import ai.ugd.geometryparser.geometry.GeometryHelpers;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Outparser {
    public static HashMap<String, List<List<Point2D> > > toNamedPointListsExternalOfGeos(DataContainer dataContainer) {
        HashMap<String, List<List<Point2D> > > namedPtList = new HashMap<>();
        for (Layer layer : dataContainer.getLayers()){
            List<List<Point2D> > pointLists = new ArrayList<>();
            for (GeomBase geo : layer.getGeomBases()) {
                pointLists.add(GeomBaseDeconstructor.externalToPoints(geo) );
            }
            namedPtList.put(layer.getName(), pointLists);
        }

        return namedPtList;
    }

    // (probably) only for GeomType.NETWORK
    // outputs the coordinate list (as usual) and adjusts the input connectionGraph to contain the correct mapping table
    public static List<List<Double>> deconstructingMultilineString(Geometry mls, List<List<Integer>> connectionGraph) {
        Coordinate[] coords = GeometryHelpers.multiLineStringToNetwork(mls, connectionGraph);
        List<Coordinate> reducedCoords = GeometryHelpers.reduceCoordinates(coords);
        return Outparser.coordinatesToDoubles(reducedCoords);
    }

    public static List<Double> coordinateToDouble (Coordinate coor) {
        return new ArrayList<Double>() { { add(coor.x); add(coor.y); } };
    }

    public static List<List<Double>> coordinatesToDoubles (List<Coordinate> coords) {
        List<List<Double>> doubleList = new ArrayList<>();
        for (Coordinate coor : coords) {
            doubleList.add(Outparser.coordinateToDouble(coor) );
        }
        return doubleList;
    }

    public static List<List<Double>> coordinatesToDoubles (Coordinate[] coords) {
        List<List<Double>> doubleList = new ArrayList<>();
        for (Coordinate coor : coords) {
            doubleList.add(Outparser.coordinateToDouble(coor) );
        }
        return doubleList;
    }

    public static List<FrontEndObject> frontEndObjectsParser(DataContainer dC) {
        List<FrontEndObject> fEOs = new ArrayList<>();

        for (GeomBase geomBase: dC.getGeomBases() ) {
            if (geomBase.isFeo() ) {
                fEOs.add(new FrontEndObject(geomBase));
            }
        }

        return fEOs;
    }
}