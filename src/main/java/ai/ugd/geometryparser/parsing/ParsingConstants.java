package ai.ugd.geometryparser.parsing;

import ai.ugd.geometryparser.data.LayerDataCollector;
import ai.ugd.geometryparser.data.LayerGeomParsingLogic;
import ai.ugd.geometryparser.data.LayerSettingsObject;
import ai.ugd.geometryparser.data.LayerValidityCheckLogic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class ParsingConstants {
    public static List<LayerSettingsObject> hcdDrawingModeLayers = Arrays.asList(
            new LayerSettingsObject("hcd_building_outline", LayerGeomParsingLogic.FORCEPOLYGON, 
                    new LayerValidityCheckLogic(LayerValidityCheckLogic.LayerValidityTypes.SPACE,
                            new LayerDataCollector( LayerDataCollector.DataCollectorType.OBJECTSONLAYER) )
            ),
            new LayerSettingsObject("hcd_building_rooms", LayerGeomParsingLogic.DEFAULT,
                    new LayerValidityCheckLogic(LayerValidityCheckLogic.LayerValidityTypes.WALL,
                            new LayerDataCollector( LayerDataCollector.DataCollectorType.OTHERLAYERSANDSELF, 
                                Arrays.asList("hcd_building_outline", "hcd_building_invisible", "hcd_building_rooms_distance")
                            ) )
            ),
            new LayerSettingsObject("hcd_building_invisible", LayerGeomParsingLogic.DEFAULT,
                    new LayerValidityCheckLogic(LayerValidityCheckLogic.LayerValidityTypes.WALL,
                            new LayerDataCollector( LayerDataCollector.DataCollectorType.OTHERLAYERSANDSELF,
                                Arrays.asList("hcd_building_outline", "hcd_building_rooms", "hcd_building_rooms_distance")
                            ) )
            ),
            new LayerSettingsObject("hcd_building_rooms_distance", LayerGeomParsingLogic.DEFAULT,
                    new LayerValidityCheckLogic(LayerValidityCheckLogic.LayerValidityTypes.WALL,
                            new LayerDataCollector( LayerDataCollector.DataCollectorType.OTHERLAYERSANDSELF,
                                Arrays.asList("hcd_building_outline", "hcd_building_invisible", "hcd_building_rooms")
                            ) )
            ),
            new LayerSettingsObject("hcd_drawings", LayerGeomParsingLogic.DEFAULT,
                    new LayerValidityCheckLogic(LayerValidityCheckLogic.LayerValidityTypes.NONE,
                            new LayerDataCollector( LayerDataCollector.DataCollectorType.NONE) )
            ),
            new LayerSettingsObject("hcd_windows", LayerGeomParsingLogic.DEFAULT,
                    new LayerValidityCheckLogic(LayerValidityCheckLogic.LayerValidityTypes.STAMP,
                            new LayerDataCollector( LayerDataCollector.DataCollectorType.OBJECTDEFINED) )
            ),
            new LayerSettingsObject("hcd_doors", LayerGeomParsingLogic.DEFAULT,
                    new LayerValidityCheckLogic(LayerValidityCheckLogic.LayerValidityTypes.STAMP,
                            new LayerDataCollector( LayerDataCollector.DataCollectorType.OBJECTDEFINED) )
            ),
            new LayerSettingsObject("hcd_entrances", LayerGeomParsingLogic.DEFAULT,
                    new LayerValidityCheckLogic(LayerValidityCheckLogic.LayerValidityTypes.STAMP,
                            new LayerDataCollector( LayerDataCollector.DataCollectorType.OBJECTDEFINED) )
            ),
            new LayerSettingsObject("hcd_pillars", LayerGeomParsingLogic.DEFAULT,
                    new LayerValidityCheckLogic(LayerValidityCheckLogic.LayerValidityTypes.OBJECT,
                            new LayerDataCollector( LayerDataCollector.DataCollectorType.OTHERLAYERS,
                                Arrays.asList("hcd_building_outline") ) )
            ),
            new LayerSettingsObject("hcd_connections", LayerGeomParsingLogic.ASIMPLICIT,
                    new LayerValidityCheckLogic(LayerValidityCheckLogic.LayerValidityTypes.CONNECTION,
                            new LayerDataCollector( LayerDataCollector.DataCollectorType.OTHERLAYERSANDSELF,
                                Arrays.asList("hcd_building_outline") ) )
            )
    );

    public static List<LayerSettingsObject> hcdDXFImportLayers = Arrays.asList(
            new LayerSettingsObject("hcd_building_outline", LayerGeomParsingLogic.FORCEPOLYGON,
                    new LayerValidityCheckLogic(LayerValidityCheckLogic.LayerValidityTypes.SPACE,
                            new LayerDataCollector( LayerDataCollector.DataCollectorType.OBJECTSONLAYER) )
            ),
            new LayerSettingsObject("hcd_building_rooms", LayerGeomParsingLogic.DEFAULT,
                    new LayerValidityCheckLogic(LayerValidityCheckLogic.LayerValidityTypes.WALL,
                            new LayerDataCollector( LayerDataCollector.DataCollectorType.OTHERLAYERSANDSELF,
                                Arrays.asList("hcd_building_outline", "hcd_building_invisible", "hcd_building_rooms_distance")
                            ) )
            ),
            new LayerSettingsObject("hcd_building_rooms_distance", LayerGeomParsingLogic.DEFAULT,
                    new LayerValidityCheckLogic(LayerValidityCheckLogic.LayerValidityTypes.WALL,
                            new LayerDataCollector( LayerDataCollector.DataCollectorType.OTHERLAYERSANDSELF,
                                Arrays.asList("hcd_building_outline", "hcd_building_invisible", "hcd_building_rooms")
                            ) )
            ),
            new LayerSettingsObject("hcd_building_invisible", LayerGeomParsingLogic.DEFAULT,
                    new LayerValidityCheckLogic(LayerValidityCheckLogic.LayerValidityTypes.WALL,
                            new LayerDataCollector( LayerDataCollector.DataCollectorType.OTHERLAYERSANDSELF,
                                Arrays.asList("hcd_building_outline", "hcd_building_rooms", "hcd_building_rooms_distance")
                            ) )
            ),
            new LayerSettingsObject("hcd_drawings", LayerGeomParsingLogic.DEFAULT,
                    new LayerValidityCheckLogic(LayerValidityCheckLogic.LayerValidityTypes.NONE,
                            new LayerDataCollector( LayerDataCollector.DataCollectorType.NONE) )
            ),
            new LayerSettingsObject("hcd_windows", LayerGeomParsingLogic.ASIMPLICIT,
                    new LayerValidityCheckLogic(LayerValidityCheckLogic.LayerValidityTypes.STAMP,
                            new LayerDataCollector( LayerDataCollector.DataCollectorType.OTHERLAYERS,
                                Arrays.asList("hcd_building_outline", "hcd_building_rooms") ) )
            ),
            new LayerSettingsObject("hcd_doors", LayerGeomParsingLogic.ASIMPLICIT,
                    new LayerValidityCheckLogic(LayerValidityCheckLogic.LayerValidityTypes.STAMP,
                            new LayerDataCollector( LayerDataCollector.DataCollectorType.OTHERLAYERS,
                                Arrays.asList("hcd_building_outline", "hcd_building_rooms") ) )
            ),
            new LayerSettingsObject("hcd_entrances", LayerGeomParsingLogic.ASIMPLICIT,
                    new LayerValidityCheckLogic(LayerValidityCheckLogic.LayerValidityTypes.STAMP,
                            new LayerDataCollector( LayerDataCollector.DataCollectorType.OTHERLAYERS,
                                Arrays.asList("hcd_building_outline", "hcd_building_rooms") ) )
            ),
            new LayerSettingsObject("hcd_pillars", LayerGeomParsingLogic.DEFAULT,
                    new LayerValidityCheckLogic(LayerValidityCheckLogic.LayerValidityTypes.OBJECT,
                            new LayerDataCollector( LayerDataCollector.DataCollectorType.OTHERLAYERS,
                                Arrays.asList("hcd_building_outline") ) )
            ),
            new LayerSettingsObject("hcd_connections", LayerGeomParsingLogic.ASIMPLICIT,
                    new LayerValidityCheckLogic(LayerValidityCheckLogic.LayerValidityTypes.CONNECTION,
                            new LayerDataCollector( LayerDataCollector.DataCollectorType.OTHERLAYERSANDSELF,
                                Arrays.asList("hcd_building_outline") ) )
            )
    );
    
    public static HashMap<String, LayerGeomParsingLogic> hcdParsing = new HashMap<String, LayerGeomParsingLogic>() {
        {
            put("hcd_building_outline", LayerGeomParsingLogic.FORCEPOLYGON);
            put("hcd_building_rooms", LayerGeomParsingLogic.DEFAULT);
            put("hcd_building_invisible", LayerGeomParsingLogic.DEFAULT);
            put("hcd_building_rooms_distance", LayerGeomParsingLogic.DEFAULT);
            put("hcd_drawings", LayerGeomParsingLogic.DEFAULT);
            put("hcd_windows", LayerGeomParsingLogic.DEFAULT);
            put("hcd_doors", LayerGeomParsingLogic.DEFAULT);
            put("hcd_pillars", LayerGeomParsingLogic.DEFAULT);
            put("hcd_connections", LayerGeomParsingLogic.ASIMPLICIT);
        }
    };

    public static List<String> hcdLayerNames = new ArrayList<String>() {
        {
            add("hcd_building_outline");
            add("hcd_building_rooms");
            add("hcd_building_invisible");
            add("hcd_building_rooms_distance");
            add("hcd_drawings");
            add("hcd_windows");
            add("hcd_doors");
            add("hcd_pillars");
            add("hcd_connections");
        }
    };
}
