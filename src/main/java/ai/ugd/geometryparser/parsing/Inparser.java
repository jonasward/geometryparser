package ai.ugd.geometryparser.parsing;

import ai.ugd.geometryparser.data.DataContainer;
import ai.ugd.geometryparser.data.LayerGeomParsingLogic;
import ai.ugd.geometryparser.data.LayerSettingsObject;

import java.awt.geom.Point2D;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

public class Inparser {
    public static DataContainer fromDXF(InputStream dxfStream){
        DXFParser dxfParser = new DXFParser(dxfStream);
//        dxfParser.initializeLayers(ParsingConstants.hcdParsing);
        dxfParser.parseLayers();
        return dxfParser.getDataContainer();
    }

    public static DataContainer fromHCDDXF(InputStream dxfStream){
        DXFParser dxfParser = new DXFParser(dxfStream, ParsingConstants.hcdDXFImportLayers);
        dxfParser.parseLayers();
        return dxfParser.getDataContainer();
    }

    public static DataContainer fromDXF(InputStream dxfStream, List<LayerSettingsObject> paringLogic){
        DXFParser dxfParser = new DXFParser(dxfStream, paringLogic);
        dxfParser.parseLayers();
        return dxfParser.getDataContainer();
    }

    public static DataContainer fromHashMapPoints(HashMap<String, List<List<Point2D>>> layersWithPointsHashMap){
        PointParser pointParser = new PointParser(layersWithPointsHashMap);
        pointParser.parseLayers();
        return pointParser.getDataContainer();
    }

    public static DataContainer fromHCDHashMapPoints(HashMap<String, List<List<Point2D>>> layersWithPointsHashMap){
        PointParser pointParser = new PointParser(layersWithPointsHashMap);
        pointParser.initializeLayers(ParsingConstants.hcdParsing);
        pointParser.parseLayers();
        return pointParser.getDataContainer();
    }

    public static DataContainer fromHashMapPoints(HashMap<String, List<List<Point2D>>> layersWithPointsHashMap, HashMap<String, LayerGeomParsingLogic> parsingMap){
        PointParser pointParser = new PointParser(layersWithPointsHashMap);
        pointParser.initializeLayers(parsingMap);
        pointParser.parseLayers();
        return pointParser.getDataContainer();
    }

//    public static DataContainer fromJSON()
}
